﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Ultra_Hash.Properties;

namespace Ultra_Hash
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private string MainFile = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) +
                                  "\\UltraHash.txt";
        private void Form1_Load(object sender, EventArgs e)
        {
            ShowInTaskbar = false;
            Visible = false;
            if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) +
                            "\\UltraHash.txt"))
            {
                string ToHash = File.ReadLines(MainFile).ElementAtOrDefault(0);
                for (int i = 0; i < 9; i++)
                {
                    ToHash = UniqueHashing(ToHash);
                }

                for (int i = 0; i < 100; i++)
                {
                    ToHash = GetHashString256(ToHash);
                    ToHash = GetHashString384(ToHash);
                    ToHash = GetHashString512(ToHash);
                }

                for (int i = 0; i < 100; i++)
                {
                    ToHash = GetHashString256(ToHash);
                    ToHash = GetHashString384(ToHash);
                    ToHash = GetHashString512(ToHash);
                }

                for (int i = 0; i < 100; i++)
                {
                    ToHash = GetHashString256(ToHash);
                    ToHash = GetHashString384(ToHash);
                    ToHash = GetHashString512(ToHash);
                }

                for (int i = 0; i < 100; i++)
                {
                    ToHash = GetHashString256(ToHash);
                    ToHash = GetHashString384(ToHash);
                    ToHash = GetHashString512(ToHash);
                }

                for (int i = 0; i < 100; i++)
                {
                    ToHash = GetHashString256(ToHash);
                    ToHash = GetHashString384(ToHash);
                    ToHash = GetHashString512(ToHash);
                }

                MainHash = ToHash;

                MainHash = SuperHash(MainHash);
                for (int i = 0; i < 100; i++)
                {
                    MainHash = SuperHash(MainHash);
                }

                MainHash = Base64Encode(MainHash);
                for (int i = 0; i < 100; i++)
                {
                    MainHash = SuperHash(MainHash) + SuperHash(MainHash);
                }

                MainHash = SuperHash(MainHash);
                MainHash = GetHashString256(MainHash);
                MainHash = GetHashString512(MainHash);
                Console.WriteLine(MainHash);
                File.WriteAllText(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\UltraHashed.txt",MainHash);
            }
            else
            {
                Application.Exit();
            }
            Application.Exit();
        }

        private string Base64Encode(string inputstring)
        {
            File.WriteAllText(Environment.GetEnvironmentVariable("TEMP") + "\\E.txt", inputstring);
            RunCommand("certutil -encode \"" + Environment.GetEnvironmentVariable("TEMP") + "\\E.txt\" \"" + Environment.GetEnvironmentVariable("TEMP") + "\\EN.txt\"");
            return File.ReadLines(Environment.GetEnvironmentVariable("TEMP") + "\\EN.txt").ElementAtOrDefault(1);
        }

        public void RunCommand(string Command)
        {
            string[] CommandChut = { Command };
            File.WriteAllLines(System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand.bat", CommandChut);
            var C = Process.Start(System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand.bat");
            C.WaitForExit();
            File.Delete(System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand.bat");
        }
        private bool Exit = false;
        public async Task RunCommandHidden(string Command)
        {
            string[] CommandChut = { Command };
            File.WriteAllLines(System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand.bat", CommandChut);
            Process C = new Process();
            C.StartInfo.FileName = System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand.bat";
            C.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            C.EnableRaisingEvents = true;
            C.Exited += C_Exited;
            C.Start();
            while (Exit == false)
            {
                await Task.Delay(10);
            }

            Exit = false;
            File.Delete(System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand.bat");
        }

        private void C_Exited(object sender, EventArgs e)
        {
            Exit = true;
        }
        private string SuperHash(string inputstring)
        {
            inputstring = GetHashString512(inputstring);
            inputstring = GetHashString256(inputstring);
            for (int i = 0; i < 1000; i++)
            {
                inputstring = GetHashString512(inputstring);
            }
            for (int i = 0; i < 1000; i++)
            {
                inputstring = GetHashString384(inputstring);
            }
            for (int i = 0; i < 1000; i++)
            {
                inputstring = GetHashString256(inputstring);
            }

            inputstring = GetHashString512(inputstring);
            return inputstring;
        }
        string MainHash = String.Empty;
        public static byte[] GetHash256(string inputString)
        {
            using (HashAlgorithm algorithm = SHA256.Create())
                return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        public static string GetHashString256(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash256(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }

        public static byte[] GetHash512(string inputString)
        {
            using (HashAlgorithm algorithm = SHA512.Create())
                return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        public static string GetHashString512(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash512(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }

        public static byte[] GetHash384(string inputString)
        {
            using (HashAlgorithm algorithm = SHA384.Create())
                return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        public static string GetHashString384(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash384(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }
        private string UniqueHashing(string inputstring)
        {
            File.WriteAllBytes(Environment.GetEnvironmentVariable("TEMP") + "\\Hasher.exe",Resources.Unique_Hasher);
            File.WriteAllText(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\Hashing.txt", inputstring);

            Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\Hasher.exe").WaitForExit();

            return File.ReadLines(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\HashedString.txt").ElementAtOrDefault(0);
            File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\HashedString.txt");
            File.Delete(Environment.GetEnvironmentVariable("TEMP") + "\\Hasher.exe");
        }
    }
}
