﻿namespace Secure_File_Vault
{
    partial class Create
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Create));
            this.Password = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.PasswordConfirm = new System.Windows.Forms.TextBox();
            this.PIM = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.VaultSize = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.CreateVaultPanel = new System.Windows.Forms.Panel();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.SecurityPINCreateCheckbox = new System.Windows.Forms.CheckBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.ThirdDriveLetter = new System.Windows.Forms.TextBox();
            this.UltraSecurityModeCreateCheckBox = new System.Windows.Forms.CheckBox();
            this.ExtraSecurityModeCreate = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.SecondDriveLetter = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.DriveLetter = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.SaveVaultLocation = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.MainTabs = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.CreateSecuritySettings = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ThreeStepVerificationCreateTextBox = new System.Windows.Forms.CheckBox();
            this.CreateROBLOXPanel = new System.Windows.Forms.Panel();
            this.button43 = new System.Windows.Forms.Button();
            this.button45 = new System.Windows.Forms.Button();
            this.CreateROBLOXModelListBox = new System.Windows.Forms.ListBox();
            this.button44 = new System.Windows.Forms.Button();
            this.CreateBrowseROBLOXModelsTextBox = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.CreateROBLOXModelCheckBox = new System.Windows.Forms.CheckBox();
            this.button42 = new System.Windows.Forms.Button();
            this.AddToListKeyFilesCreateButton = new System.Windows.Forms.Button();
            this.CreateKeyFilesListBox = new System.Windows.Forms.ListBox();
            this.CreateTwoStepVerificationPanel = new System.Windows.Forms.Panel();
            this.button38 = new System.Windows.Forms.Button();
            this.button37 = new System.Windows.Forms.Button();
            this.label34 = new System.Windows.Forms.Label();
            this.CreateVerificationCodeTextBox = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.CreateEmailAddressTextBox = new System.Windows.Forms.TextBox();
            this.TwoStepVerificationCreateCheckBox = new System.Windows.Forms.CheckBox();
            this.button35 = new System.Windows.Forms.Button();
            this.CreateKeyfilesBrowseTextBox = new System.Windows.Forms.TextBox();
            this.CreateKeyfilesCheckbox = new System.Windows.Forms.CheckBox();
            this.label32 = new System.Windows.Forms.Label();
            this.CreateNewHashingCheckBox = new System.Windows.Forms.CheckBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.button17 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.CreateDriveLetterPanel = new System.Windows.Forms.Panel();
            this.label29 = new System.Windows.Forms.Label();
            this.button16 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.CreationPanel = new System.Windows.Forms.Panel();
            this.FinalEncryptionTime = new System.Windows.Forms.Label();
            this.webBrowser2 = new System.Windows.Forms.WebBrowser();
            this.label30 = new System.Windows.Forms.Label();
            this.button19 = new System.Windows.Forms.Button();
            this.CreateVaultLocationPanel = new System.Windows.Forms.Panel();
            this.button34 = new System.Windows.Forms.Button();
            this.label31 = new System.Windows.Forms.Label();
            this.button33 = new System.Windows.Forms.Button();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.DragAndDropCreate = new System.Windows.Forms.Panel();
            this.label27 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button9 = new System.Windows.Forms.Button();
            this.CreateVaultSize = new System.Windows.Forms.Panel();
            this.EncryptionTimeLabel = new System.Windows.Forms.Label();
            this.button22 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.CreatePasswordPanel = new System.Windows.Forms.Panel();
            this.button24 = new System.Windows.Forms.Button();
            this.GeneratedPasswordTextBox = new System.Windows.Forms.TextBox();
            this.button23 = new System.Windows.Forms.Button();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.button10 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.CreatePIMPanel = new System.Windows.Forms.Panel();
            this.label28 = new System.Windows.Forms.Label();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.label22 = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.OpenSecuritySettingsPanel = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.OpenTwoStepVerificationPanel = new System.Windows.Forms.Panel();
            this.button39 = new System.Windows.Forms.Button();
            this.button40 = new System.Windows.Forms.Button();
            this.label35 = new System.Windows.Forms.Label();
            this.OpenVerificationCodeTextBox = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.OpenEmailAddressTextBox = new System.Windows.Forms.TextBox();
            this.OpenROBLOXModelPanel = new System.Windows.Forms.Panel();
            this.button46 = new System.Windows.Forms.Button();
            this.button47 = new System.Windows.Forms.Button();
            this.OpenROBLOXModelListBox = new System.Windows.Forms.ListBox();
            this.button48 = new System.Windows.Forms.Button();
            this.OpenROBLOXModelBrowseTextBox = new System.Windows.Forms.TextBox();
            this.OpenROBLOXModelCheckBox = new System.Windows.Forms.CheckBox();
            this.button41 = new System.Windows.Forms.Button();
            this.OpenTwoStepVerificationCheckBox = new System.Windows.Forms.CheckBox();
            this.button36 = new System.Windows.Forms.Button();
            this.OpenKeyFilesBrowseButton = new System.Windows.Forms.Button();
            this.OpenKeyFilesListBox = new System.Windows.Forms.ListBox();
            this.OpenKeyFilesBrowseTextbox = new System.Windows.Forms.TextBox();
            this.OpenKeyFilesCheckbox = new System.Windows.Forms.CheckBox();
            this.OpenNewHashingCheckBox = new System.Windows.Forms.CheckBox();
            this.SecurityPINOpenTextBox = new System.Windows.Forms.TextBox();
            this.SecurityPINOpenCheckbox = new System.Windows.Forms.CheckBox();
            this.UltraSecurityOpenCheckBox = new System.Windows.Forms.CheckBox();
            this.ExtraSecurityModeOpen = new System.Windows.Forms.CheckBox();
            this.button29 = new System.Windows.Forms.Button();
            this.button28 = new System.Windows.Forms.Button();
            this.OpenDriveLetterPanel = new System.Windows.Forms.Panel();
            this.button27 = new System.Windows.Forms.Button();
            this.button26 = new System.Windows.Forms.Button();
            this.ThirdOpenLetter = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.OpenSecondLetter = new System.Windows.Forms.TextBox();
            this.OpenFirstLetter = new System.Windows.Forms.TextBox();
            this.OpenLocationPanel = new System.Windows.Forms.Panel();
            this.button49 = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.OpenVaultDirectory = new System.Windows.Forms.TextBox();
            this.button7 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.OpenPIMPanel = new System.Windows.Forms.Panel();
            this.button31 = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.OpenVaultPIM = new System.Windows.Forms.TextBox();
            this.OpenPasswordPanel = new System.Windows.Forms.Panel();
            this.button32 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.PasswordOpen = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.ManageVaultPanel = new System.Windows.Forms.Panel();
            this.CloseAndLockVaultTimeLabel = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.StatusLabel = new System.Windows.Forms.Label();
            this.HideShowButton = new System.Windows.Forms.Button();
            this.StatusProgressBar = new System.Windows.Forms.ProgressBar();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.linkLabel3 = new System.Windows.Forms.LinkLabel();
            this.CoronavirusLabel = new System.Windows.Forms.Label();
            this.CreateUniqueEncryption = new System.Windows.Forms.CheckBox();
            this.label38 = new System.Windows.Forms.Label();
            this.OpenUniqueEncryption = new System.Windows.Forms.CheckBox();
            this.MainTabs.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.CreateSecuritySettings.SuspendLayout();
            this.panel1.SuspendLayout();
            this.CreateROBLOXPanel.SuspendLayout();
            this.CreateTwoStepVerificationPanel.SuspendLayout();
            this.CreateDriveLetterPanel.SuspendLayout();
            this.CreationPanel.SuspendLayout();
            this.CreateVaultLocationPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.DragAndDropCreate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.CreateVaultSize.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.CreatePasswordPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.CreatePIMPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.OpenSecuritySettingsPanel.SuspendLayout();
            this.panel3.SuspendLayout();
            this.OpenTwoStepVerificationPanel.SuspendLayout();
            this.OpenROBLOXModelPanel.SuspendLayout();
            this.OpenDriveLetterPanel.SuspendLayout();
            this.OpenLocationPanel.SuspendLayout();
            this.OpenPIMPanel.SuspendLayout();
            this.OpenPasswordPanel.SuspendLayout();
            this.ManageVaultPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.SuspendLayout();
            // 
            // Password
            // 
            this.Password.Location = new System.Drawing.Point(11, 37);
            this.Password.Name = "Password";
            this.Password.Size = new System.Drawing.Size(473, 30);
            this.Password.TabIndex = 0;
            this.Password.UseSystemPasswordChar = true;
            this.Password.TextChanged += new System.EventHandler(this.Password_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(235, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Create a strong password";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(217, 25);
            this.label2.TabIndex = 3;
            this.label2.Text = "Confirm Your Password";
            // 
            // PasswordConfirm
            // 
            this.PasswordConfirm.Location = new System.Drawing.Point(11, 92);
            this.PasswordConfirm.Name = "PasswordConfirm";
            this.PasswordConfirm.Size = new System.Drawing.Size(473, 30);
            this.PasswordConfirm.TabIndex = 2;
            this.PasswordConfirm.UseSystemPasswordChar = true;
            // 
            // PIM
            // 
            this.PIM.Location = new System.Drawing.Point(549, 84);
            this.PIM.Name = "PIM";
            this.PIM.Size = new System.Drawing.Size(69, 30);
            this.PIM.TabIndex = 4;
            this.PIM.TextChanged += new System.EventHandler(this.PIM_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Yu Gothic UI", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(452, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(266, 54);
            this.label3.TabIndex = 5;
            this.label3.Text = "Choose a PIM";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(6, 24);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(245, 59);
            this.button1.TabIndex = 6;
            this.button1.Text = "Create File Vault";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(483, 488);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(687, 56);
            this.button2.TabIndex = 7;
            this.button2.Text = "Cancel (Exits program)";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // VaultSize
            // 
            this.VaultSize.Location = new System.Drawing.Point(523, 94);
            this.VaultSize.Name = "VaultSize";
            this.VaultSize.Size = new System.Drawing.Size(113, 30);
            this.VaultSize.TabIndex = 8;
            this.VaultSize.TextChanged += new System.EventHandler(this.VaultSize_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe Script", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(326, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(454, 86);
            this.label4.TabIndex = 9;
            this.label4.Text = "File Vault Size";
            // 
            // CreateVaultPanel
            // 
            this.CreateVaultPanel.Location = new System.Drawing.Point(0, 0);
            this.CreateVaultPanel.Name = "CreateVaultPanel";
            this.CreateVaultPanel.Size = new System.Drawing.Size(1176, 557);
            this.CreateVaultPanel.TabIndex = 10;
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Location = new System.Drawing.Point(608, 162);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(224, 25);
            this.linkLabel2.TabIndex = 32;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "Clear Drive Label Usage";
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // SecurityPINCreateCheckbox
            // 
            this.SecurityPINCreateCheckbox.AutoSize = true;
            this.SecurityPINCreateCheckbox.BackColor = System.Drawing.Color.Maroon;
            this.SecurityPINCreateCheckbox.Enabled = false;
            this.SecurityPINCreateCheckbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SecurityPINCreateCheckbox.ForeColor = System.Drawing.Color.Yellow;
            this.SecurityPINCreateCheckbox.Location = new System.Drawing.Point(358, 52);
            this.SecurityPINCreateCheckbox.Name = "SecurityPINCreateCheckbox";
            this.SecurityPINCreateCheckbox.Size = new System.Drawing.Size(389, 35);
            this.SecurityPINCreateCheckbox.TabIndex = 31;
            this.SecurityPINCreateCheckbox.Text = "Generate 4 Digit Security PIN";
            this.SecurityPINCreateCheckbox.UseVisualStyleBackColor = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft JhengHei UI Light", 38F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(330, 19);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(540, 65);
            this.label18.TabIndex = 30;
            this.label18.Text = "More Security Settings";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(631, 102);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(57, 25);
            this.label17.TabIndex = 29;
            this.label17.Text = "Third";
            // 
            // ThirdDriveLetter
            // 
            this.ThirdDriveLetter.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.ThirdDriveLetter.Location = new System.Drawing.Point(703, 100);
            this.ThirdDriveLetter.MaxLength = 1;
            this.ThirdDriveLetter.Name = "ThirdDriveLetter";
            this.ThirdDriveLetter.Size = new System.Drawing.Size(101, 30);
            this.ThirdDriveLetter.TabIndex = 28;
            this.ThirdDriveLetter.TextChanged += new System.EventHandler(this.ThirdDriveLetter_TextChanged);
            // 
            // UltraSecurityModeCreateCheckBox
            // 
            this.UltraSecurityModeCreateCheckBox.AutoSize = true;
            this.UltraSecurityModeCreateCheckBox.BackColor = System.Drawing.Color.Maroon;
            this.UltraSecurityModeCreateCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UltraSecurityModeCreateCheckBox.ForeColor = System.Drawing.Color.Yellow;
            this.UltraSecurityModeCreateCheckBox.Location = new System.Drawing.Point(437, 19);
            this.UltraSecurityModeCreateCheckBox.Name = "UltraSecurityModeCreateCheckBox";
            this.UltraSecurityModeCreateCheckBox.Size = new System.Drawing.Size(234, 35);
            this.UltraSecurityModeCreateCheckBox.TabIndex = 27;
            this.UltraSecurityModeCreateCheckBox.Text = "ANTI FBI MODE";
            this.UltraSecurityModeCreateCheckBox.UseVisualStyleBackColor = false;
            this.UltraSecurityModeCreateCheckBox.CheckedChanged += new System.EventHandler(this.UltraSecurityModeCreateCheckBox_CheckedChanged);
            // 
            // ExtraSecurityModeCreate
            // 
            this.ExtraSecurityModeCreate.AutoSize = true;
            this.ExtraSecurityModeCreate.BackColor = System.Drawing.Color.Maroon;
            this.ExtraSecurityModeCreate.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExtraSecurityModeCreate.ForeColor = System.Drawing.Color.Yellow;
            this.ExtraSecurityModeCreate.Location = new System.Drawing.Point(9, 20);
            this.ExtraSecurityModeCreate.Name = "ExtraSecurityModeCreate";
            this.ExtraSecurityModeCreate.Size = new System.Drawing.Size(276, 35);
            this.ExtraSecurityModeCreate.TabIndex = 26;
            this.ExtraSecurityModeCreate.Text = "Extra Security Mode";
            this.ExtraSecurityModeCreate.UseVisualStyleBackColor = false;
            this.ExtraSecurityModeCreate.CheckedChanged += new System.EventHandler(this.ExtraSecurityModeCreate_CheckedChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(262, 147);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(583, 25);
            this.label15.TabIndex = 25;
            this.label15.Text = "You will not be able to use your computer while file vault is creating";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(364, 162);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 25);
            this.label10.TabIndex = 24;
            this.label10.Text = "Second";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(382, 103);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 25);
            this.label9.TabIndex = 23;
            this.label9.Text = "First";
            // 
            // SecondDriveLetter
            // 
            this.SecondDriveLetter.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.SecondDriveLetter.Location = new System.Drawing.Point(480, 160);
            this.SecondDriveLetter.MaxLength = 1;
            this.SecondDriveLetter.Name = "SecondDriveLetter";
            this.SecondDriveLetter.Size = new System.Drawing.Size(101, 30);
            this.SecondDriveLetter.TabIndex = 22;
            this.SecondDriveLetter.TextChanged += new System.EventHandler(this.SecondDriveLetter_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F);
            this.label8.Location = new System.Drawing.Point(151, 10);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(986, 76);
            this.label8.TabIndex = 21;
            this.label8.Text = "Choose 3 drive letters not in use";
            // 
            // DriveLetter
            // 
            this.DriveLetter.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.DriveLetter.Location = new System.Drawing.Point(480, 100);
            this.DriveLetter.MaxLength = 1;
            this.DriveLetter.Name = "DriveLetter";
            this.DriveLetter.Size = new System.Drawing.Size(101, 30);
            this.DriveLetter.TabIndex = 20;
            this.DriveLetter.TextChanged += new System.EventHandler(this.DriveLetter_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(56, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(315, 25);
            this.label7.TabIndex = 19;
            this.label7.Text = "Choose a location to save the vault";
            // 
            // SaveVaultLocation
            // 
            this.SaveVaultLocation.Location = new System.Drawing.Point(54, 41);
            this.SaveVaultLocation.Name = "SaveVaultLocation";
            this.SaveVaultLocation.Size = new System.Drawing.Size(390, 30);
            this.SaveVaultLocation.TabIndex = 18;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(460, 36);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(211, 46);
            this.button4.TabIndex = 17;
            this.button4.Text = "Browse";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(156, 135);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(172, 29);
            this.checkBox1.TabIndex = 15;
            this.checkBox1.Text = "Show Password";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // MainTabs
            // 
            this.MainTabs.Controls.Add(this.tabPage1);
            this.MainTabs.Controls.Add(this.tabPage3);
            this.MainTabs.Location = new System.Drawing.Point(12, 54);
            this.MainTabs.Name = "MainTabs";
            this.MainTabs.SelectedIndex = 0;
            this.MainTabs.Size = new System.Drawing.Size(1225, 578);
            this.MainTabs.TabIndex = 11;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Black;
            this.tabPage1.Controls.Add(this.CreateSecuritySettings);
            this.tabPage1.Controls.Add(this.CreateDriveLetterPanel);
            this.tabPage1.Controls.Add(this.CreationPanel);
            this.tabPage1.Controls.Add(this.CreateVaultLocationPanel);
            this.tabPage1.Controls.Add(this.CreateVaultSize);
            this.tabPage1.Controls.Add(this.CreatePasswordPanel);
            this.tabPage1.Controls.Add(this.CreatePIMPanel);
            this.tabPage1.Controls.Add(this.CreateVaultPanel);
            this.tabPage1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1217, 552);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Create A File Vault";
            // 
            // CreateSecuritySettings
            // 
            this.CreateSecuritySettings.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.CreateSecuritySettings.Controls.Add(this.panel1);
            this.CreateSecuritySettings.Controls.Add(this.button17);
            this.CreateSecuritySettings.Controls.Add(this.label18);
            this.CreateSecuritySettings.Controls.Add(this.button18);
            this.CreateSecuritySettings.Location = new System.Drawing.Point(0, 0);
            this.CreateSecuritySettings.Name = "CreateSecuritySettings";
            this.CreateSecuritySettings.Size = new System.Drawing.Size(1179, 554);
            this.CreateSecuritySettings.TabIndex = 17;
            this.CreateSecuritySettings.Paint += new System.Windows.Forms.PaintEventHandler(this.CreateSecuritySettings_Paint);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.label38);
            this.panel1.Controls.Add(this.CreateUniqueEncryption);
            this.panel1.Controls.Add(this.ThreeStepVerificationCreateTextBox);
            this.panel1.Controls.Add(this.CreateROBLOXPanel);
            this.panel1.Controls.Add(this.label37);
            this.panel1.Controls.Add(this.CreateROBLOXModelCheckBox);
            this.panel1.Controls.Add(this.button42);
            this.panel1.Controls.Add(this.AddToListKeyFilesCreateButton);
            this.panel1.Controls.Add(this.CreateKeyFilesListBox);
            this.panel1.Controls.Add(this.CreateTwoStepVerificationPanel);
            this.panel1.Controls.Add(this.TwoStepVerificationCreateCheckBox);
            this.panel1.Controls.Add(this.button35);
            this.panel1.Controls.Add(this.CreateKeyfilesBrowseTextBox);
            this.panel1.Controls.Add(this.CreateKeyfilesCheckbox);
            this.panel1.Controls.Add(this.label32);
            this.panel1.Controls.Add(this.CreateNewHashingCheckBox);
            this.panel1.Controls.Add(this.label24);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Controls.Add(this.SecurityPINCreateCheckbox);
            this.panel1.Controls.Add(this.ExtraSecurityModeCreate);
            this.panel1.Controls.Add(this.UltraSecurityModeCreateCheckBox);
            this.panel1.Location = new System.Drawing.Point(13, 84);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1155, 398);
            this.panel1.TabIndex = 53;
            // 
            // ThreeStepVerificationCreateTextBox
            // 
            this.ThreeStepVerificationCreateTextBox.AutoSize = true;
            this.ThreeStepVerificationCreateTextBox.BackColor = System.Drawing.Color.Maroon;
            this.ThreeStepVerificationCreateTextBox.Enabled = false;
            this.ThreeStepVerificationCreateTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ThreeStepVerificationCreateTextBox.ForeColor = System.Drawing.Color.Yellow;
            this.ThreeStepVerificationCreateTextBox.Location = new System.Drawing.Point(821, 264);
            this.ThreeStepVerificationCreateTextBox.Name = "ThreeStepVerificationCreateTextBox";
            this.ThreeStepVerificationCreateTextBox.Size = new System.Drawing.Size(255, 35);
            this.ThreeStepVerificationCreateTextBox.TabIndex = 62;
            this.ThreeStepVerificationCreateTextBox.Text = "3-Step Verification";
            this.ThreeStepVerificationCreateTextBox.UseVisualStyleBackColor = false;
            this.ThreeStepVerificationCreateTextBox.CheckedChanged += new System.EventHandler(this.ThreeStepVerificationCreateTextBox_CheckedChanged);
            // 
            // CreateROBLOXPanel
            // 
            this.CreateROBLOXPanel.Controls.Add(this.button43);
            this.CreateROBLOXPanel.Controls.Add(this.button45);
            this.CreateROBLOXPanel.Controls.Add(this.CreateROBLOXModelListBox);
            this.CreateROBLOXPanel.Controls.Add(this.button44);
            this.CreateROBLOXPanel.Controls.Add(this.CreateBrowseROBLOXModelsTextBox);
            this.CreateROBLOXPanel.Location = new System.Drawing.Point(4, 538);
            this.CreateROBLOXPanel.Name = "CreateROBLOXPanel";
            this.CreateROBLOXPanel.Size = new System.Drawing.Size(531, 201);
            this.CreateROBLOXPanel.TabIndex = 61;
            this.CreateROBLOXPanel.Visible = false;
            // 
            // button43
            // 
            this.button43.Location = new System.Drawing.Point(380, 55);
            this.button43.Name = "button43";
            this.button43.Size = new System.Drawing.Size(130, 32);
            this.button43.TabIndex = 59;
            this.button43.Text = "Add";
            this.button43.UseVisualStyleBackColor = true;
            this.button43.Click += new System.EventHandler(this.button43_Click);
            // 
            // button45
            // 
            this.button45.Location = new System.Drawing.Point(147, 152);
            this.button45.Name = "button45";
            this.button45.Size = new System.Drawing.Size(200, 40);
            this.button45.TabIndex = 60;
            this.button45.Text = "Done";
            this.button45.UseVisualStyleBackColor = true;
            this.button45.Click += new System.EventHandler(this.button45_Click);
            // 
            // CreateROBLOXModelListBox
            // 
            this.CreateROBLOXModelListBox.FormattingEnabled = true;
            this.CreateROBLOXModelListBox.ItemHeight = 25;
            this.CreateROBLOXModelListBox.Location = new System.Drawing.Point(7, 42);
            this.CreateROBLOXModelListBox.Name = "CreateROBLOXModelListBox";
            this.CreateROBLOXModelListBox.Size = new System.Drawing.Size(369, 104);
            this.CreateROBLOXModelListBox.TabIndex = 58;
            // 
            // button44
            // 
            this.button44.Location = new System.Drawing.Point(380, 0);
            this.button44.Name = "button44";
            this.button44.Size = new System.Drawing.Size(130, 32);
            this.button44.TabIndex = 57;
            this.button44.Text = "Browse";
            this.button44.UseVisualStyleBackColor = true;
            this.button44.Click += new System.EventHandler(this.button44_Click);
            // 
            // CreateBrowseROBLOXModelsTextBox
            // 
            this.CreateBrowseROBLOXModelsTextBox.Location = new System.Drawing.Point(6, 1);
            this.CreateBrowseROBLOXModelsTextBox.Name = "CreateBrowseROBLOXModelsTextBox";
            this.CreateBrowseROBLOXModelsTextBox.Size = new System.Drawing.Size(369, 30);
            this.CreateBrowseROBLOXModelsTextBox.TabIndex = 56;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label37.Location = new System.Drawing.Point(7, 497);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(149, 51);
            this.label37.TabIndex = 55;
            this.label37.Text = "Upload a roblox model\r\n to encrypt your vault\r\n\r\n";
            // 
            // CreateROBLOXModelCheckBox
            // 
            this.CreateROBLOXModelCheckBox.AutoSize = true;
            this.CreateROBLOXModelCheckBox.BackColor = System.Drawing.Color.Maroon;
            this.CreateROBLOXModelCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreateROBLOXModelCheckBox.ForeColor = System.Drawing.Color.Yellow;
            this.CreateROBLOXModelCheckBox.Location = new System.Drawing.Point(10, 456);
            this.CreateROBLOXModelCheckBox.Name = "CreateROBLOXModelCheckBox";
            this.CreateROBLOXModelCheckBox.Size = new System.Drawing.Size(362, 35);
            this.CreateROBLOXModelCheckBox.TabIndex = 54;
            this.CreateROBLOXModelCheckBox.Text = "ROBLOX Model Encryption";
            this.CreateROBLOXModelCheckBox.UseVisualStyleBackColor = false;
            this.CreateROBLOXModelCheckBox.CheckedChanged += new System.EventHandler(this.CreateROBLOXModelCheckBox_CheckedChanged);
            // 
            // button42
            // 
            this.button42.Location = new System.Drawing.Point(414, 452);
            this.button42.Name = "button42";
            this.button42.Size = new System.Drawing.Size(267, 47);
            this.button42.TabIndex = 53;
            this.button42.Text = "Create new keyfile";
            this.button42.UseVisualStyleBackColor = true;
            this.button42.Click += new System.EventHandler(this.button42_Click);
            // 
            // AddToListKeyFilesCreateButton
            // 
            this.AddToListKeyFilesCreateButton.Location = new System.Drawing.Point(729, 355);
            this.AddToListKeyFilesCreateButton.Name = "AddToListKeyFilesCreateButton";
            this.AddToListKeyFilesCreateButton.Size = new System.Drawing.Size(130, 32);
            this.AddToListKeyFilesCreateButton.TabIndex = 44;
            this.AddToListKeyFilesCreateButton.Text = "Add";
            this.AddToListKeyFilesCreateButton.UseVisualStyleBackColor = true;
            this.AddToListKeyFilesCreateButton.Click += new System.EventHandler(this.AddToListKeyFilesCreateButton_Click);
            // 
            // CreateKeyFilesListBox
            // 
            this.CreateKeyFilesListBox.FormattingEnabled = true;
            this.CreateKeyFilesListBox.ItemHeight = 25;
            this.CreateKeyFilesListBox.Location = new System.Drawing.Point(356, 342);
            this.CreateKeyFilesListBox.Name = "CreateKeyFilesListBox";
            this.CreateKeyFilesListBox.Size = new System.Drawing.Size(369, 104);
            this.CreateKeyFilesListBox.TabIndex = 43;
            // 
            // CreateTwoStepVerificationPanel
            // 
            this.CreateTwoStepVerificationPanel.Controls.Add(this.button38);
            this.CreateTwoStepVerificationPanel.Controls.Add(this.button37);
            this.CreateTwoStepVerificationPanel.Controls.Add(this.label34);
            this.CreateTwoStepVerificationPanel.Controls.Add(this.CreateVerificationCodeTextBox);
            this.CreateTwoStepVerificationPanel.Controls.Add(this.label33);
            this.CreateTwoStepVerificationPanel.Controls.Add(this.CreateEmailAddressTextBox);
            this.CreateTwoStepVerificationPanel.Location = new System.Drawing.Point(799, 52);
            this.CreateTwoStepVerificationPanel.Name = "CreateTwoStepVerificationPanel";
            this.CreateTwoStepVerificationPanel.Size = new System.Drawing.Size(305, 211);
            this.CreateTwoStepVerificationPanel.TabIndex = 52;
            this.CreateTwoStepVerificationPanel.Visible = false;
            // 
            // button38
            // 
            this.button38.Location = new System.Drawing.Point(3, 162);
            this.button38.Name = "button38";
            this.button38.Size = new System.Drawing.Size(288, 33);
            this.button38.TabIndex = 51;
            this.button38.Text = "Verify";
            this.button38.UseVisualStyleBackColor = true;
            this.button38.Click += new System.EventHandler(this.button38_Click);
            // 
            // button37
            // 
            this.button37.Location = new System.Drawing.Point(68, 65);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(152, 34);
            this.button37.TabIndex = 50;
            this.button37.Text = "Get Code";
            this.button37.UseVisualStyleBackColor = true;
            this.button37.Click += new System.EventHandler(this.button37_Click);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(47, 98);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(203, 25);
            this.label34.TabIndex = 49;
            this.label34.Text = "Enter verification code";
            // 
            // CreateVerificationCodeTextBox
            // 
            this.CreateVerificationCodeTextBox.Location = new System.Drawing.Point(12, 126);
            this.CreateVerificationCodeTextBox.Name = "CreateVerificationCodeTextBox";
            this.CreateVerificationCodeTextBox.Size = new System.Drawing.Size(265, 30);
            this.CreateVerificationCodeTextBox.TabIndex = 48;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(25, 5);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(233, 25);
            this.label33.TabIndex = 47;
            this.label33.Text = "Enter your email address:";
            // 
            // CreateEmailAddressTextBox
            // 
            this.CreateEmailAddressTextBox.Location = new System.Drawing.Point(12, 33);
            this.CreateEmailAddressTextBox.Name = "CreateEmailAddressTextBox";
            this.CreateEmailAddressTextBox.Size = new System.Drawing.Size(265, 30);
            this.CreateEmailAddressTextBox.TabIndex = 46;
            // 
            // TwoStepVerificationCreateCheckBox
            // 
            this.TwoStepVerificationCreateCheckBox.AutoSize = true;
            this.TwoStepVerificationCreateCheckBox.BackColor = System.Drawing.Color.Maroon;
            this.TwoStepVerificationCreateCheckBox.Enabled = false;
            this.TwoStepVerificationCreateCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TwoStepVerificationCreateCheckBox.ForeColor = System.Drawing.Color.Yellow;
            this.TwoStepVerificationCreateCheckBox.Location = new System.Drawing.Point(811, 10);
            this.TwoStepVerificationCreateCheckBox.Name = "TwoStepVerificationCreateCheckBox";
            this.TwoStepVerificationCreateCheckBox.Size = new System.Drawing.Size(255, 35);
            this.TwoStepVerificationCreateCheckBox.TabIndex = 45;
            this.TwoStepVerificationCreateCheckBox.Text = "2-Step Verification";
            this.TwoStepVerificationCreateCheckBox.UseVisualStyleBackColor = false;
            this.TwoStepVerificationCreateCheckBox.CheckedChanged += new System.EventHandler(this.TwoStepVerificationCreateCheckBox_CheckedChanged);
            // 
            // button35
            // 
            this.button35.Location = new System.Drawing.Point(729, 300);
            this.button35.Name = "button35";
            this.button35.Size = new System.Drawing.Size(130, 32);
            this.button35.TabIndex = 42;
            this.button35.Text = "Browse";
            this.button35.UseVisualStyleBackColor = true;
            this.button35.Click += new System.EventHandler(this.button35_Click);
            // 
            // CreateKeyfilesBrowseTextBox
            // 
            this.CreateKeyfilesBrowseTextBox.Location = new System.Drawing.Point(355, 301);
            this.CreateKeyfilesBrowseTextBox.Name = "CreateKeyfilesBrowseTextBox";
            this.CreateKeyfilesBrowseTextBox.Size = new System.Drawing.Size(369, 30);
            this.CreateKeyfilesBrowseTextBox.TabIndex = 41;
            // 
            // CreateKeyfilesCheckbox
            // 
            this.CreateKeyfilesCheckbox.AutoSize = true;
            this.CreateKeyfilesCheckbox.BackColor = System.Drawing.Color.Maroon;
            this.CreateKeyfilesCheckbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreateKeyfilesCheckbox.ForeColor = System.Drawing.Color.Yellow;
            this.CreateKeyfilesCheckbox.Location = new System.Drawing.Point(348, 251);
            this.CreateKeyfilesCheckbox.Name = "CreateKeyfilesCheckbox";
            this.CreateKeyfilesCheckbox.Size = new System.Drawing.Size(129, 35);
            this.CreateKeyfilesCheckbox.TabIndex = 40;
            this.CreateKeyfilesCheckbox.Text = "Keyfiles";
            this.CreateKeyfilesCheckbox.UseVisualStyleBackColor = false;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label32.Location = new System.Drawing.Point(48, 258);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(289, 136);
            this.label32.TabIndex = 39;
            this.label32.Text = resources.GetString("label32.Text");
            // 
            // CreateNewHashingCheckBox
            // 
            this.CreateNewHashingCheckBox.AutoSize = true;
            this.CreateNewHashingCheckBox.BackColor = System.Drawing.Color.Maroon;
            this.CreateNewHashingCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreateNewHashingCheckBox.ForeColor = System.Drawing.Color.Yellow;
            this.CreateNewHashingCheckBox.Location = new System.Drawing.Point(9, 216);
            this.CreateNewHashingCheckBox.Name = "CreateNewHashingCheckBox";
            this.CreateNewHashingCheckBox.Size = new System.Drawing.Size(325, 35);
            this.CreateNewHashingCheckBox.TabIndex = 38;
            this.CreateNewHashingCheckBox.Text = "New Hashing Protection";
            this.CreateNewHashingCheckBox.UseVisualStyleBackColor = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.label24.Location = new System.Drawing.Point(345, 90);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(256, 156);
            this.label24.TabIndex = 37;
            this.label24.Text = resources.GetString("label24.Text");
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label23.Location = new System.Drawing.Point(43, 70);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(209, 136);
            this.label23.TabIndex = 36;
            this.label23.Text = "Extra 7Zip AES 256 Encryption\r\n\r\n- Millitary Grade\r\n- POLICE ENCRYPTION\r\n- FBI EN" +
    "CRYPTION\r\n- GOVERNMENT ENCRYPTION\r\n- ANTI DOG\r\n- Supports Silent";
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(980, 491);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(183, 49);
            this.button17.TabIndex = 35;
            this.button17.Text = "Next >";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(10, 493);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(187, 50);
            this.button18.TabIndex = 34;
            this.button18.Text = "< Back";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // CreateDriveLetterPanel
            // 
            this.CreateDriveLetterPanel.BackColor = System.Drawing.Color.Silver;
            this.CreateDriveLetterPanel.Controls.Add(this.label29);
            this.CreateDriveLetterPanel.Controls.Add(this.button16);
            this.CreateDriveLetterPanel.Controls.Add(this.button15);
            this.CreateDriveLetterPanel.Controls.Add(this.linkLabel2);
            this.CreateDriveLetterPanel.Controls.Add(this.ThirdDriveLetter);
            this.CreateDriveLetterPanel.Controls.Add(this.DriveLetter);
            this.CreateDriveLetterPanel.Controls.Add(this.label8);
            this.CreateDriveLetterPanel.Controls.Add(this.label17);
            this.CreateDriveLetterPanel.Controls.Add(this.SecondDriveLetter);
            this.CreateDriveLetterPanel.Controls.Add(this.label9);
            this.CreateDriveLetterPanel.Controls.Add(this.label10);
            this.CreateDriveLetterPanel.Location = new System.Drawing.Point(0, 0);
            this.CreateDriveLetterPanel.Name = "CreateDriveLetterPanel";
            this.CreateDriveLetterPanel.Size = new System.Drawing.Size(1179, 560);
            this.CreateDriveLetterPanel.TabIndex = 15;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(370, 243);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(469, 100);
            this.label29.TabIndex = 34;
            this.label29.Text = "Our program requires that you input 3 drive letters\r\nthat is not in use by window" +
    "s or veracrypt. This is that\r\nso the program can create virtual disks and veracr" +
    "ypt\r\nvaults";
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(944, 491);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(226, 63);
            this.button16.TabIndex = 33;
            this.button16.Text = "Next >";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(3, 483);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(275, 64);
            this.button15.TabIndex = 24;
            this.button15.Text = "< Back";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // CreationPanel
            // 
            this.CreationPanel.BackColor = System.Drawing.Color.DarkRed;
            this.CreationPanel.Controls.Add(this.FinalEncryptionTime);
            this.CreationPanel.Controls.Add(this.webBrowser2);
            this.CreationPanel.Controls.Add(this.label30);
            this.CreationPanel.Controls.Add(this.button19);
            this.CreationPanel.Controls.Add(this.button2);
            this.CreationPanel.Controls.Add(this.button1);
            this.CreationPanel.Controls.Add(this.label15);
            this.CreationPanel.Location = new System.Drawing.Point(0, 0);
            this.CreationPanel.Name = "CreationPanel";
            this.CreationPanel.Size = new System.Drawing.Size(1179, 560);
            this.CreationPanel.TabIndex = 18;
            // 
            // FinalEncryptionTime
            // 
            this.FinalEncryptionTime.AutoSize = true;
            this.FinalEncryptionTime.Location = new System.Drawing.Point(906, 24);
            this.FinalEncryptionTime.Name = "FinalEncryptionTime";
            this.FinalEncryptionTime.Size = new System.Drawing.Size(162, 25);
            this.FinalEncryptionTime.TabIndex = 39;
            this.FinalEncryptionTime.Text = "Time To Encrypt:";
            // 
            // webBrowser2
            // 
            this.webBrowser2.Location = new System.Drawing.Point(41, 179);
            this.webBrowser2.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser2.Name = "webBrowser2";
            this.webBrowser2.ScriptErrorsSuppressed = true;
            this.webBrowser2.Size = new System.Drawing.Size(1121, 301);
            this.webBrowser2.TabIndex = 38;
            this.webBrowser2.Url = new System.Uri("https://howsecureismypassword.net/", System.UriKind.Absolute);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.label30.Location = new System.Drawing.Point(264, 8);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(542, 130);
            this.label30.TabIndex = 37;
            this.label30.Text = resources.GetString("label30.Text");
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(23, 488);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(444, 56);
            this.button19.TabIndex = 36;
            this.button19.Text = "< Back";
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // CreateVaultLocationPanel
            // 
            this.CreateVaultLocationPanel.BackColor = System.Drawing.Color.DarkRed;
            this.CreateVaultLocationPanel.Controls.Add(this.button34);
            this.CreateVaultLocationPanel.Controls.Add(this.label31);
            this.CreateVaultLocationPanel.Controls.Add(this.button33);
            this.CreateVaultLocationPanel.Controls.Add(this.pictureBox8);
            this.CreateVaultLocationPanel.Controls.Add(this.DragAndDropCreate);
            this.CreateVaultLocationPanel.Controls.Add(this.pictureBox4);
            this.CreateVaultLocationPanel.Controls.Add(this.pictureBox1);
            this.CreateVaultLocationPanel.Controls.Add(this.button9);
            this.CreateVaultLocationPanel.Controls.Add(this.SaveVaultLocation);
            this.CreateVaultLocationPanel.Controls.Add(this.button4);
            this.CreateVaultLocationPanel.Controls.Add(this.label7);
            this.CreateVaultLocationPanel.Location = new System.Drawing.Point(0, 0);
            this.CreateVaultLocationPanel.Name = "CreateVaultLocationPanel";
            this.CreateVaultLocationPanel.Size = new System.Drawing.Size(1165, 551);
            this.CreateVaultLocationPanel.TabIndex = 13;
            this.CreateVaultLocationPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.CreateVaultLocationPanel_Paint);
            // 
            // button34
            // 
            this.button34.Location = new System.Drawing.Point(549, 397);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(230, 39);
            this.button34.TabIndex = 27;
            this.button34.Text = "Secure Mode";
            this.button34.UseVisualStyleBackColor = true;
            this.button34.Click += new System.EventHandler(this.button34_Click);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label31.Location = new System.Drawing.Point(821, 184);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(318, 170);
            this.label31.TabIndex = 26;
            this.label31.Text = resources.GetString("label31.Text");
            // 
            // button33
            // 
            this.button33.Location = new System.Drawing.Point(871, 109);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(238, 55);
            this.button33.TabIndex = 25;
            this.button33.Text = "Benchmark Mode";
            this.button33.UseVisualStyleBackColor = true;
            this.button33.Click += new System.EventHandler(this.button33_Click);
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = global::Secure_File_Vault.Properties.Resources.download1;
            this.pictureBox8.Location = new System.Drawing.Point(559, 92);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(230, 227);
            this.pictureBox8.TabIndex = 24;
            this.pictureBox8.TabStop = false;
            // 
            // DragAndDropCreate
            // 
            this.DragAndDropCreate.AllowDrop = true;
            this.DragAndDropCreate.BackColor = System.Drawing.SystemColors.Window;
            this.DragAndDropCreate.Controls.Add(this.label27);
            this.DragAndDropCreate.Location = new System.Drawing.Point(67, 107);
            this.DragAndDropCreate.Name = "DragAndDropCreate";
            this.DragAndDropCreate.Size = new System.Drawing.Size(456, 193);
            this.DragAndDropCreate.TabIndex = 23;
            this.DragAndDropCreate.Paint += new System.Windows.Forms.PaintEventHandler(this.DragAndDropCreate_Paint);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(41, 79);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(384, 25);
            this.label27.TabIndex = 0;
            this.label27.Text = "Drag and drop file here to replace with vault";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Secure_File_Vault.Properties.Resources.first_slide;
            this.pictureBox4.Location = new System.Drawing.Point(725, 35);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(391, 35);
            this.pictureBox4.TabIndex = 22;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Secure_File_Vault.Properties.Resources.Vault_Location_Slide;
            this.pictureBox1.Location = new System.Drawing.Point(23, 382);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(425, 146);
            this.pictureBox1.TabIndex = 21;
            this.pictureBox1.TabStop = false;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(871, 473);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(266, 55);
            this.button9.TabIndex = 20;
            this.button9.Text = "Next >";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // CreateVaultSize
            // 
            this.CreateVaultSize.BackColor = System.Drawing.Color.LightCoral;
            this.CreateVaultSize.Controls.Add(this.EncryptionTimeLabel);
            this.CreateVaultSize.Controls.Add(this.button22);
            this.CreateVaultSize.Controls.Add(this.button21);
            this.CreateVaultSize.Controls.Add(this.button20);
            this.CreateVaultSize.Controls.Add(this.label26);
            this.CreateVaultSize.Controls.Add(this.label25);
            this.CreateVaultSize.Controls.Add(this.pictureBox9);
            this.CreateVaultSize.Controls.Add(this.pictureBox3);
            this.CreateVaultSize.Controls.Add(this.button11);
            this.CreateVaultSize.Controls.Add(this.label4);
            this.CreateVaultSize.Controls.Add(this.button12);
            this.CreateVaultSize.Controls.Add(this.VaultSize);
            this.CreateVaultSize.Location = new System.Drawing.Point(0, 0);
            this.CreateVaultSize.Name = "CreateVaultSize";
            this.CreateVaultSize.Size = new System.Drawing.Size(1179, 560);
            this.CreateVaultSize.TabIndex = 14;
            // 
            // EncryptionTimeLabel
            // 
            this.EncryptionTimeLabel.AutoSize = true;
            this.EncryptionTimeLabel.Location = new System.Drawing.Point(809, 199);
            this.EncryptionTimeLabel.Name = "EncryptionTimeLabel";
            this.EncryptionTimeLabel.Size = new System.Drawing.Size(159, 25);
            this.EncryptionTimeLabel.TabIndex = 31;
            this.EncryptionTimeLabel.Text = "Encryption Time:";
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(979, 75);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(168, 43);
            this.button22.TabIndex = 30;
            this.button22.Text = "500 Megabytes";
            this.button22.UseVisualStyleBackColor = true;
            this.button22.Click += new System.EventHandler(this.button22_Click);
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(810, 76);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(163, 41);
            this.button21.TabIndex = 29;
            this.button21.Text = "100 Megabytes";
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.button21_Click);
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(653, 76);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(151, 41);
            this.button20.TabIndex = 28;
            this.button20.Text = "10 Megabytes";
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.button20_Click);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(442, 95);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(77, 17);
            this.label26.TabIndex = 27;
            this.label26.Text = "Enter Size:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(253, 117);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(749, 25);
            this.label25.TabIndex = 26;
            this.label25.Text = "Size must be in MegaBytes, and the maximum size if using \"Anti FBI Mode\" is 500MB" +
    "";
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = global::Secure_File_Vault.Properties.Resources.download2;
            this.pictureBox9.Location = new System.Drawing.Point(452, 153);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(242, 235);
            this.pictureBox9.TabIndex = 25;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Secure_File_Vault.Properties.Resources.Vault_size_slide;
            this.pictureBox3.Location = new System.Drawing.Point(375, 408);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(431, 136);
            this.pictureBox3.TabIndex = 24;
            this.pictureBox3.TabStop = false;
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(21, 491);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(257, 62);
            this.button11.TabIndex = 23;
            this.button11.Text = "< Back";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(889, 483);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(284, 71);
            this.button12.TabIndex = 22;
            this.button12.Text = "Next >";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // CreatePasswordPanel
            // 
            this.CreatePasswordPanel.BackColor = System.Drawing.Color.Gray;
            this.CreatePasswordPanel.Controls.Add(this.button24);
            this.CreatePasswordPanel.Controls.Add(this.GeneratedPasswordTextBox);
            this.CreatePasswordPanel.Controls.Add(this.button23);
            this.CreatePasswordPanel.Controls.Add(this.webBrowser1);
            this.CreatePasswordPanel.Controls.Add(this.pictureBox2);
            this.CreatePasswordPanel.Controls.Add(this.button10);
            this.CreatePasswordPanel.Controls.Add(this.button8);
            this.CreatePasswordPanel.Controls.Add(this.Password);
            this.CreatePasswordPanel.Controls.Add(this.label1);
            this.CreatePasswordPanel.Controls.Add(this.PasswordConfirm);
            this.CreatePasswordPanel.Controls.Add(this.checkBox1);
            this.CreatePasswordPanel.Controls.Add(this.label2);
            this.CreatePasswordPanel.Location = new System.Drawing.Point(0, 0);
            this.CreatePasswordPanel.Name = "CreatePasswordPanel";
            this.CreatePasswordPanel.Size = new System.Drawing.Size(1176, 554);
            this.CreatePasswordPanel.TabIndex = 12;
            // 
            // button24
            // 
            this.button24.Location = new System.Drawing.Point(577, 499);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(183, 37);
            this.button24.TabIndex = 26;
            this.button24.Text = "Save To Text File";
            this.button24.UseVisualStyleBackColor = true;
            this.button24.Click += new System.EventHandler(this.button24_Click);
            // 
            // GeneratedPasswordTextBox
            // 
            this.GeneratedPasswordTextBox.Location = new System.Drawing.Point(341, 503);
            this.GeneratedPasswordTextBox.Name = "GeneratedPasswordTextBox";
            this.GeneratedPasswordTextBox.Size = new System.Drawing.Size(229, 30);
            this.GeneratedPasswordTextBox.TabIndex = 25;
            this.GeneratedPasswordTextBox.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(79, 419);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(326, 58);
            this.button23.TabIndex = 24;
            this.button23.Text = "Open LastPass";
            this.button23.UseVisualStyleBackColor = true;
            this.button23.Click += new System.EventHandler(this.button23_Click);
            // 
            // webBrowser1
            // 
            this.webBrowser1.Location = new System.Drawing.Point(500, 8);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.ScriptErrorsSuppressed = true;
            this.webBrowser1.Size = new System.Drawing.Size(665, 477);
            this.webBrowser1.TabIndex = 23;
            this.webBrowser1.Url = new System.Uri("https://passwordsgenerator.net/", System.UriKind.Absolute);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Secure_File_Vault.Properties.Resources.password_slide;
            this.pictureBox2.Location = new System.Drawing.Point(41, 302);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(426, 100);
            this.pictureBox2.TabIndex = 22;
            this.pictureBox2.TabStop = false;
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(11, 491);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(199, 45);
            this.button10.TabIndex = 21;
            this.button10.Text = "< Back";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(965, 491);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(205, 50);
            this.button8.TabIndex = 16;
            this.button8.Text = "Next >";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // CreatePIMPanel
            // 
            this.CreatePIMPanel.BackColor = System.Drawing.Color.Brown;
            this.CreatePIMPanel.Controls.Add(this.label28);
            this.CreatePIMPanel.Controls.Add(this.pictureBox7);
            this.CreatePIMPanel.Controls.Add(this.label22);
            this.CreatePIMPanel.Controls.Add(this.pictureBox5);
            this.CreatePIMPanel.Controls.Add(this.button13);
            this.CreatePIMPanel.Controls.Add(this.button14);
            this.CreatePIMPanel.Controls.Add(this.label3);
            this.CreatePIMPanel.Controls.Add(this.PIM);
            this.CreatePIMPanel.Location = new System.Drawing.Point(0, 0);
            this.CreatePIMPanel.Name = "CreatePIMPanel";
            this.CreatePIMPanel.Size = new System.Drawing.Size(1173, 557);
            this.CreatePIMPanel.TabIndex = 16;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(353, 86);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(190, 25);
            this.label28.TabIndex = 29;
            this.label28.Text = "Enter PIM (Optional)";
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = global::Secure_File_Vault.Properties.Resources.download;
            this.pictureBox7.Location = new System.Drawing.Point(834, 175);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(258, 227);
            this.pictureBox7.TabIndex = 28;
            this.pictureBox7.TabStop = false;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(320, 207);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(462, 75);
            this.label22.TabIndex = 27;
            this.label22.Text = "The default PIM is 500 if you leave the textbox blank\r\n\r\nthe PIM must be 500 or h" +
    "igher\r\n";
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::Secure_File_Vault.Properties.Resources.pim_slide;
            this.pictureBox5.Location = new System.Drawing.Point(375, 345);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(419, 191);
            this.pictureBox5.TabIndex = 26;
            this.pictureBox5.TabStop = false;
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(11, 491);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(199, 53);
            this.button13.TabIndex = 25;
            this.button13.Text = "< Back";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(980, 500);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(188, 44);
            this.button14.TabIndex = 24;
            this.button14.Text = "Next >";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.OpenSecuritySettingsPanel);
            this.tabPage3.Controls.Add(this.OpenDriveLetterPanel);
            this.tabPage3.Controls.Add(this.OpenLocationPanel);
            this.tabPage3.Controls.Add(this.OpenPIMPanel);
            this.tabPage3.Controls.Add(this.OpenPasswordPanel);
            this.tabPage3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1217, 552);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Open File Vault";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // OpenSecuritySettingsPanel
            // 
            this.OpenSecuritySettingsPanel.Controls.Add(this.panel3);
            this.OpenSecuritySettingsPanel.Controls.Add(this.button29);
            this.OpenSecuritySettingsPanel.Controls.Add(this.button28);
            this.OpenSecuritySettingsPanel.Location = new System.Drawing.Point(0, 0);
            this.OpenSecuritySettingsPanel.Name = "OpenSecuritySettingsPanel";
            this.OpenSecuritySettingsPanel.Size = new System.Drawing.Size(1147, 537);
            this.OpenSecuritySettingsPanel.TabIndex = 0;
            this.OpenSecuritySettingsPanel.Visible = false;
            // 
            // panel3
            // 
            this.panel3.AutoScroll = true;
            this.panel3.Controls.Add(this.OpenUniqueEncryption);
            this.panel3.Controls.Add(this.OpenTwoStepVerificationPanel);
            this.panel3.Controls.Add(this.OpenROBLOXModelPanel);
            this.panel3.Controls.Add(this.OpenROBLOXModelCheckBox);
            this.panel3.Controls.Add(this.button41);
            this.panel3.Controls.Add(this.OpenTwoStepVerificationCheckBox);
            this.panel3.Controls.Add(this.button36);
            this.panel3.Controls.Add(this.OpenKeyFilesBrowseButton);
            this.panel3.Controls.Add(this.OpenKeyFilesListBox);
            this.panel3.Controls.Add(this.OpenKeyFilesBrowseTextbox);
            this.panel3.Controls.Add(this.OpenKeyFilesCheckbox);
            this.panel3.Controls.Add(this.OpenNewHashingCheckBox);
            this.panel3.Controls.Add(this.SecurityPINOpenTextBox);
            this.panel3.Controls.Add(this.SecurityPINOpenCheckbox);
            this.panel3.Controls.Add(this.UltraSecurityOpenCheckBox);
            this.panel3.Controls.Add(this.ExtraSecurityModeOpen);
            this.panel3.Location = new System.Drawing.Point(9, 11);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1135, 470);
            this.panel3.TabIndex = 65;
            // 
            // OpenTwoStepVerificationPanel
            // 
            this.OpenTwoStepVerificationPanel.Controls.Add(this.button39);
            this.OpenTwoStepVerificationPanel.Controls.Add(this.button40);
            this.OpenTwoStepVerificationPanel.Controls.Add(this.label35);
            this.OpenTwoStepVerificationPanel.Controls.Add(this.OpenVerificationCodeTextBox);
            this.OpenTwoStepVerificationPanel.Controls.Add(this.label36);
            this.OpenTwoStepVerificationPanel.Controls.Add(this.OpenEmailAddressTextBox);
            this.OpenTwoStepVerificationPanel.Location = new System.Drawing.Point(590, 5);
            this.OpenTwoStepVerificationPanel.Name = "OpenTwoStepVerificationPanel";
            this.OpenTwoStepVerificationPanel.Size = new System.Drawing.Size(296, 211);
            this.OpenTwoStepVerificationPanel.TabIndex = 53;
            this.OpenTwoStepVerificationPanel.Visible = false;
            // 
            // button39
            // 
            this.button39.Location = new System.Drawing.Point(4, 162);
            this.button39.Name = "button39";
            this.button39.Size = new System.Drawing.Size(288, 33);
            this.button39.TabIndex = 51;
            this.button39.Text = "Verify";
            this.button39.UseVisualStyleBackColor = true;
            this.button39.Click += new System.EventHandler(this.button39_Click);
            // 
            // button40
            // 
            this.button40.Location = new System.Drawing.Point(68, 65);
            this.button40.Name = "button40";
            this.button40.Size = new System.Drawing.Size(152, 34);
            this.button40.TabIndex = 50;
            this.button40.Text = "Get Code";
            this.button40.UseVisualStyleBackColor = true;
            this.button40.Click += new System.EventHandler(this.button40_Click);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(47, 98);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(203, 25);
            this.label35.TabIndex = 49;
            this.label35.Text = "Enter verification code";
            // 
            // OpenVerificationCodeTextBox
            // 
            this.OpenVerificationCodeTextBox.Location = new System.Drawing.Point(12, 126);
            this.OpenVerificationCodeTextBox.Name = "OpenVerificationCodeTextBox";
            this.OpenVerificationCodeTextBox.Size = new System.Drawing.Size(265, 30);
            this.OpenVerificationCodeTextBox.TabIndex = 48;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(25, 5);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(233, 25);
            this.label36.TabIndex = 47;
            this.label36.Text = "Enter your email address:";
            // 
            // OpenEmailAddressTextBox
            // 
            this.OpenEmailAddressTextBox.Location = new System.Drawing.Point(12, 33);
            this.OpenEmailAddressTextBox.Name = "OpenEmailAddressTextBox";
            this.OpenEmailAddressTextBox.Size = new System.Drawing.Size(265, 30);
            this.OpenEmailAddressTextBox.TabIndex = 46;
            // 
            // OpenROBLOXModelPanel
            // 
            this.OpenROBLOXModelPanel.Controls.Add(this.button46);
            this.OpenROBLOXModelPanel.Controls.Add(this.button47);
            this.OpenROBLOXModelPanel.Controls.Add(this.OpenROBLOXModelListBox);
            this.OpenROBLOXModelPanel.Controls.Add(this.button48);
            this.OpenROBLOXModelPanel.Controls.Add(this.OpenROBLOXModelBrowseTextBox);
            this.OpenROBLOXModelPanel.Location = new System.Drawing.Point(586, 291);
            this.OpenROBLOXModelPanel.Name = "OpenROBLOXModelPanel";
            this.OpenROBLOXModelPanel.Size = new System.Drawing.Size(531, 201);
            this.OpenROBLOXModelPanel.TabIndex = 64;
            this.OpenROBLOXModelPanel.Visible = false;
            // 
            // button46
            // 
            this.button46.Location = new System.Drawing.Point(380, 55);
            this.button46.Name = "button46";
            this.button46.Size = new System.Drawing.Size(130, 32);
            this.button46.TabIndex = 59;
            this.button46.Text = "Add";
            this.button46.UseVisualStyleBackColor = true;
            this.button46.Click += new System.EventHandler(this.button46_Click);
            // 
            // button47
            // 
            this.button47.Location = new System.Drawing.Point(147, 152);
            this.button47.Name = "button47";
            this.button47.Size = new System.Drawing.Size(200, 40);
            this.button47.TabIndex = 60;
            this.button47.Text = "Done";
            this.button47.UseVisualStyleBackColor = true;
            this.button47.Click += new System.EventHandler(this.button47_Click);
            // 
            // OpenROBLOXModelListBox
            // 
            this.OpenROBLOXModelListBox.FormattingEnabled = true;
            this.OpenROBLOXModelListBox.ItemHeight = 25;
            this.OpenROBLOXModelListBox.Location = new System.Drawing.Point(7, 42);
            this.OpenROBLOXModelListBox.Name = "OpenROBLOXModelListBox";
            this.OpenROBLOXModelListBox.Size = new System.Drawing.Size(369, 104);
            this.OpenROBLOXModelListBox.TabIndex = 58;
            // 
            // button48
            // 
            this.button48.Location = new System.Drawing.Point(380, 0);
            this.button48.Name = "button48";
            this.button48.Size = new System.Drawing.Size(130, 32);
            this.button48.TabIndex = 57;
            this.button48.Text = "Browse";
            this.button48.UseVisualStyleBackColor = true;
            this.button48.Click += new System.EventHandler(this.button48_Click);
            // 
            // OpenROBLOXModelBrowseTextBox
            // 
            this.OpenROBLOXModelBrowseTextBox.Location = new System.Drawing.Point(6, 1);
            this.OpenROBLOXModelBrowseTextBox.Name = "OpenROBLOXModelBrowseTextBox";
            this.OpenROBLOXModelBrowseTextBox.Size = new System.Drawing.Size(369, 30);
            this.OpenROBLOXModelBrowseTextBox.TabIndex = 56;
            // 
            // OpenROBLOXModelCheckBox
            // 
            this.OpenROBLOXModelCheckBox.AutoSize = true;
            this.OpenROBLOXModelCheckBox.BackColor = System.Drawing.Color.Transparent;
            this.OpenROBLOXModelCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OpenROBLOXModelCheckBox.ForeColor = System.Drawing.Color.Black;
            this.OpenROBLOXModelCheckBox.Location = new System.Drawing.Point(620, 239);
            this.OpenROBLOXModelCheckBox.Name = "OpenROBLOXModelCheckBox";
            this.OpenROBLOXModelCheckBox.Size = new System.Drawing.Size(362, 35);
            this.OpenROBLOXModelCheckBox.TabIndex = 62;
            this.OpenROBLOXModelCheckBox.Text = "ROBLOX Model Encryption";
            this.OpenROBLOXModelCheckBox.UseVisualStyleBackColor = false;
            this.OpenROBLOXModelCheckBox.CheckedChanged += new System.EventHandler(this.OpenROBLOXModelCheckBox_CheckedChanged);
            // 
            // button41
            // 
            this.button41.Location = new System.Drawing.Point(17, 317);
            this.button41.Name = "button41";
            this.button41.Size = new System.Drawing.Size(197, 35);
            this.button41.TabIndex = 55;
            this.button41.Text = "Create a new keyfile";
            this.button41.UseVisualStyleBackColor = true;
            this.button41.Click += new System.EventHandler(this.button41_Click);
            // 
            // OpenTwoStepVerificationCheckBox
            // 
            this.OpenTwoStepVerificationCheckBox.AutoSize = true;
            this.OpenTwoStepVerificationCheckBox.Enabled = false;
            this.OpenTwoStepVerificationCheckBox.Location = new System.Drawing.Point(45, 99);
            this.OpenTwoStepVerificationCheckBox.Name = "OpenTwoStepVerificationCheckBox";
            this.OpenTwoStepVerificationCheckBox.Size = new System.Drawing.Size(191, 29);
            this.OpenTwoStepVerificationCheckBox.TabIndex = 54;
            this.OpenTwoStepVerificationCheckBox.Text = "2-Step Verification";
            this.OpenTwoStepVerificationCheckBox.UseVisualStyleBackColor = true;
            this.OpenTwoStepVerificationCheckBox.CheckedChanged += new System.EventHandler(this.OpenTwoStepVerificationCheckBox_CheckedChanged);
            // 
            // button36
            // 
            this.button36.Location = new System.Drawing.Point(433, 239);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(138, 41);
            this.button36.TabIndex = 45;
            this.button36.Text = "Add";
            this.button36.UseVisualStyleBackColor = true;
            this.button36.Click += new System.EventHandler(this.button36_Click);
            // 
            // OpenKeyFilesBrowseButton
            // 
            this.OpenKeyFilesBrowseButton.Location = new System.Drawing.Point(433, 164);
            this.OpenKeyFilesBrowseButton.Name = "OpenKeyFilesBrowseButton";
            this.OpenKeyFilesBrowseButton.Size = new System.Drawing.Size(138, 41);
            this.OpenKeyFilesBrowseButton.TabIndex = 44;
            this.OpenKeyFilesBrowseButton.Text = "Browse";
            this.OpenKeyFilesBrowseButton.UseVisualStyleBackColor = true;
            this.OpenKeyFilesBrowseButton.Click += new System.EventHandler(this.OpenKeyFilesBrowseButton_Click);
            // 
            // OpenKeyFilesListBox
            // 
            this.OpenKeyFilesListBox.FormattingEnabled = true;
            this.OpenKeyFilesListBox.ItemHeight = 25;
            this.OpenKeyFilesListBox.Location = new System.Drawing.Point(17, 207);
            this.OpenKeyFilesListBox.Name = "OpenKeyFilesListBox";
            this.OpenKeyFilesListBox.Size = new System.Drawing.Size(389, 104);
            this.OpenKeyFilesListBox.TabIndex = 43;
            // 
            // OpenKeyFilesBrowseTextbox
            // 
            this.OpenKeyFilesBrowseTextbox.Location = new System.Drawing.Point(17, 169);
            this.OpenKeyFilesBrowseTextbox.Name = "OpenKeyFilesBrowseTextbox";
            this.OpenKeyFilesBrowseTextbox.Size = new System.Drawing.Size(389, 30);
            this.OpenKeyFilesBrowseTextbox.TabIndex = 42;
            // 
            // OpenKeyFilesCheckbox
            // 
            this.OpenKeyFilesCheckbox.AutoSize = true;
            this.OpenKeyFilesCheckbox.Location = new System.Drawing.Point(17, 137);
            this.OpenKeyFilesCheckbox.Name = "OpenKeyFilesCheckbox";
            this.OpenKeyFilesCheckbox.Size = new System.Drawing.Size(100, 29);
            this.OpenKeyFilesCheckbox.TabIndex = 41;
            this.OpenKeyFilesCheckbox.Text = "Keyfiles";
            this.OpenKeyFilesCheckbox.UseVisualStyleBackColor = true;
            // 
            // OpenNewHashingCheckBox
            // 
            this.OpenNewHashingCheckBox.AutoSize = true;
            this.OpenNewHashingCheckBox.Location = new System.Drawing.Point(302, 6);
            this.OpenNewHashingCheckBox.Name = "OpenNewHashingCheckBox";
            this.OpenNewHashingCheckBox.Size = new System.Drawing.Size(239, 29);
            this.OpenNewHashingCheckBox.TabIndex = 40;
            this.OpenNewHashingCheckBox.Text = "New Hashing Protection";
            this.OpenNewHashingCheckBox.UseVisualStyleBackColor = true;
            // 
            // SecurityPINOpenTextBox
            // 
            this.SecurityPINOpenTextBox.Enabled = false;
            this.SecurityPINOpenTextBox.Location = new System.Drawing.Point(192, 63);
            this.SecurityPINOpenTextBox.Name = "SecurityPINOpenTextBox";
            this.SecurityPINOpenTextBox.Size = new System.Drawing.Size(71, 30);
            this.SecurityPINOpenTextBox.TabIndex = 37;
            // 
            // SecurityPINOpenCheckbox
            // 
            this.SecurityPINOpenCheckbox.AutoSize = true;
            this.SecurityPINOpenCheckbox.Enabled = false;
            this.SecurityPINOpenCheckbox.Location = new System.Drawing.Point(45, 65);
            this.SecurityPINOpenCheckbox.Name = "SecurityPINOpenCheckbox";
            this.SecurityPINOpenCheckbox.Size = new System.Drawing.Size(139, 29);
            this.SecurityPINOpenCheckbox.TabIndex = 36;
            this.SecurityPINOpenCheckbox.Text = "Security PIN";
            this.SecurityPINOpenCheckbox.UseVisualStyleBackColor = true;
            this.SecurityPINOpenCheckbox.CheckedChanged += new System.EventHandler(this.SecurityPINOpenCheckbox_CheckedChanged);
            // 
            // UltraSecurityOpenCheckBox
            // 
            this.UltraSecurityOpenCheckBox.AutoSize = true;
            this.UltraSecurityOpenCheckBox.Location = new System.Drawing.Point(17, 33);
            this.UltraSecurityOpenCheckBox.Name = "UltraSecurityOpenCheckBox";
            this.UltraSecurityOpenCheckBox.Size = new System.Drawing.Size(177, 29);
            this.UltraSecurityOpenCheckBox.TabIndex = 33;
            this.UltraSecurityOpenCheckBox.Text = "ANTI FBI MODE";
            this.UltraSecurityOpenCheckBox.UseVisualStyleBackColor = true;
            this.UltraSecurityOpenCheckBox.CheckedChanged += new System.EventHandler(this.UltraSecurityOpenCheckBox_CheckedChanged);
            // 
            // ExtraSecurityModeOpen
            // 
            this.ExtraSecurityModeOpen.AutoSize = true;
            this.ExtraSecurityModeOpen.Location = new System.Drawing.Point(17, 4);
            this.ExtraSecurityModeOpen.Name = "ExtraSecurityModeOpen";
            this.ExtraSecurityModeOpen.Size = new System.Drawing.Size(207, 29);
            this.ExtraSecurityModeOpen.TabIndex = 31;
            this.ExtraSecurityModeOpen.Text = "Extra Security Mode";
            this.ExtraSecurityModeOpen.UseVisualStyleBackColor = true;
            this.ExtraSecurityModeOpen.CheckedChanged += new System.EventHandler(this.ExtraSecurityModeOpen_CheckedChanged);
            // 
            // button29
            // 
            this.button29.Location = new System.Drawing.Point(17, 482);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(206, 55);
            this.button29.TabIndex = 39;
            this.button29.Text = "< Back";
            this.button29.UseVisualStyleBackColor = true;
            this.button29.Click += new System.EventHandler(this.button29_Click);
            // 
            // button28
            // 
            this.button28.Location = new System.Drawing.Point(238, 482);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(206, 55);
            this.button28.TabIndex = 38;
            this.button28.Text = "Next >";
            this.button28.UseVisualStyleBackColor = true;
            this.button28.Click += new System.EventHandler(this.button28_Click);
            // 
            // OpenDriveLetterPanel
            // 
            this.OpenDriveLetterPanel.Controls.Add(this.button27);
            this.OpenDriveLetterPanel.Controls.Add(this.button26);
            this.OpenDriveLetterPanel.Controls.Add(this.ThirdOpenLetter);
            this.OpenDriveLetterPanel.Controls.Add(this.label12);
            this.OpenDriveLetterPanel.Controls.Add(this.label19);
            this.OpenDriveLetterPanel.Controls.Add(this.label13);
            this.OpenDriveLetterPanel.Controls.Add(this.label14);
            this.OpenDriveLetterPanel.Controls.Add(this.OpenSecondLetter);
            this.OpenDriveLetterPanel.Controls.Add(this.OpenFirstLetter);
            this.OpenDriveLetterPanel.Location = new System.Drawing.Point(0, 0);
            this.OpenDriveLetterPanel.Name = "OpenDriveLetterPanel";
            this.OpenDriveLetterPanel.Size = new System.Drawing.Size(1147, 537);
            this.OpenDriveLetterPanel.TabIndex = 1;
            this.OpenDriveLetterPanel.Visible = false;
            // 
            // button27
            // 
            this.button27.Location = new System.Drawing.Point(17, 476);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(226, 47);
            this.button27.TabIndex = 37;
            this.button27.Text = "< Back";
            this.button27.UseVisualStyleBackColor = true;
            this.button27.Click += new System.EventHandler(this.button27_Click);
            // 
            // button26
            // 
            this.button26.Location = new System.Drawing.Point(906, 475);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(226, 47);
            this.button26.TabIndex = 36;
            this.button26.Text = "Next >";
            this.button26.UseVisualStyleBackColor = true;
            this.button26.Click += new System.EventHandler(this.button26_Click);
            // 
            // ThirdOpenLetter
            // 
            this.ThirdOpenLetter.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.ThirdOpenLetter.Location = new System.Drawing.Point(172, 157);
            this.ThirdOpenLetter.MaxLength = 1;
            this.ThirdOpenLetter.Name = "ThirdOpenLetter";
            this.ThirdOpenLetter.Size = new System.Drawing.Size(101, 30);
            this.ThirdOpenLetter.TabIndex = 34;
            this.ThirdOpenLetter.TextChanged += new System.EventHandler(this.ThirdOpenLetter_TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(86, 125);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(80, 25);
            this.label12.TabIndex = 29;
            this.label12.Text = "Second";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(109, 160);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(57, 25);
            this.label19.TabIndex = 35;
            this.label19.Text = "Third";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(117, 88);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(49, 25);
            this.label13.TabIndex = 28;
            this.label13.Text = "First";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(21, 57);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(290, 25);
            this.label14.TabIndex = 26;
            this.label14.Text = "Choose 3 drive letters not in use";
            // 
            // OpenSecondLetter
            // 
            this.OpenSecondLetter.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.OpenSecondLetter.Location = new System.Drawing.Point(172, 121);
            this.OpenSecondLetter.MaxLength = 1;
            this.OpenSecondLetter.Name = "OpenSecondLetter";
            this.OpenSecondLetter.Size = new System.Drawing.Size(101, 30);
            this.OpenSecondLetter.TabIndex = 27;
            this.OpenSecondLetter.TextChanged += new System.EventHandler(this.OpenSecondLetter_TextChanged);
            // 
            // OpenFirstLetter
            // 
            this.OpenFirstLetter.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.OpenFirstLetter.Location = new System.Drawing.Point(172, 85);
            this.OpenFirstLetter.MaxLength = 1;
            this.OpenFirstLetter.Name = "OpenFirstLetter";
            this.OpenFirstLetter.Size = new System.Drawing.Size(101, 30);
            this.OpenFirstLetter.TabIndex = 25;
            this.OpenFirstLetter.TextChanged += new System.EventHandler(this.OpenFirstLetter_TextChanged);
            // 
            // OpenLocationPanel
            // 
            this.OpenLocationPanel.Controls.Add(this.button49);
            this.OpenLocationPanel.Controls.Add(this.button25);
            this.OpenLocationPanel.Controls.Add(this.OpenVaultDirectory);
            this.OpenLocationPanel.Controls.Add(this.button7);
            this.OpenLocationPanel.Controls.Add(this.label11);
            this.OpenLocationPanel.Location = new System.Drawing.Point(0, 0);
            this.OpenLocationPanel.Name = "OpenLocationPanel";
            this.OpenLocationPanel.Size = new System.Drawing.Size(1147, 537);
            this.OpenLocationPanel.TabIndex = 3;
            this.OpenLocationPanel.Visible = false;
            // 
            // button49
            // 
            this.button49.Location = new System.Drawing.Point(356, 127);
            this.button49.Name = "button49";
            this.button49.Size = new System.Drawing.Size(394, 60);
            this.button49.TabIndex = 9;
            this.button49.Text = "Click to drag and drop file";
            this.button49.UseVisualStyleBackColor = true;
            this.button49.Click += new System.EventHandler(this.button49_Click);
            // 
            // button25
            // 
            this.button25.Location = new System.Drawing.Point(938, 481);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(195, 42);
            this.button25.TabIndex = 8;
            this.button25.Text = "Next";
            this.button25.UseVisualStyleBackColor = true;
            this.button25.Click += new System.EventHandler(this.button25_Click_1);
            // 
            // OpenVaultDirectory
            // 
            this.OpenVaultDirectory.Location = new System.Drawing.Point(134, 31);
            this.OpenVaultDirectory.Name = "OpenVaultDirectory";
            this.OpenVaultDirectory.Size = new System.Drawing.Size(946, 30);
            this.OpenVaultDirectory.TabIndex = 6;
            this.OpenVaultDirectory.TextChanged += new System.EventHandler(this.OpenVaultDirectory_TextChanged);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(11, 24);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(111, 41);
            this.button7.TabIndex = 5;
            this.button7.Text = "Browse";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(450, 3);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(200, 25);
            this.label11.TabIndex = 7;
            this.label11.Text = "Select Vault To Open";
            // 
            // OpenPIMPanel
            // 
            this.OpenPIMPanel.Controls.Add(this.button31);
            this.OpenPIMPanel.Controls.Add(this.button30);
            this.OpenPIMPanel.Controls.Add(this.label6);
            this.OpenPIMPanel.Controls.Add(this.OpenVaultPIM);
            this.OpenPIMPanel.Location = new System.Drawing.Point(0, 0);
            this.OpenPIMPanel.Name = "OpenPIMPanel";
            this.OpenPIMPanel.Size = new System.Drawing.Size(1147, 538);
            this.OpenPIMPanel.TabIndex = 4;
            this.OpenPIMPanel.Visible = false;
            // 
            // button31
            // 
            this.button31.Location = new System.Drawing.Point(26, 474);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(187, 52);
            this.button31.TabIndex = 6;
            this.button31.Text = "< Back";
            this.button31.UseVisualStyleBackColor = true;
            this.button31.Click += new System.EventHandler(this.button31_Click);
            // 
            // button30
            // 
            this.button30.Location = new System.Drawing.Point(925, 474);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(187, 52);
            this.button30.TabIndex = 5;
            this.button30.Text = "Next >";
            this.button30.UseVisualStyleBackColor = true;
            this.button30.Click += new System.EventHandler(this.button30_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(57, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 25);
            this.label6.TabIndex = 4;
            this.label6.Text = "Vault PIM";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // OpenVaultPIM
            // 
            this.OpenVaultPIM.Location = new System.Drawing.Point(62, 48);
            this.OpenVaultPIM.Name = "OpenVaultPIM";
            this.OpenVaultPIM.Size = new System.Drawing.Size(78, 30);
            this.OpenVaultPIM.TabIndex = 3;
            this.OpenVaultPIM.TextChanged += new System.EventHandler(this.OpenVaultPIM_TextChanged);
            // 
            // OpenPasswordPanel
            // 
            this.OpenPasswordPanel.Controls.Add(this.button32);
            this.OpenPasswordPanel.Controls.Add(this.button6);
            this.OpenPasswordPanel.Controls.Add(this.PasswordOpen);
            this.OpenPasswordPanel.Controls.Add(this.label5);
            this.OpenPasswordPanel.Controls.Add(this.label16);
            this.OpenPasswordPanel.Controls.Add(this.checkBox2);
            this.OpenPasswordPanel.Location = new System.Drawing.Point(0, 0);
            this.OpenPasswordPanel.Name = "OpenPasswordPanel";
            this.OpenPasswordPanel.Size = new System.Drawing.Size(1147, 538);
            this.OpenPasswordPanel.TabIndex = 2;
            this.OpenPasswordPanel.Visible = false;
            // 
            // button32
            // 
            this.button32.Location = new System.Drawing.Point(21, 480);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(211, 46);
            this.button32.TabIndex = 33;
            this.button32.Text = "< Back";
            this.button32.UseVisualStyleBackColor = true;
            this.button32.Click += new System.EventHandler(this.button32_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(347, 153);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(460, 57);
            this.button6.TabIndex = 0;
            this.button6.Text = "Open";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // PasswordOpen
            // 
            this.PasswordOpen.Location = new System.Drawing.Point(347, 115);
            this.PasswordOpen.Name = "PasswordOpen";
            this.PasswordOpen.Size = new System.Drawing.Size(467, 30);
            this.PasswordOpen.TabIndex = 1;
            this.PasswordOpen.UseSystemPasswordChar = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(502, 88);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(148, 25);
            this.label5.TabIndex = 2;
            this.label5.Text = "Vault Password";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(367, 215);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(461, 75);
            this.label16.TabIndex = 32;
            this.label16.Text = "Your computer will be locked when opening file vault\r\nso the virtual disk creatio" +
    "n/decryption works without\r\nerrors\r\n";
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(656, 80);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(172, 29);
            this.checkBox2.TabIndex = 30;
            this.checkBox2.Text = "Show Password";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // ManageVaultPanel
            // 
            this.ManageVaultPanel.Controls.Add(this.CloseAndLockVaultTimeLabel);
            this.ManageVaultPanel.Controls.Add(this.pictureBox6);
            this.ManageVaultPanel.Controls.Add(this.label21);
            this.ManageVaultPanel.Controls.Add(this.label20);
            this.ManageVaultPanel.Controls.Add(this.button5);
            this.ManageVaultPanel.Controls.Add(this.button3);
            this.ManageVaultPanel.Location = new System.Drawing.Point(12, 57);
            this.ManageVaultPanel.Name = "ManageVaultPanel";
            this.ManageVaultPanel.Size = new System.Drawing.Size(1200, 356);
            this.ManageVaultPanel.TabIndex = 0;
            this.ManageVaultPanel.Visible = false;
            // 
            // CloseAndLockVaultTimeLabel
            // 
            this.CloseAndLockVaultTimeLabel.AutoSize = true;
            this.CloseAndLockVaultTimeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CloseAndLockVaultTimeLabel.Location = new System.Drawing.Point(96, 140);
            this.CloseAndLockVaultTimeLabel.Name = "CloseAndLockVaultTimeLabel";
            this.CloseAndLockVaultTimeLabel.Size = new System.Drawing.Size(356, 31);
            this.CloseAndLockVaultTimeLabel.TabIndex = 5;
            this.CloseAndLockVaultTimeLabel.Text = "Time to close and lock vault:";
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::Secure_File_Vault.Properties.Resources._lock;
            this.pictureBox6.Location = new System.Drawing.Point(860, 34);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(262, 285);
            this.pictureBox6.TabIndex = 4;
            this.pictureBox6.TabStop = false;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(539, 242);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(254, 65);
            this.label21.TabIndex = 3;
            this.label21.Text = "Closes the file vault and begins the\r\nencryption process\r\n\r\nThe process would tak" +
    "e depending on what security\r\nsettings you have selected\r\n";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(539, 101);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(210, 13);
            this.label20.TabIndex = 2;
            this.label20.Text = "Open the current file vault that is decrypted";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(50, 260);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(433, 59);
            this.button5.TabIndex = 1;
            this.button5.Text = "Close and Lock File Vault";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(50, 78);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(433, 59);
            this.button3.TabIndex = 0;
            this.button3.Text = "Open File Vault";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.linkLabel1.LinkColor = System.Drawing.Color.Red;
            this.linkLabel1.Location = new System.Drawing.Point(493, 11);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(194, 31);
            this.linkLabel1.TabIndex = 1;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Close Program";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // StatusLabel
            // 
            this.StatusLabel.AutoSize = true;
            this.StatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StatusLabel.Location = new System.Drawing.Point(704, 6);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(0, 46);
            this.StatusLabel.TabIndex = 19;
            // 
            // HideShowButton
            // 
            this.HideShowButton.Location = new System.Drawing.Point(268, 11);
            this.HideShowButton.Name = "HideShowButton";
            this.HideShowButton.Size = new System.Drawing.Size(191, 40);
            this.HideShowButton.TabIndex = 20;
            this.HideShowButton.Text = "Hide/Show Screen";
            this.HideShowButton.UseVisualStyleBackColor = true;
            this.HideShowButton.Visible = false;
            this.HideShowButton.Click += new System.EventHandler(this.button25_Click);
            // 
            // StatusProgressBar
            // 
            this.StatusProgressBar.Location = new System.Drawing.Point(15, 641);
            this.StatusProgressBar.Name = "StatusProgressBar";
            this.StatusProgressBar.Size = new System.Drawing.Size(1177, 33);
            this.StatusProgressBar.TabIndex = 21;
            // 
            // linkLabel3
            // 
            this.linkLabel3.AutoSize = true;
            this.linkLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel3.Location = new System.Drawing.Point(921, 5);
            this.linkLabel3.Name = "linkLabel3";
            this.linkLabel3.Size = new System.Drawing.Size(271, 46);
            this.linkLabel3.TabIndex = 22;
            this.linkLabel3.TabStop = true;
            this.linkLabel3.Text = "Privacy Policy";
            this.linkLabel3.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel3_LinkClicked);
            // 
            // CoronavirusLabel
            // 
            this.CoronavirusLabel.AutoSize = true;
            this.CoronavirusLabel.BackColor = System.Drawing.Color.Yellow;
            this.CoronavirusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.CoronavirusLabel.Location = new System.Drawing.Point(46, 123);
            this.CoronavirusLabel.Name = "CoronavirusLabel";
            this.CoronavirusLabel.Size = new System.Drawing.Size(126, 31);
            this.CoronavirusLabel.TabIndex = 23;
            this.CoronavirusLabel.Text = "Loading..";
            // 
            // CreateUniqueEncryption
            // 
            this.CreateUniqueEncryption.AutoSize = true;
            this.CreateUniqueEncryption.BackColor = System.Drawing.Color.Maroon;
            this.CreateUniqueEncryption.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreateUniqueEncryption.ForeColor = System.Drawing.Color.Yellow;
            this.CreateUniqueEncryption.Location = new System.Drawing.Point(564, 522);
            this.CreateUniqueEncryption.Name = "CreateUniqueEncryption";
            this.CreateUniqueEncryption.Size = new System.Drawing.Size(380, 35);
            this.CreateUniqueEncryption.TabIndex = 63;
            this.CreateUniqueEncryption.Text = "Unique-Encryption-Algorithm";
            this.CreateUniqueEncryption.UseVisualStyleBackColor = false;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(559, 569);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(453, 125);
            this.label38.TabIndex = 64;
            this.label38.Text = resources.GetString("label38.Text");
            // 
            // OpenUniqueEncryption
            // 
            this.OpenUniqueEncryption.AutoSize = true;
            this.OpenUniqueEncryption.Location = new System.Drawing.Point(302, 37);
            this.OpenUniqueEncryption.Name = "OpenUniqueEncryption";
            this.OpenUniqueEncryption.Size = new System.Drawing.Size(281, 29);
            this.OpenUniqueEncryption.TabIndex = 65;
            this.OpenUniqueEncryption.Text = "Unique-Encryption-Algorithm";
            this.OpenUniqueEncryption.UseVisualStyleBackColor = true;
            // 
            // Create
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Highlight;
            this.ClientSize = new System.Drawing.Size(1210, 685);
            this.Controls.Add(this.linkLabel3);
            this.Controls.Add(this.StatusProgressBar);
            this.Controls.Add(this.HideShowButton);
            this.Controls.Add(this.StatusLabel);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.MainTabs);
            this.Controls.Add(this.ManageVaultPanel);
            this.Controls.Add(this.CoronavirusLabel);
            this.Name = "Create";
            this.ShowIcon = false;
            this.Load += new System.EventHandler(this.Create_Load);
            this.MainTabs.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.CreateSecuritySettings.ResumeLayout(false);
            this.CreateSecuritySettings.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.CreateROBLOXPanel.ResumeLayout(false);
            this.CreateROBLOXPanel.PerformLayout();
            this.CreateTwoStepVerificationPanel.ResumeLayout(false);
            this.CreateTwoStepVerificationPanel.PerformLayout();
            this.CreateDriveLetterPanel.ResumeLayout(false);
            this.CreateDriveLetterPanel.PerformLayout();
            this.CreationPanel.ResumeLayout(false);
            this.CreationPanel.PerformLayout();
            this.CreateVaultLocationPanel.ResumeLayout(false);
            this.CreateVaultLocationPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.DragAndDropCreate.ResumeLayout(false);
            this.DragAndDropCreate.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.CreateVaultSize.ResumeLayout(false);
            this.CreateVaultSize.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.CreatePasswordPanel.ResumeLayout(false);
            this.CreatePasswordPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.CreatePIMPanel.ResumeLayout(false);
            this.CreatePIMPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.OpenSecuritySettingsPanel.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.OpenTwoStepVerificationPanel.ResumeLayout(false);
            this.OpenTwoStepVerificationPanel.PerformLayout();
            this.OpenROBLOXModelPanel.ResumeLayout(false);
            this.OpenROBLOXModelPanel.PerformLayout();
            this.OpenDriveLetterPanel.ResumeLayout(false);
            this.OpenDriveLetterPanel.PerformLayout();
            this.OpenLocationPanel.ResumeLayout(false);
            this.OpenLocationPanel.PerformLayout();
            this.OpenPIMPanel.ResumeLayout(false);
            this.OpenPIMPanel.PerformLayout();
            this.OpenPasswordPanel.ResumeLayout(false);
            this.OpenPasswordPanel.PerformLayout();
            this.ManageVaultPanel.ResumeLayout(false);
            this.ManageVaultPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Password;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox PasswordConfirm;
        private System.Windows.Forms.TextBox PIM;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox VaultSize;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel CreateVaultPanel;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox SaveVaultLocation;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox DriveLetter;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox SecondDriveLetter;
        private System.Windows.Forms.TabControl MainTabs;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel ManageVaultPanel;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Panel OpenSecuritySettingsPanel;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox OpenVaultDirectory;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox OpenVaultPIM;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox PasswordOpen;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox OpenSecondLetter;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox OpenFirstLetter;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox ExtraSecurityModeOpen;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.CheckBox ExtraSecurityModeCreate;
        private System.Windows.Forms.CheckBox UltraSecurityModeCreateCheckBox;
        private System.Windows.Forms.CheckBox UltraSecurityOpenCheckBox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox ThirdDriveLetter;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox ThirdOpenLetter;
        private System.Windows.Forms.CheckBox SecurityPINCreateCheckbox;
		private System.Windows.Forms.TextBox SecurityPINOpenTextBox;
		private System.Windows.Forms.CheckBox SecurityPINOpenCheckbox;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.Panel CreatePasswordPanel;
        private System.Windows.Forms.Panel CreateVaultLocationPanel;
        private System.Windows.Forms.Panel CreateVaultSize;
        private System.Windows.Forms.Panel CreateDriveLetterPanel;
        private System.Windows.Forms.Panel CreatePIMPanel;
        private System.Windows.Forms.Panel CreateSecuritySettings;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Panel CreationPanel;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Panel DragAndDropCreate;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Label StatusLabel;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.TextBox GeneratedPasswordTextBox;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.WebBrowser webBrowser2;
        private System.Windows.Forms.Button HideShowButton;
        private System.Windows.Forms.Panel OpenPIMPanel;
        private System.Windows.Forms.Panel OpenLocationPanel;
        private System.Windows.Forms.Panel OpenPasswordPanel;
        private System.Windows.Forms.Panel OpenDriveLetterPanel;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.ProgressBar StatusProgressBar;
        private System.Windows.Forms.Button button33;
        private System.Windows.Forms.Label EncryptionTimeLabel;
        private System.Windows.Forms.Label FinalEncryptionTime;
        private System.Windows.Forms.Label CloseAndLockVaultTimeLabel;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.CheckBox OpenNewHashingCheckBox;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.CheckBox CreateNewHashingCheckBox;
        private System.Windows.Forms.Button AddToListKeyFilesCreateButton;
        private System.Windows.Forms.ListBox CreateKeyFilesListBox;
        private System.Windows.Forms.Button button35;
        private System.Windows.Forms.TextBox CreateKeyfilesBrowseTextBox;
        private System.Windows.Forms.CheckBox CreateKeyfilesCheckbox;
        private System.Windows.Forms.Button button36;
        private System.Windows.Forms.Button OpenKeyFilesBrowseButton;
        private System.Windows.Forms.ListBox OpenKeyFilesListBox;
        private System.Windows.Forms.TextBox OpenKeyFilesBrowseTextbox;
        private System.Windows.Forms.CheckBox OpenKeyFilesCheckbox;
        private System.Windows.Forms.CheckBox TwoStepVerificationCreateCheckBox;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox CreateEmailAddressTextBox;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox CreateVerificationCodeTextBox;
        private System.Windows.Forms.Panel CreateTwoStepVerificationPanel;
        private System.Windows.Forms.Button button38;
        private System.Windows.Forms.Panel OpenTwoStepVerificationPanel;
        private System.Windows.Forms.Button button39;
        private System.Windows.Forms.Button button40;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox OpenVerificationCodeTextBox;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox OpenEmailAddressTextBox;
        private System.Windows.Forms.CheckBox OpenTwoStepVerificationCheckBox;
        private System.Windows.Forms.Button button41;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button42;
        private System.Windows.Forms.Button button43;
        private System.Windows.Forms.ListBox CreateROBLOXModelListBox;
        private System.Windows.Forms.Button button44;
        private System.Windows.Forms.TextBox CreateBrowseROBLOXModelsTextBox;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.CheckBox CreateROBLOXModelCheckBox;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Panel CreateROBLOXPanel;
        private System.Windows.Forms.Button button45;
        private System.Windows.Forms.Panel OpenROBLOXModelPanel;
        private System.Windows.Forms.Button button46;
        private System.Windows.Forms.Button button47;
        private System.Windows.Forms.ListBox OpenROBLOXModelListBox;
        private System.Windows.Forms.Button button48;
        private System.Windows.Forms.TextBox OpenROBLOXModelBrowseTextBox;
        private System.Windows.Forms.CheckBox OpenROBLOXModelCheckBox;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.CheckBox ThreeStepVerificationCreateTextBox;
        private System.Windows.Forms.Button button49;
        private System.Windows.Forms.LinkLabel linkLabel3;
        private System.Windows.Forms.Label CoronavirusLabel;
        private System.Windows.Forms.CheckBox CreateUniqueEncryption;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.CheckBox OpenUniqueEncryption;
    }
}