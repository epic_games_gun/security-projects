﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Google.Authenticator;

namespace Secure_File_Vault
{
    public partial class Create3StepVerification : Form
    {
        public Create3StepVerification()
        {
            InitializeComponent();
            Closing += Create3StepVerification_Closing;
        }

        private bool NeedToClose = false;
        private void Create3StepVerification_Closing(object sender, CancelEventArgs e)
        {
            if (NeedToClose == false)
            {
                e.Cancel = true;
            }
        }

        private string MainDirectory = Environment.GetEnvironmentVariable("TEMP") + "\\FileVault";

        private string Code = string.Empty;
        private async void Create3StepVerification_Load(object sender, EventArgs e)
        {
            FormBorderStyle = FormBorderStyle.FixedSingle;
            try
            {
                TopMost = true;

                string SecretKey()
                {
                    Random Key = new Random();
                    string Return = string.Empty;
                    int Code = Key.Next(11111, 999999999);
                    return Code.ToString() + Code.ToString();
                }

                Code = SecretKey();
                TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
                var setupInfo = tfa.GenerateSetupCode("File Vault", "Unlock-code", Code, false, 100);
                File.WriteAllText(MainDirectory + "\\Image.txt",setupInfo.QrCodeSetupImageUrl);
                string BASE64 =
                    File.ReadAllText(MainDirectory + "\\Image.txt")
                        .Split(',')[1].Trim();
                File.WriteAllText(MainDirectory + "\\ImageBASE64.txt",BASE64);
                Decode(MainDirectory + "\\ImageBASE64.txt",MainDirectory + "\\QrCode.png");
                pictureBox1.ImageLocation = MainDirectory + "\\QrCode.png";
                richTextBox1.Text = setupInfo.ManualEntryKey;
            }
            catch (Exception d)
            {
                Console.WriteLine(d);
                throw;
            }
        }
        private void Decode(string Input, string Output)
        {
            RunCommand("certutil -decode \"" + Input + "\" \"" + Output + "\"");
        }

        public void RunCommand(string Command)
        {
            string[] CommandChut = { Command };
            File.WriteAllLines(System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand.bat", CommandChut);
            var C = Process.Start(System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand.bat");
            C.WaitForExit();
            File.Delete(System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand.bat");
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            string user_enter = textBox1.Text;
            TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
            bool Correct = tfa.ValidateTwoFactorPIN(Code, user_enter);
            if (Correct == true)
            {
                NeedToClose = true;
                File.WriteAllText(MainDirectory + "\\Code.txt",Code);
                Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Process.Start(MainDirectory + "\\QrCode.png");
        }
    }
}
