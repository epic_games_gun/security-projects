﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Secure_File_Vault
{
	public partial class GenerateSecurityPIN : Form
	{
		public GenerateSecurityPIN()
		{
			InitializeComponent();
		}
		private string MainDirectory = Environment.GetEnvironmentVariable("TEMP") + "\\FileVault";
		private void GenerateSecurityPIN_Load(object sender, EventArgs e)
        {
            TopMost = true;
            TopLevel = true;
            FormBorderStyle = FormBorderStyle.FixedSingle;
            Text = "Generate Security PIN";
			ShowIcon = false;
			Random d = new Random();
			int f = d.Next(1111, 9999);
			File.WriteAllText(MainDirectory + "\\GeneratedPIN.txt", f.ToString());
            richTextBox1.Text = f.ToString();
			Clipboard.SetText(f.ToString());
            richTextBox1.ReadOnly = true;
		}
        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }
		private void button1_Click(object sender, EventArgs e)
		{
			if (textBox1.Text == File.ReadLines(MainDirectory + "\\GeneratedPIN.txt").ElementAtOrDefault(0))
			{
				Close();
			}
			else
			{
				MessageBox.Show("Wrong PIN, make sure to rewrite the generated PIN to the textbox!");
			}
		}
	}
}
