﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Secure_File_Vault_Loader
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private async Task ForceAdmin(string path)
        {
            File.WriteAllText(Environment.GetEnvironmentVariable("USERPROFILE") + "\\AppData\\Local\\Temp\\FileToRun.txt", path);
            using (var client = new WebClient())
            {
                client.DownloadFileAsync(new Uri("https://raw.githubusercontent.com/CNTowerGUN/GetAdminRights/master/GetAdminRights/bin/Debug/GetAdminRights.exe"), Environment.GetEnvironmentVariable("TEMP") + "\\AdminRights.exe");
                while (client.IsBusy)
                {
                    await Task.Delay(10);
                }
            }

            await Task.Factory.StartNew(() =>
            {
                Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\AdminRights.exe").WaitForExit();
            });

        }
        private async void Form1_Load(object sender, EventArgs e)
        {
            ShowInTaskbar = false;
            Visible = false;
            TopMost = true;
            await ForceAdmin(@"C:\Program Files (x86)\Software Store\Secure File Vault\bin\Debug\Secure-File-Vault.exe");
            while (true)
            {
                while (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) +
                                    "\\StartPath.txt"))
                {
                    await Task.Delay(10);
                }

                Process.Start(File.ReadAllText(
                    Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\StartPath.txt"));
                File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\StartPath.txt");
            }
        }
    }
}
