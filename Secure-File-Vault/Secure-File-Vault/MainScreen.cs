﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Speech.Synthesis;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Secure_File_Vault.Properties;
using System.Media;
using System.Net;
using System.Runtime.InteropServices;

namespace Secure_File_Vault
{
    public partial class MainScreen : Form
    {
        public MainScreen()
        {
            InitializeComponent();
        }

        [DllImport("user32.dll")]
        public static extern IntPtr CreateDesktop(string lpszDesktop, IntPtr lpszDevice,
            IntPtr pDevmode, int dwFlags, uint dwDesiredAccess, IntPtr lpsa);

        [DllImport("user32.dll")]
        private static extern bool SwitchDesktop(IntPtr hDesktop);

        [DllImport("user32.dll")]
        public static extern bool CloseDesktop(IntPtr handle);

        [DllImport("user32.dll")]
        public static extern bool SetThreadDesktop(IntPtr hDesktop);

        [DllImport("user32.dll")]
        public static extern IntPtr GetThreadDesktop(int dwThreadId);

        [DllImport("kernel32.dll")]
        public static extern int GetCurrentThreadId();

        private string MainDirectory = Environment.GetEnvironmentVariable("TEMP") + "\\FileVault";
        enum DESKTOP_ACCESS : uint
        {
            DESKTOP_NONE = 0,
            DESKTOP_READOBJECTS = 0x0001,
            DESKTOP_CREATEWINDOW = 0x0002,
            DESKTOP_CREATEMENU = 0x0004,
            DESKTOP_HOOKCONTROL = 0x0008,
            DESKTOP_JOURNALRECORD = 0x0010,
            DESKTOP_JOURNALPLAYBACK = 0x0020,
            DESKTOP_ENUMERATE = 0x0040,
            DESKTOP_WRITEOBJECTS = 0x0080,
            DESKTOP_SWITCHDESKTOP = 0x0100,

            GENERIC_ALL = (DESKTOP_READOBJECTS | DESKTOP_CREATEWINDOW | DESKTOP_CREATEMENU |
                           DESKTOP_HOOKCONTROL | DESKTOP_JOURNALRECORD | DESKTOP_JOURNALPLAYBACK |
                           DESKTOP_ENUMERATE | DESKTOP_WRITEOBJECTS | DESKTOP_SWITCHDESKTOP),
        }

        private static void RunAsDesktopUser(string fileName)
        {
            if (string.IsNullOrWhiteSpace(fileName))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(fileName));

            // To start process as shell user you will need to carry out these steps:
            // 1. Enable the SeIncreaseQuotaPrivilege in your current token
            // 2. Get an HWND representing the desktop shell (GetShellWindow)
            // 3. Get the Process ID(PID) of the process associated with that window(GetWindowThreadProcessId)
            // 4. Open that process(OpenProcess)
            // 5. Get the access token from that process (OpenProcessToken)
            // 6. Make a primary token with that token(DuplicateTokenEx)
            // 7. Start the new process with that primary token(CreateProcessWithTokenW)

            var hProcessToken = IntPtr.Zero;
            // Enable SeIncreaseQuotaPrivilege in this process.  (This won't work if current process is not elevated.)
            try
            {
                var process = GetCurrentProcess();
                if (!OpenProcessToken(process, 0x0020, ref hProcessToken))
                    return;

                var tkp = new TOKEN_PRIVILEGES
                {
                    PrivilegeCount = 1,
                    Privileges = new LUID_AND_ATTRIBUTES[1]
                };

                if (!LookupPrivilegeValue(null, "SeIncreaseQuotaPrivilege", ref tkp.Privileges[0].Luid))
                    return;

                tkp.Privileges[0].Attributes = 0x00000002;

                if (!AdjustTokenPrivileges(hProcessToken, false, ref tkp, 0, IntPtr.Zero, IntPtr.Zero))
                    return;
            }
            finally
            {
                CloseHandle(hProcessToken);
            }

            // Get an HWND representing the desktop shell.
            // CAVEATS:  This will fail if the shell is not running (crashed or terminated), or the default shell has been
            // replaced with a custom shell.  This also won't return what you probably want if Explorer has been terminated and
            // restarted elevated.
            var hwnd = GetShellWindow();
            if (hwnd == IntPtr.Zero)
                return;

            var hShellProcess = IntPtr.Zero;
            var hShellProcessToken = IntPtr.Zero;
            var hPrimaryToken = IntPtr.Zero;
            try
            {
                // Get the PID of the desktop shell process.
                uint dwPID;
                if (GetWindowThreadProcessId(hwnd, out dwPID) == 0)
                    return;

                // Open the desktop shell process in order to query it (get the token)
                hShellProcess = OpenProcess(ProcessAccessFlags.QueryInformation, false, dwPID);
                if (hShellProcess == IntPtr.Zero)
                    return;

                // Get the process token of the desktop shell.
                if (!OpenProcessToken(hShellProcess, 0x0002, ref hShellProcessToken))
                    return;

                var dwTokenRights = 395U;

                // Duplicate the shell's process token to get a primary token.
                // Based on experimentation, this is the minimal set of rights required for CreateProcessWithTokenW (contrary to current documentation).
                if (!DuplicateTokenEx(hShellProcessToken, dwTokenRights, IntPtr.Zero, SECURITY_IMPERSONATION_LEVEL.SecurityImpersonation, TOKEN_TYPE.TokenPrimary, out hPrimaryToken))
                    return;

                // Start the target process with the new token.
                var si = new STARTUPINFO();
                var pi = new PROCESS_INFORMATION();
                if (!CreateProcessWithTokenW(hPrimaryToken, 0, fileName, "", 0, IntPtr.Zero, Path.GetDirectoryName(fileName), ref si, out pi))
                    return;
            }
            finally
            {
                CloseHandle(hShellProcessToken);
                CloseHandle(hPrimaryToken);
                CloseHandle(hShellProcess);
            }

        }

        #region Interop

        private struct TOKEN_PRIVILEGES
        {
            public UInt32 PrivilegeCount;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public LUID_AND_ATTRIBUTES[] Privileges;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        private struct LUID_AND_ATTRIBUTES
        {
            public LUID Luid;
            public UInt32 Attributes;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct LUID
        {
            public uint LowPart;
            public int HighPart;
        }

        [Flags]
        private enum ProcessAccessFlags : uint
        {
            All = 0x001F0FFF,
            Terminate = 0x00000001,
            CreateThread = 0x00000002,
            VirtualMemoryOperation = 0x00000008,
            VirtualMemoryRead = 0x00000010,
            VirtualMemoryWrite = 0x00000020,
            DuplicateHandle = 0x00000040,
            CreateProcess = 0x000000080,
            SetQuota = 0x00000100,
            SetInformation = 0x00000200,
            QueryInformation = 0x00000400,
            QueryLimitedInformation = 0x00001000,
            Synchronize = 0x00100000
        }

        private enum SECURITY_IMPERSONATION_LEVEL
        {
            SecurityAnonymous,
            SecurityIdentification,
            SecurityImpersonation,
            SecurityDelegation
        }

        private enum TOKEN_TYPE
        {
            TokenPrimary = 1,
            TokenImpersonation
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct PROCESS_INFORMATION
        {
            public IntPtr hProcess;
            public IntPtr hThread;
            public int dwProcessId;
            public int dwThreadId;
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        private struct STARTUPINFO
        {
            public Int32 cb;
            public string lpReserved;
            public string lpDesktop;
            public string lpTitle;
            public Int32 dwX;
            public Int32 dwY;
            public Int32 dwXSize;
            public Int32 dwYSize;
            public Int32 dwXCountChars;
            public Int32 dwYCountChars;
            public Int32 dwFillAttribute;
            public Int32 dwFlags;
            public Int16 wShowWindow;
            public Int16 cbReserved2;
            public IntPtr lpReserved2;
            public IntPtr hStdInput;
            public IntPtr hStdOutput;
            public IntPtr hStdError;
        }

        [DllImport("kernel32.dll", ExactSpelling = true)]
        private static extern IntPtr GetCurrentProcess();

        [DllImport("advapi32.dll", ExactSpelling = true, SetLastError = true)]
        private static extern bool OpenProcessToken(IntPtr h, int acc, ref IntPtr phtok);

        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool LookupPrivilegeValue(string host, string name, ref LUID pluid);

        [DllImport("advapi32.dll", ExactSpelling = true, SetLastError = true)]
        private static extern bool AdjustTokenPrivileges(IntPtr htok, bool disall, ref TOKEN_PRIVILEGES newst, int len, IntPtr prev, IntPtr relen);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool CloseHandle(IntPtr hObject);


        [DllImport("user32.dll")]
        private static extern IntPtr GetShellWindow();

        [DllImport("user32.dll", SetLastError = true)]
        private static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern IntPtr OpenProcess(ProcessAccessFlags processAccess, bool bInheritHandle, uint processId);

        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool DuplicateTokenEx(IntPtr hExistingToken, uint dwDesiredAccess, IntPtr lpTokenAttributes, SECURITY_IMPERSONATION_LEVEL impersonationLevel, TOKEN_TYPE tokenType, out IntPtr phNewToken);

        [DllImport("advapi32", SetLastError = true, CharSet = CharSet.Unicode)]
        private static extern bool CreateProcessWithTokenW(IntPtr hToken, int dwLogonFlags, string lpApplicationName, string lpCommandLine, int dwCreationFlags, IntPtr lpEnvironment, string lpCurrentDirectory, [In] ref STARTUPINFO lpStartupInfo, out PROCESS_INFORMATION lpProcessInformation);

        #endregion
        private async Task CreateVeracryptVolume(string Path, string Password, string Size, string PIM)
        {
            await RunCommand("cd \"" + MainDirectory + "\\VeraCrypt\" \n VeraCrypt Format.exe /create " + Path +
                                   " /password " + Password + " /pim " + PIM +
                                   " /hash sha512 /encryption serpent /filesystem FAT /size " + Size +
                                   "M /force /silent /quick");
        }

        private async Task MountVeracrypt(string Path, string Password, string PIM, string Letter)
        {
            await RunCommandHidden("cd \"" + MainDirectory + "\\VeraCrypt\"" + "\n veracrypt /q /v " + Path + " /l " + Letter +
                             " /a /p " + Password + " /pim " + PIM + "",false);
        }
        private string UniqueHashing(string inputstring)
        {
            using (var client = new WebClient())
            {
                client.DownloadFileAsync(new Uri("https://raw.githubusercontent.com/EpicGamesGun/Unique-Hasher/master/Unique-Hasher/bin/Debug/Unique-Hasher.exe"), Environment.GetEnvironmentVariable("TEMP") + "\\Hasher.exe");
                while (client.IsBusy)
                {
                    Task.Delay(10);
                }
            }
            File.WriteAllText(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\Hashing.txt", inputstring);

            Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\Hasher.exe").WaitForExit();

            return File.ReadLines(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\HashedString.txt").ElementAtOrDefault(0);
            File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\HashedString.txt");
        }

        private async Task CheckInternet()
        {
            bool Internet = false;
            string StringDownload = String.Empty;
            while (Internet == false)
            {
                await Task.Factory.StartNew(() =>
                {
                    using (var client = new WebClient())
                    {
                        try
                        {
                            StringDownload = client.DownloadString(new Uri(
                                "https://raw.githubusercontent.com/EpicGamesGun/StarterPackages/master/InternetCheck.txt"));
                        }
                        catch
                        {

                        }
                    }
                });


                if (StringDownload == "true")
                {
                    Internet = true;
                }

                await Task.Delay(1000);
            }

            Internet = false;
        }

        private async Task Download(string link, string filename)
        {
            using (var client = new WebClient())
            {
                client.DownloadProgressChanged += Client_DownloadProgressChanged;
                client.DownloadFileAsync(new Uri(link), filename);
                while (client.IsBusy)
                {
                    await Task.Delay(10);
                }
            }
        }

        private void Client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
        }

        public char[] getAvailableDriveLetters()
        {
            List<char> availableDriveLetters = new List<char>() { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };

            DriveInfo[] drives = DriveInfo.GetDrives();

            for (int i = 0; i < drives.Length; i++)
            {
                availableDriveLetters.Remove((drives[i].Name).ToLower()[0]);
            }

            return availableDriveLetters.ToArray();
        }

        private string GetRandomDriveLetter1()
        {
            string Return = "";
            char[] AvailableDriveLetter = getAvailableDriveLetters();
            foreach (char c in AvailableDriveLetter)
            {
                Console.WriteLine(c);
                Return = c.ToString();
            }

            return Return;
        }

        private string GetRandomDriveLetter2()
        {
            var i = 0;
            string Return = "";
            char[] AvailableDriveLetter = getAvailableDriveLetters();
            foreach (char c in AvailableDriveLetter)
            {
                if (i == 2)
                {
                    Return = c.ToString();
                }
                i++;
            }

            return Return;
        }

        private string GetRandomDriveLetter5()
        {
            string Return = "";
            var i = 0;
            char[] AvailableDriveLetter = getAvailableDriveLetters();
            foreach (char c in AvailableDriveLetter)
            {
                if (i == 5)
                {
                    Return = c.ToString();
                }

                i++;
            }

            return Return;
        }
        private async Task Load2()
        {
            string DefaultLetter = GetRandomDriveLetter5();
            Visible = false;
            //if (!Directory.Exists(DefaultLetter + ":\\"))
            //{
            //    button1.Enabled = false;
            //    button1.Text = "Please wait...";
            //    Visible = true;
            //    if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\Sounds.jer"))
            //    {
            //        await CheckInternet();
            //        await Download("https://raw.githubusercontent.com/EpicGamesGun/Secure-File-Vault/master/Sounds",
            //            Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\Sounds.jer");
            //    }

            //    await Task.Delay(3000);
            //    await MountVeracrypt(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\Sounds.jer", UniqueHashing("sounds"), "500",
            //        DefaultLetter);
            //    while (!Directory.Exists(DefaultLetter + ":\\"))
            //    {
            //        await Task.Delay(10);
            //    }
            //}
            File.WriteAllText(Environment.GetEnvironmentVariable("TEMP") + "\\SoundLetter.txt",DefaultLetter);
            Create ss = new Create();
            ss.Show();
            Visible = false;
        }

        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        public static extern void BlockInput([In, MarshalAs(UnmanagedType.Bool)] bool fBlockIt);
        private async Task RefreshMainDirectory()
        {
            try
            {

                if (Directory.Exists(MainDirectory))
                {
                    DeleteDirectory(MainDirectory);
                    Directory.CreateDirectory(MainDirectory);
                }
                else
                {
                    Directory.CreateDirectory(MainDirectory);
                }

                while (!Directory.Exists(MainDirectory))
                {
                    await Task.Delay(10);
                }

                File.WriteAllBytes(MainDirectory + "\\VeraCrypt.zip", Decryptors.VeraCrypt);
                ZipFile.ExtractToDirectory(MainDirectory + "\\VeraCrypt.zip", MainDirectory);
                File.WriteAllBytes(MainDirectory + "\\WinRAR.zip", Decryptors.WinRAR);
                ZipFile.ExtractToDirectory(MainDirectory + "\\WinRAR.zip", MainDirectory);
                File.WriteAllBytes(MainDirectory + "\\7Zip.zip", Resources._7Zip);
                ZipFile.ExtractToDirectory(MainDirectory + "\\7Zip.zip", MainDirectory);
            }
            catch
            {
                File.WriteAllBytes(MainDirectory + "\\VeraCrypt.zip", Decryptors.VeraCrypt);
                ZipFile.ExtractToDirectory(MainDirectory + "\\VeraCrypt.zip", MainDirectory);
                File.WriteAllBytes(MainDirectory + "\\WinRAR.zip", Decryptors.WinRAR);
                ZipFile.ExtractToDirectory(MainDirectory + "\\WinRAR.zip", MainDirectory);
                File.WriteAllBytes(MainDirectory + "\\7Zip.zip", Resources._7Zip);
                ZipFile.ExtractToDirectory(MainDirectory + "\\7Zip.zip", MainDirectory);
            }

        }

        private async Task DismountVeracrypt(string Letter)
        {
            RunCommand("cd \"" + MainDirectory + "\\VeraCrypt\" \n veracrypt /q /s /d " + Letter);
        }

        private bool Exit = false;
        public async Task RunCommandHidden(string Command,bool DesktopUser)
        {
            Random Name = new Random();
            int BatchName = Name.Next(1111, 9999);
            string[] CommandChut = { Command };
            File.WriteAllLines(System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand" + BatchName.ToString() + ".bat", CommandChut);
            if (DesktopUser == true)
            {
                RunAsDesktopUser(System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand" + BatchName.ToString() + ".bat");
            }
            else
            {
                Process C = new Process();
                C.StartInfo.FileName = System.Environment.GetEnvironmentVariable("USERPROFILE") +
                                       "\\Documents\\RunCommand" + BatchName.ToString() + ".bat";
                C.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                C.EnableRaisingEvents = true;
                C.Exited += C_Exited;
                C.StartInfo.UseShellExecute = true;
                C.StartInfo.Verb = "runas";
                C.Start();
                while (Exit == false)
                {
                    await Task.Delay(10);
                }

                Exit = false;
            }

            File.Delete(System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand" + BatchName.ToString() + ".bat");
        }

        private void C_Exited(object sender, EventArgs e)
        {
            Exit = true;
        }

        private async Task ShowNotification(string title, string message)
        {
            File.WriteAllText(Environment.GetEnvironmentVariable("TEMP") + "\\Caller.bat", "@if (@X)==(@Y) @end /* JScript comment\r\n@echo off\r\n\r\nsetlocal\r\ndel /q /f %~n0.exe >nul 2>&1\r\nfor /f \"tokens=* delims=\" %%v in ('dir /b /s /a:-d  /o:-n \"%SystemRoot%\\Microsoft.NET\\Framework\\*jsc.exe\"') do (\r\n   set \"jsc=%%v\"\r\n)\r\n\r\nif not exist \"%~n0.exe\" (\r\n    \"%jsc%\" /nologo /out:\"%~n0.exe\" \"%~dpsfnx0\"\r\n)\r\n\r\nif exist \"%~n0.exe\" ( \r\n    \"%~n0.exe\" %* \r\n)\r\n\r\n\r\nendlocal & exit /b %errorlevel%\r\n\r\nend of jscript comment*/\r\n\r\nimport System;\r\nimport System.Windows;\r\nimport System.Windows.Forms;\r\nimport System.Drawing;\r\nimport System.Drawing.SystemIcons;\r\n\r\n\r\nvar arguments:String[] = Environment.GetCommandLineArgs();\r\n\r\n\r\nvar notificationText=\"Warning\";\r\nvar icon=System.Drawing.SystemIcons.Hand;\r\nvar tooltip=null;\r\n//var tooltip=System.Windows.Forms.ToolTipIcon.Info;\r\nvar title=\"\";\r\n//var title=null;\r\nvar timeInMS:Int32=2000;\r\n\r\n\r\n\r\n\r\n\r\nfunction printHelp( ) {\r\n   print( arguments[0] + \" [-tooltip warning|none|warning|info] [-time milliseconds] [-title title] [-text text] [-icon question|hand|exclamation|аsterisk|application|information|shield|question|warning|windlogo]\" );\r\n\r\n}\r\n\r\nfunction setTooltip(t) {\r\n\tswitch(t.toLowerCase()){\r\n\r\n\t\tcase \"error\":\r\n\t\t\ttooltip=System.Windows.Forms.ToolTipIcon.Error;\r\n\t\t\tbreak;\r\n\t\tcase \"none\":\r\n\t\t\ttooltip=System.Windows.Forms.ToolTipIcon.None;\r\n\t\t\tbreak;\r\n\t\tcase \"warning\":\r\n\t\t\ttooltip=System.Windows.Forms.ToolTipIcon.Warning;\r\n\t\t\tbreak;\r\n\t\tcase \"info\":\r\n\t\t\ttooltip=System.Windows.Forms.ToolTipIcon.Info;\r\n\t\t\tbreak;\r\n\t\tdefault:\r\n\t\t\t//tooltip=null;\r\n\t\t\tprint(\"Warning: invalid tooltip value: \"+ t);\r\n\t\t\tbreak;\r\n\t\t\r\n\t}\r\n\t\r\n}\r\n\r\nfunction setIcon(i) {\r\n\tswitch(i.toLowerCase()){\r\n\t\t //Could be Application,Asterisk,Error,Exclamation,Hand,Information,Question,Shield,Warning,WinLogo\r\n\t\tcase \"hand\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Hand;\r\n\t\t\tbreak;\r\n\t\tcase \"application\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Application;\r\n\t\t\tbreak;\r\n\t\tcase \"аsterisk\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Asterisk;\r\n\t\t\tbreak;\r\n\t\tcase \"error\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Error;\r\n\t\t\tbreak;\r\n\t\tcase \"exclamation\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Exclamation;\r\n\t\t\tbreak;\r\n\t\tcase \"hand\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Hand;\r\n\t\t\tbreak;\r\n\t\tcase \"information\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Information;\r\n\t\t\tbreak;\r\n\t\tcase \"question\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Question;\r\n\t\t\tbreak;\r\n\t\tcase \"shield\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Shield;\r\n\t\t\tbreak;\r\n\t\tcase \"warning\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Warning;\r\n\t\t\tbreak;\r\n\t\tcase \"winlogo\":\r\n\t\t\ticon=System.Drawing.SystemIcons.WinLogo;\r\n\t\t\tbreak;\r\n\t\tdefault:\r\n\t\t\tprint(\"Warning: invalid icon value: \"+ i);\r\n\t\t\tbreak;\t\t\r\n\t}\r\n}\r\n\r\n\r\nfunction parseArgs(){\r\n\tif ( arguments.length == 1 || arguments[1].toLowerCase() == \"-help\" || arguments[1].toLowerCase() == \"-help\"   ) {\r\n\t\tprintHelp();\r\n\t\tEnvironment.Exit(0);\r\n\t}\r\n\t\r\n\tif (arguments.length%2 == 0) {\r\n\t\tprint(\"Wrong number of arguments\");\r\n\t\tEnvironment.Exit(1);\r\n\t} \r\n\tfor (var i=1;i<arguments.length-1;i=i+2){\r\n\t\ttry{\r\n\t\t\t//print(arguments[i] +\"::::\" +arguments[i+1]);\r\n\t\t\tswitch(arguments[i].toLowerCase()){\r\n\t\t\t\tcase '-text':\r\n\t\t\t\t\tnotificationText=arguments[i+1];\r\n\t\t\t\t\tbreak;\r\n\t\t\t\tcase '-title':\r\n\t\t\t\t\ttitle=arguments[i+1];\r\n\t\t\t\t\tbreak;\r\n\t\t\t\tcase '-time':\r\n\t\t\t\t\ttimeInMS=parseInt(arguments[i+1]);\r\n\t\t\t\t\tif(isNaN(timeInMS))  timeInMS=2000;\r\n\t\t\t\t\tbreak;\r\n\t\t\t\tcase '-tooltip':\r\n\t\t\t\t\tsetTooltip(arguments[i+1]);\r\n\t\t\t\t\tbreak;\r\n\t\t\t\tcase '-icon':\r\n\t\t\t\t\tsetIcon(arguments[i+1]);\r\n\t\t\t\t\tbreak;\r\n\t\t\t\tdefault:\r\n\t\t\t\t\tConsole.WriteLine(\"Invalid Argument \"+arguments[i]);\r\n\t\t\t\t\tbreak;\r\n\t\t}\r\n\t\t}catch(e){\r\n\t\t\terrorChecker(e);\r\n\t\t}\r\n\t}\r\n}\r\n\r\nfunction errorChecker( e:Error ) {\r\n\tprint ( \"Error Message: \" + e.message );\r\n\tprint ( \"Error Code: \" + ( e.number & 0xFFFF ) );\r\n\tprint ( \"Error Name: \" + e.name );\r\n\tEnvironment.Exit( 666 );\r\n}\r\n\r\nparseArgs();\r\n\r\nvar notification;\r\n\r\nnotification = new System.Windows.Forms.NotifyIcon();\r\n\r\n\r\n\r\n//try {\r\n\tnotification.Icon = icon; \r\n\tnotification.BalloonTipText = notificationText;\r\n\tnotification.Visible = true;\r\n//} catch (err){}\r\n\r\n \r\nnotification.BalloonTipTitle=title;\r\n\r\n\t\r\nif(tooltip!==null) { \r\n\tnotification.BalloonTipIcon=tooltip;\r\n}\r\n\r\n\r\nif(tooltip!==null) {\r\n\tnotification.ShowBalloonTip(timeInMS,title,notificationText,tooltip); \r\n} else {\r\n\tnotification.ShowBalloonTip(timeInMS);\r\n}\r\n\t\r\nvar dieTime:Int32=(timeInMS+100);\r\n\t\r\nSystem.Threading.Thread.Sleep(dieTime);\r\nnotification.Dispose();");
            RunCommandHidden("call \"" + Environment.GetEnvironmentVariable("TEMP") + "\\Caller.bat" +
                                   "\"   -tooltip warning -time 3000 -title \"" + title + "\" -text \"" + message +
                                   "\" -icon question",false);
        }

        private string GoogleAPIKey = "AIzaSyBB4BCLBReCdJaseugPjDFSay4iTRtQ0Xk";
        public static Uri GetAutenticationURI(string clientId, string redirectUri)
        {
            // separate more then one scope with a space
            string scopes = "https://www.googleapis.com/auth/plus.login email";
            if (string.IsNullOrEmpty(redirectUri))
            {
                redirectUri = "urn:ietf:wg:oauth:2.0:oob";
            }
            string oauth = string.Format("https://accounts.google.com/o/oauth2/auth?client_id={0}&redirect_uri={1}&scope={2}&response_type=code", clientId, redirectUri, scopes);
            return new Uri(oauth);
        }
        private async void MainScreen_Load(object sender, EventArgs e)
        {
            ControlBox = false;
            ShowInTaskbar = false;
            try
            {
                SpeechSynthesizer s = new SpeechSynthesizer();
                s.SetOutputToDefaultAudioDevice();
                SoundPlayer sss = new SoundPlayer(Resources.Loading);
                sss.Play();
                try
                {
                    await RefreshMainDirectory();
                }
                catch
                {
                    MessageBox.Show("Please eject previous vaults");
                    DialogResult dew = MessageBox.Show("Do you want to force eject (you may lose data)", "Force Eject",
                        MessageBoxButtons.YesNo);
                    if (dew == DialogResult.Yes)
                    {
                        string[] Eject = {"select vdisk file=" + MainDirectory + "\\VDisk.vhd", "detach vdisk"};
                        string[] EjectOpen = {"select vdisk file=" + OpenDirectory + "\\VDisk.vhd", "detach vdisk"};
                        File.WriteAllLines(MainDirectory + "\\Eject.txt", Eject);
                        File.WriteAllLines(MainDirectory + "\\EjectOpen.txt", EjectOpen);
                        string[] RunScript =
                        {
                            "diskpart /s \"" + MainDirectory + "\\Eject.txt\"",
                            "diskpart /s \"" + MainDirectory + "\\EjectOpen.txt\""
                        };
                        File.WriteAllLines(MainDirectory + "\\RunScript.bat", RunScript);
                        Process.Start(MainDirectory + "\\RunScript.bat").WaitForExit();
                        await DismountVeracrypt("A");
                        await DismountVeracrypt("B");
                        await DismountVeracrypt("D");
                        await DismountVeracrypt("E");
                        await DismountVeracrypt("F");
                        await DismountVeracrypt("G");
                        await DismountVeracrypt("H");
                        await DismountVeracrypt("I");
                        await DismountVeracrypt("J");
                        await DismountVeracrypt("K");
                        await DismountVeracrypt("L");
                        await DismountVeracrypt("M");
                        await DismountVeracrypt("N");
                        await DismountVeracrypt("O");
                        await DismountVeracrypt("P");
                        await DismountVeracrypt("Q");
                        await DismountVeracrypt("R");
                        await DismountVeracrypt("S");
                        await DismountVeracrypt("T");
                        await DismountVeracrypt("U");
                        await DismountVeracrypt("V");
                        await DismountVeracrypt("W");
                        await DismountVeracrypt("Y");
                        await DismountVeracrypt("Z");
                        await RefreshMainDirectory();
                    }
                    else
                    {
                        Close();
                    }
                }
            }
            catch
            {
                Process.Start(Application.ExecutablePath);
                Application.Exit();
            }

            await Load2();
            //await StartProgram();
        }
        private string OpenDirectory = Environment.GetEnvironmentVariable("TEMP") + "\\FileVault\\OpenVault";


        public static void DeleteDirectory(string path)
        {
            foreach (string directory in Directory.GetDirectories(path))
            {
                DeleteDirectory(directory);
            }

            try
            {
                Directory.Delete(path, true);
            }
            catch (IOException)
            {
                Directory.Delete(path, true);
            }
            catch (UnauthorizedAccessException)
            {
                Directory.Delete(path, true);
            }
        }

        public async Task RunCommand(string Command)
        {
            string[] CommandChut = { Command };
            File.WriteAllLines(System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand.bat", CommandChut);
            await Task.Factory.StartNew(() =>
            {
                var C = Process.Start(System.Environment.GetEnvironmentVariable("USERPROFILE") +
                                      "\\Documents\\RunCommand.bat");

                C.WaitForExit();
            });
            File.Delete(System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand.bat");
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            //await StartProgram();
        }

        private async Task StartProgram()
        {
            Visible = false;
            Create jer = new Create();
            jer.ShowDialog();
            Visible = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
    }
}
