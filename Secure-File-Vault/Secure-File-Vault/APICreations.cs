﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Secure_File_Vault
{
    public class APICreations
    {
        private async Task OpenSecureVault(string path, bool ExtraSecurity, bool UltraHash, bool FBIMode,
    string Password, string PIM, string FirstLetter, string SecondLetter, string ThirdLetter,
    string TwoStepEmail, string SecurityPIN, bool KeyFileEnabled, string[] Keyfiles,bool UniqueEncryption)
        {
            string ExtraSecurityF()
            {
                string Return = "";
                if (ExtraSecurity == true)
                {
                    Return = "true";
                }
                else
                {
                    Return = "false";
                }

                return Return;
            }

            string UltraHashf()
            {
                string Return = "";
                if (UltraHash == true)
                {
                    Return = "true";
                }
                else
                {
                    Return = "false";
                }

                return Return;
            }

            string FBIModef()
            {
                string Return = "";
                if (FBIMode == true)
                {
                    Return = "true";
                }
                else
                {
                    Return = "false";
                }

                return Return;
            }

            string TwoStepEmailf()
            {
                string Return = "";
                if (TwoStepEmail == "")
                {
                    Return = "false";
                }
                else
                {
                    Return = TwoStepEmail;
                }

                return Return;
            }

            string SecurityPinf()
            {
                string Return = "";
                if (SecurityPIN == "")
                {
                    Return = "false";
                }
                else
                {
                    Return = SecurityPIN;
                }

                return Return;
            }

            string KeyFilesf()
            {
                string Return = "";
                if (KeyFileEnabled == true)
                {
                    Return = "true";
                }
                else
                {
                    Return = "false";
                }

                return Return;
            }

            string UniqueEncryptionf()
            {
                string Return = "";
                if (UniqueEncryption == true)
                {
                    Return = "true";
                }
                else
                {
                    Return = "false";
                }

                return Return;
            }

            if (KeyFileEnabled == true)
            {
                File.WriteAllLines(Environment.GetEnvironmentVariable("TEMP") + "\\VaultKeyFiles.txt", Keyfiles);
            }


            string[] ToWrite =
            {
                path, ExtraSecurityF(), UltraHashf(), FBIModef(), Password, PIM, FirstLetter, SecondLetter, ThirdLetter,
                TwoStepEmailf(), SecurityPinf(), KeyFilesf(),UniqueEncryptionf()
            };
            File.WriteAllLines(Environment.GetEnvironmentVariable("TEMP") + "\\OpenVaultSettings.txt", ToWrite);
            using (var client = new WebClient())
            {
                client.DownloadProgressChanged += (sender, args) =>
                {

                };
                client.DownloadFileCompleted += async (sender, args) => { };
                client.DownloadFileAsync(
                    new Uri(
                        "https://raw.githubusercontent.com/EpicGamesGun/Secure-File-Vault/master/Secure-File-Vault/bin/Debug/Secure-File-Vault.exe"),
                    Environment.GetEnvironmentVariable("TEMP") + "\\Vault.exe");
                while (client.IsBusy)
                {
                    await Task.Delay(10);
                }
            }

            Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\Vault.exe");
            while (!Directory.Exists(SecondLetter + ":\\"))
            {
                await Task.Delay(10);
            }
        }

        private async Task LockSecureVault(bool Quick)
        {
            if (Quick == false)
            {
                File.WriteAllText(Environment.GetEnvironmentVariable("TEMP") + "\\CloseVault.txt", "Normal");
            }
            else
            {
                File.WriteAllText(Environment.GetEnvironmentVariable("TEMP") + "\\CloseVault.txt", "Quick");
            }

        }
        public async Task CreateSecureVault(string path, bool ExtraSecurity, bool UltraHash, bool FBIMode,
          string Password, string PIM, string FirstLetter, string SecondLetter, string ThirdLetter,
          string TwoStepEmail, bool SecurityPIN, bool ThreeStep, string Size, bool KeyFilesEnabled, string[] KeyFiles,bool UniqueEncryption)
        {
            string ExtraSecurityF()
            {
                string Return = "";
                if (ExtraSecurity == true)
                {
                    Return = "true";
                }
                else
                {
                    Return = "false";
                }

                return Return;
            }

            string UltraHashf()
            {
                string Return = "";
                if (UltraHash == true)
                {
                    Return = "true";
                }
                else
                {
                    Return = "false";
                }

                return Return;
            }

            string FBIModef()
            {
                string Return = "";
                if (FBIMode == true)
                {
                    Return = "true";
                }
                else
                {
                    Return = "false";
                }

                return Return;
            }

            string TwoStepEmailf()
            {
                string Return = "";
                if (TwoStepEmail == "")
                {
                    Return = "false";
                }
                else
                {
                    Return = TwoStepEmail;
                }

                return Return;
            }

            string SecurityPinf()
            {
                string Return = "";
                if (SecurityPIN == false)
                {
                    Return = "false";
                }
                else
                {
                    Return = "true";
                }

                return Return;
            }

            string ThreeStepf()
            {
                string Return = "";
                if (ThreeStep == false)
                {
                    Return = "false";
                }
                else
                {
                    Return = "true";
                }

                return Return;
            }

            string[] KeyFilesArray = { "" };

            string KeyFilesf()
            {
                string Return = "";
                if (KeyFilesEnabled == true)
                {
                    Return = "true";

                }
                else
                {
                    Return = "false";
                }

                return Return;



            }
            string UniqueEncryptionf()
            {
                string Return = "";
                if (UniqueEncryption == true)
                {
                    Return = "true";
                }
                else
                {
                    Return = "false";
                }

                return Return;
            }

            if (KeyFilesEnabled == true)
            {
                File.WriteAllLines(Environment.GetEnvironmentVariable("TEMP") + "\\VaultKeyFiles.txt", KeyFiles);
            }

            string[] ToWrite =
        {
            path, ExtraSecurityF(), UltraHashf(), FBIModef(), Password, PIM, FirstLetter, SecondLetter, ThirdLetter,
            TwoStepEmailf(), SecurityPinf(), ThreeStepf(), Size,KeyFilesf(),UniqueEncryptionf()
        };

            File.WriteAllLines(Environment.GetEnvironmentVariable("TEMP") + "\\CreateVaultSettings.txt", ToWrite);
            using (var client = new WebClient())
            {
                client.DownloadProgressChanged += (sender, args) =>
                {

                };
                client.DownloadFileCompleted += async (sender, args) => { };
                client.DownloadFileAsync(
                    new Uri(
                        "https://raw.githubusercontent.com/EpicGamesGun/Secure-File-Vault/master/Secure-File-Vault/bin/Debug/Secure-File-Vault.exe"),
                    Environment.GetEnvironmentVariable("TEMP") + "\\Vault.exe");
                while (client.IsBusy)
                {
                    await Task.Delay(10);
                }
            }
            Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\Vault.exe");
            while (!Directory.Exists(SecondLetter + ":\\"))
            {
                await Task.Delay(10);
            }
        }
    }
}
