﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Media;
using System.Net;
using System.Net.Mail;
using System.Runtime.InteropServices;
using System.Runtime.Versioning;
using System.Security.Cryptography;
using System.Speech.Synthesis;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Windows.ApplicationModel.CommunicationBlocking;
using Windows.UI.Xaml.Media;
using Secure_File_Vault.Properties;

namespace Secure_File_Vault
{
    public partial class Create : Form
    {
        public Create()
        {
            InitializeComponent();
            this.Activated += Create_Activated;
            Load += Create_Load1;
            Load += Create_Load2;
            Load += Create_Load3;
        }

        private async void Create_Load3(object sender, EventArgs e)
        {
            if (!File.Exists(Environment.GetEnvironmentVariable("APPDATA") + "\\FinalFlash.wav"))
            {
                using (var client = new WebClient())
                {
                    client.DownloadFileAsync(
                        new Uri(
                            "https://gitlab.com/Cntowergun/security-projects/-/raw/master/Secure-File-Vault/FinalFlash.wav?inline=false"),
                        Environment.GetEnvironmentVariable("APPDATA") + "\\FinalFlash.wav");
                    while (client.IsBusy)
                    {
                        await Task.Delay(10);
                    }
                }
            }
        }
        SoundPlayer FinalFlash = new SoundPlayer(Environment.GetEnvironmentVariable("APPDATA") + "\\FinalFlash.wav");
        private async void Create_Load2(object sender, EventArgs e)
        {
            CoronavirusLabel.Text = "Coronavirus Info (Worldwide):\nConfirmed Cases:" + await GetCoronavirusCases() + "\nCritical Cases:" + await GetCriticalCoronavirusCases() + "\nDeaths:" + await GetDeathCoronavirusCases() + "\nRecovered:" + await GetRecoveredCoronavirusCases() + "\nActive:" + await GetActiveCoronavirusCases() + "\nVaccines in development: " + await GetVaccineCoronavirusCases() + "\nStatus by COVID Checker™";
        }


        private async Task<string> GetCoronavirusCases()
        {
            string CheckCoronavirusCasesFile =
                Environment.GetEnvironmentVariable("TEMP") + "\\CoronavirusCheckerThingHuiChut.txt";
            using (var client = new WebClient())
            {
                client.DownloadFileAsync(new Uri("https://ncov2019.live/"),
                    Environment.GetEnvironmentVariable("TEMP") + "\\CoronavirusCheckerThingHuiChut.txt");
                while (client.IsBusy)
                {
                    await Task.Delay(10);
                }
            }
            var ReadLine = 0;
            string CoronavirusCases = "";
            foreach (var readLine in File.ReadLines(CheckCoronavirusCasesFile))
            {
                if (readLine.Contains("Total Confirmed"))
                {
                    CoronavirusCases = File.ReadLines(CheckCoronavirusCasesFile)
                        .ElementAtOrDefault(ReadLine - 3);
                }

                ReadLine++;
            }

            CoronavirusCases = CoronavirusCases.Replace(",", "");
            CoronavirusCases.Replace(" ", "");

            return CoronavirusCases;
        }

        private async Task<string> GetRecoveredCoronavirusCases()
        {
            string CheckCoronavirusCasesFile =
                Environment.GetEnvironmentVariable("TEMP") + "\\CoronavirusCheckerThingHuiChut.txt";
            using (var client = new WebClient())
            {
                client.DownloadFileAsync(new Uri("https://ncov2019.live/"),
                    Environment.GetEnvironmentVariable("TEMP") + "\\CoronavirusCheckerThingHuiChut.txt");
                while (client.IsBusy)
                {
                    await Task.Delay(10);
                }
            }
            var ReadLine = 0;
            string CoronavirusCases = "";
            foreach (var readLine in File.ReadLines(CheckCoronavirusCasesFile))
            {
                if (readLine.Contains("Total Recovered"))
                {
                    CoronavirusCases = File.ReadLines(CheckCoronavirusCasesFile)
                        .ElementAtOrDefault(ReadLine - 3);
                }

                ReadLine++;
            }

            CoronavirusCases = CoronavirusCases.Replace(",", "");
            CoronavirusCases.Replace(" ", "");

            return CoronavirusCases;
        }

        private async Task<string> GetActiveCoronavirusCases()
        {
            string CheckCoronavirusCasesFile =
                Environment.GetEnvironmentVariable("TEMP") + "\\CoronavirusCheckerThingHuiChut.txt";
            using (var client = new WebClient())
            {
                client.DownloadFileAsync(new Uri("https://ncov2019.live/"),
                    Environment.GetEnvironmentVariable("TEMP") + "\\CoronavirusCheckerThingHuiChut.txt");
                while (client.IsBusy)
                {
                    await Task.Delay(10);
                }
            }
            var ReadLine = 0;
            string CoronavirusCases = "";
            foreach (var readLine in File.ReadLines(CheckCoronavirusCasesFile))
            {
                if (readLine.Contains("Total Active"))
                {
                    CoronavirusCases = File.ReadLines(CheckCoronavirusCasesFile)
                        .ElementAtOrDefault(ReadLine - 3);
                }

                ReadLine++;
            }

            CoronavirusCases = CoronavirusCases.Replace(",", "");
            CoronavirusCases.Replace(" ", "");

            return CoronavirusCases;
        }

        private async Task<string> GetCriticalCoronavirusCases()
        {
            string CheckCoronavirusCasesFile =
                Environment.GetEnvironmentVariable("TEMP") + "\\CoronavirusCheckerThingHuiChut.txt";
            using (var client = new WebClient())
            {
                client.DownloadFileAsync(new Uri("https://ncov2019.live/"),
                    Environment.GetEnvironmentVariable("TEMP") + "\\CoronavirusCheckerThingHuiChut.txt");
                while (client.IsBusy)
                {
                    await Task.Delay(10);
                }
            }
            var ReadLine = 0;
            string CoronavirusCases = "";
            foreach (var readLine in File.ReadLines(CheckCoronavirusCasesFile))
            {
                if (readLine.Contains("Total Critical"))
                {
                    CoronavirusCases = File.ReadLines(CheckCoronavirusCasesFile)
                        .ElementAtOrDefault(ReadLine - 3);
                }

                ReadLine++;
            }

            CoronavirusCases = CoronavirusCases.Replace(",", "");
            CoronavirusCases.Replace(" ", "");

            return CoronavirusCases;
        }

        private async Task<string> GetDeathCoronavirusCases()
        {
            string CheckCoronavirusCasesFile =
                Environment.GetEnvironmentVariable("TEMP") + "\\CoronavirusCheckerThingHuiChut.txt";
            using (var client = new WebClient())
            {
                client.DownloadFileAsync(new Uri("https://ncov2019.live/"),
                    Environment.GetEnvironmentVariable("TEMP") + "\\CoronavirusCheckerThingHuiChut.txt");
                while (client.IsBusy)
                {
                    await Task.Delay(10);
                }
            }
            var ReadLine = 0;
            string CoronavirusCases = "";
            foreach (var readLine in File.ReadLines(CheckCoronavirusCasesFile))
            {
                if (readLine.Contains("Total Deceased"))
                {
                    CoronavirusCases = File.ReadLines(CheckCoronavirusCasesFile)
                        .ElementAtOrDefault(ReadLine - 3);
                }

                ReadLine++;
            }

            CoronavirusCases = CoronavirusCases.Replace(",", "");
            CoronavirusCases.Replace(" ", "");

            return CoronavirusCases;
        }

        private async Task<string> GetVaccineCoronavirusCases()
        {
            string CheckCoronavirusCasesFile =
                Environment.GetEnvironmentVariable("TEMP") + "\\CoronavirusCheckerThingHuiChut.txt";
            using (var client = new WebClient())
            {
                client.DownloadFileAsync(new Uri("https://ncov2019.live/"),
                    Environment.GetEnvironmentVariable("TEMP") + "\\CoronavirusCheckerThingHuiChut.txt");
                while (client.IsBusy)
                {
                    await Task.Delay(10);
                }
            }
            var ReadLine = 0;
            string CoronavirusCases = "";
            foreach (var readLine in File.ReadLines(CheckCoronavirusCasesFile))
            {
                if (readLine.Contains("Total Vaccines In Development"))
                {
                    CoronavirusCases = File.ReadLines(CheckCoronavirusCasesFile)
                        .ElementAtOrDefault(ReadLine - 3);
                }

                ReadLine++;
            }

            CoronavirusCases = CoronavirusCases.Split('<')[0].Trim();
            CoronavirusCases.Replace(" ", "");
            return CoronavirusCases;
        }
        private bool LockMouse = false;
        private async void Create_Load1(object sender, EventArgs e)
        {
            while (true)
            {
                try
                {
                    await Task.Delay(10);
                    if (LockMouse == true)
                    {
                        BlockInput(true);
                    }
                    else
                    {
                        BlockInput(false);
                    }
                }
                catch
                {

                }
            }
        }

        [DllImport("user32.dll")]
        public static extern IntPtr CreateDesktop(string lpszDesktop, IntPtr lpszDevice,
            IntPtr pDevmode, int dwFlags, uint dwDesiredAccess, IntPtr lpsa);

        [DllImport("user32.dll")]
        private static extern bool SwitchDesktop(IntPtr hDesktop);

        [DllImport("user32.dll")]
        public static extern bool CloseDesktop(IntPtr handle);

        [DllImport("user32.dll")]
        public static extern bool SetThreadDesktop(IntPtr hDesktop);

        [DllImport("user32.dll")]
        public static extern IntPtr GetThreadDesktop(int dwThreadId);

        [DllImport("kernel32.dll")]
        public static extern int GetCurrentThreadId();

        private string GoogleAPIKey = "AIzaSyBB4BCLBReCdJaseugPjDFSay4iTRtQ0Xk";
        public static Uri GetAutenticationURI(string clientId, string redirectUri)
        {
            // separate more then one scope with a space
            string scopes = "https://www.googleapis.com/auth/plus.login email";
            if (string.IsNullOrEmpty(redirectUri))
            {
                redirectUri = "urn:ietf:wg:oauth:2.0:oob";
            }
            string oauth = string.Format("https://accounts.google.com/o/oauth2/auth?client_id={0}&redirect_uri={1}&scope={2}&response_type=code", clientId, redirectUri, scopes);
            return new Uri(oauth);
        }
        enum DESKTOP_ACCESS : uint
        {
            DESKTOP_NONE = 0,
            DESKTOP_READOBJECTS = 0x0001,
            DESKTOP_CREATEWINDOW = 0x0002,
            DESKTOP_CREATEMENU = 0x0004,
            DESKTOP_HOOKCONTROL = 0x0008,
            DESKTOP_JOURNALRECORD = 0x0010,
            DESKTOP_JOURNALPLAYBACK = 0x0020,
            DESKTOP_ENUMERATE = 0x0040,
            DESKTOP_WRITEOBJECTS = 0x0080,
            DESKTOP_SWITCHDESKTOP = 0x0100,

            GENERIC_ALL = (DESKTOP_READOBJECTS | DESKTOP_CREATEWINDOW | DESKTOP_CREATEMENU |
                           DESKTOP_HOOKCONTROL | DESKTOP_JOURNALRECORD | DESKTOP_JOURNALPLAYBACK |
                           DESKTOP_ENUMERATE | DESKTOP_WRITEOBJECTS | DESKTOP_SWITCHDESKTOP),
        }

        private void Create_Activated(object sender, EventArgs e)
        {
            DragAndDropCreate.DragDrop += DragAndDropCreate_DragDrop;
        }

        public static void DeleteDirectory(string path)
        {
            foreach (string directory in Directory.GetDirectories(path))
            {
                DeleteDirectory(directory);
            }

            try
            {
                Directory.Delete(path, true);
            }
            catch (IOException)
            {
                Directory.Delete(path, true);
            }
            catch (UnauthorizedAccessException)
            {
                Directory.Delete(path, true);
            }
        }

        public async Task RunCommand(string Command)
        {
            string[] CommandChut = {Command};
            File.WriteAllLines(System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand.bat",
                CommandChut);
            await Task.Factory.StartNew(() =>
            {
                Process.Start(System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand.bat")
                    .WaitForExit();
            });
            File.Delete(System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand.bat");
        }

        private string MainDirectory = Environment.GetEnvironmentVariable("TEMP") + "\\FileVault";

        private async void Create_Load(object sender, EventArgs e)
        {
            Console.WriteLine("LOAD");
            FormBorderStyle = FormBorderStyle.FixedSingle;
            CreatePIMPanel.Visible = false;
            OpenLocationPanel.Visible = true;
            CreateDriveLetterPanel.Visible = false;
            CreatePasswordPanel.Visible = false;
            CreateSecuritySettings.Visible = false;
            CreateVaultLocationPanel.Visible = false;
            CreateVaultSize.Visible = false;
            CreationPanel.Visible = false;
            CreateVaultLocationPanel.Visible = true;
            DragAndDropCreate.AllowDrop = true;
            DragAndDropCreate.DragDrop += DragAndDropCreate_DragDrop1;
            await CheckSilentOpenVault();
            await CheckSilentCreateVault();
        }




        public char[] getAvailableDriveLetters()
        {
            List<char> availableDriveLetters = new List<char>() { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };

            DriveInfo[] drives = DriveInfo.GetDrives();

            for (int i = 0; i < drives.Length; i++)
            {
                availableDriveLetters.Remove((drives[i].Name).ToLower()[0]);
            }

            return availableDriveLetters.ToArray();
        }

        private string GetRandomDriveLetter1()
        {
            string Return = "";
            char[] AvailableDriveLetter = getAvailableDriveLetters();
            foreach (char c in AvailableDriveLetter)
            {
                Console.WriteLine(c);
                Return = c.ToString();
            }

            return Return;
        }
        
        private string GetRandomDriveLetter2()
        {
            var i = 0;
            string Return = "";
            char[] AvailableDriveLetter = getAvailableDriveLetters();
            foreach (char c in AvailableDriveLetter)
            {
                if (i == 2)
                {
                    Return = c.ToString();
                }
                i++;
            }

            return Return;
        }

        private string GetRandomDriveLetter3()
        {
            string Return = "";
            var i = 0;
            char[] AvailableDriveLetter = getAvailableDriveLetters();
            foreach (char c in AvailableDriveLetter)
            {
                if (i == 3)
                {
                    Return = c.ToString();
                }

                i++;
            }

            return Return;
        }

        private async void PlaySound(Stream location)
        {
            SoundPlayer dew = new SoundPlayer(location);
            dew.Play();
        }

        private async Task PlaySoundSync(Stream location)
        {
            SoundPlayer dew = new SoundPlayer(location);
            await Task.Factory.StartNew(() => { dew.PlaySync(); });
        }

        private async Task CheckSilentCreateVault()
        {
            if (File.Exists(Environment.GetEnvironmentVariable("TEMP") + "\\CreateVaultSettings.txt"))
            {
                string ReadFile = Environment.GetEnvironmentVariable("TEMP") + "\\CreateVaultSettings.txt";
                string Path = File.ReadLines(ReadFile).ElementAtOrDefault(0);
                string ExtraSecurity = File.ReadLines(ReadFile).ElementAtOrDefault(1);
                string UltraHash = File.ReadLines(ReadFile).ElementAtOrDefault(2);
                string FBIMode = File.ReadLines(ReadFile).ElementAtOrDefault(3);
                string CreatePassword = File.ReadLines(ReadFile).ElementAtOrDefault(4);
                string CreatePIM = File.ReadLines(ReadFile).ElementAtOrDefault(5);
                string FirstLetter = File.ReadLines(ReadFile).ElementAtOrDefault(6);
                string SecondLetter = File.ReadLines(ReadFile).ElementAtOrDefault(7);
                string ThirdLetter = File.ReadLines(ReadFile).ElementAtOrDefault(8);
                string TwoStepEmail = File.ReadLines(ReadFile).ElementAtOrDefault(9);
                string SecurityPin = File.ReadLines(ReadFile).ElementAtOrDefault(10);
                string ThreeStep = File.ReadLines(ReadFile).ElementAtOrDefault(11);
                string CreateVaultSize = File.ReadLines(ReadFile).ElementAtOrDefault(12);
                string CreateKeyFiles = File.ReadLines(ReadFile).ElementAtOrDefault(13);
                string UniqueEncryptionAlgorithm = File.ReadLines(ReadFile).ElementAtOrDefault(14);
                SaveVaultLocation.Text = Path;
                if (ExtraSecurity == "true")
                {
                    ExtraSecurityModeCreate.Checked = true;
                }

                if (UniqueEncryptionAlgorithm == "true")
                {
                    CreateUniqueEncryption.Checked = true;
                }

                if (CreateKeyFiles == "true")
                {
                    foreach (var readLine in File.ReadLines(Environment.GetEnvironmentVariable("TEMP") + "\\VaultKeyFiles.txt"))
                    {
                        CreateKeyFilesListBox.Items.Add(readLine);
                    }
                    File.Delete(Environment.GetEnvironmentVariable("TEMP") + "\\VaultKeyFiles.txt");
                    CreateKeyfilesCheckbox.Checked = true;
                }

                if (UltraHash == "true")
                {
                    CreateNewHashingCheckBox.Checked = true;
                }

                if (FBIMode == "true")
                {
                    UltraSecurityModeCreateCheckBox.Checked = true;
                    if (!Convert.ToBoolean(TwoStepEmail == "false"))
                    {
                        Verified = true;
                        CreateEmailAddressTextBox.Text = TwoStepEmail;
                        VerifiedEmail = TwoStepEmail;
                        TwoStepVerificationCreateCheckBox.Checked = true;
                        if (ThreeStep == "true")
                        {
                            Create3StepVerification d = new Create3StepVerification();
                            d.ShowDialog();
                                
                            if (File.Exists(MainDirectory + "\\Code.txt"))
                            {
                                ThreeStepVerificationCreateTextBox.Checked = true;
                            }
                            else
                            { 
                                ThreeStepVerificationCreateTextBox.Enabled = true; 
                                ThreeStepVerificationCreateTextBox.Checked = false;
                            }
                        }
                    }

                    if (!Convert.ToBoolean(SecurityPin == "false"))
                    {
                        SecurityPINCreateCheckbox.Checked = true;
                    }
                }


                VaultSize.Text = CreateVaultSize;
                Password.Text = CreatePassword;
                PasswordConfirm.Text = CreatePassword;
                PIM.Text = CreatePIM;
                DriveLetter.Text = FirstLetter;
                SecondDriveLetter.Text = SecondLetter;
                ThirdDriveLetter.Text = ThirdLetter;
                await CreateFileVault();
                File.Delete(ReadFile);
                Visible = false;
                while (!File.Exists(Environment.GetEnvironmentVariable("TEMP") + "\\CloseVault.txt"))
                {
                    await Task.Delay(10);
                }
                string GetState = File.ReadAllText(Environment.GetEnvironmentVariable("TEMP") + "\\CloseVault.txt");
                Visible = true;
                CreateVault = true;
                if (GetState == "Normal")
                {
                    await LockFileVault();
                }
                else if (GetState == "Quick")
                {
                    await DismountVeracrypt(ThirdOpenLetter.Text);
                    await DismountVeracrypt(OpenSecondLetter.Text);
                    await DismountVeracrypt(OpenFirstLetter.Text);
                    try
                    {
                        string[] UnMountVdisk = {"select vdisk file=" + OpenDirectory + "\\VDisk.vhd", "detach vdisk"};
                        File.WriteAllLines(OpenDirectory + "\\UnMountScript.txt", UnMountVdisk);
                        string[] RunScript = {"diskpart /s \"" + OpenDirectory + "\\UnMountScript.txt\""};
                        File.WriteAllLines(OpenDirectory + "\\Unmount.bat", RunScript);
                        await Task.Factory.StartNew(() =>
                        {
                            Process.Start(OpenDirectory + "\\Unmount.bat").WaitForExit();
                        });
                    }
                    catch
                    {

                    }
                }
                File.Delete(Environment.GetEnvironmentVariable("TEMP") + "\\CloseVault.txt");
                DeleteDirectory(MainDirectory);
                Application.Exit();
            }
            else
            {
                Console.WriteLine("PASS - No Silent create vault requests");
            }
        }

        public static string TwoStepEmailV = "";
        private async Task CheckSilentOpenVault()
        {
            if (File.Exists(Environment.GetEnvironmentVariable("TEMP") + "\\OpenVaultSettings.txt"))
            {
                string ReadFile = Environment.GetEnvironmentVariable("TEMP") + "\\OpenVaultSettings.txt";
                string Path = File.ReadLines(ReadFile).ElementAtOrDefault(0);
                string ExtraSecurity = File.ReadLines(ReadFile).ElementAtOrDefault(1);
                string UltraHash = File.ReadLines(ReadFile).ElementAtOrDefault(2);
                string FBIMode = File.ReadLines(ReadFile).ElementAtOrDefault(3);
                string Password = File.ReadLines(ReadFile).ElementAtOrDefault(4);
                string PIM = File.ReadLines(ReadFile).ElementAtOrDefault(5);
                string FirstLetter = File.ReadLines(ReadFile).ElementAtOrDefault(6);
                string SecondLetter = File.ReadLines(ReadFile).ElementAtOrDefault(7);
                string ThirdLetter = File.ReadLines(ReadFile).ElementAtOrDefault(8);
                string TwoStepEmail = File.ReadLines(ReadFile).ElementAtOrDefault(9);
                string SecurityPin = File.ReadLines(ReadFile).ElementAtOrDefault(10);
                string KeyFiles = File.ReadLines(ReadFile).ElementAtOrDefault(11);
                string UniqueEncryptionAlgorithm = File.ReadLines(ReadFile).ElementAtOrDefault(12);
                OpenVaultDirectory.Text = Path;
                TwoStepEmailV = TwoStepEmail;
                if (ExtraSecurity == "true")
                {
                    ExtraSecurityModeOpen.Checked = true;
                }

                if (UniqueEncryptionAlgorithm == "true")
                {
                    OpenUniqueEncryption.Checked = true;
                }

                if (UltraHash == "true")
                {
                    OpenNewHashingCheckBox.Checked = true;
                }

                if (KeyFiles == "true")
                {
                    foreach (var readLine in File.ReadLines(Environment.GetEnvironmentVariable("TEMP") + "\\VaultKeyFiles.txt"))
                    {
                        OpenKeyFilesListBox.Items.Add(readLine);
                    }
                    File.Delete(Environment.GetEnvironmentVariable("TEMP") + "\\VaultKeyFiles.txt");
                    OpenKeyFilesCheckbox.Checked = true;
                }

                if (FBIMode == "true")
                {
                    UltraSecurityOpenCheckBox.Checked = true;
                    if (!Convert.ToBoolean(TwoStepEmail == "false"))
                    {
                        VerifiedOpen = true;
                        OpenEmailAddressTextBox.Text = TwoStepEmail;
                        OpenVerifiedEmailAddress = TwoStepEmail;
                        OpenTwoStepVerificationCheckBox.Checked = true;
                        /// Identity Verification ///
                        Visible = false;
                        Verification v = new Verification();
                        v.ShowDialog();
                        if (Verification.Verified == false)
                        {
                            Application.Exit();
                        }
                        Visible = true;
                    }

                    if (!Convert.ToBoolean(SecurityPin == "false"))
                    {
                        SecurityPINOpenCheckbox.Checked = true;
                        SecurityPINOpenTextBox.Text = SecurityPin;
                    }
                }



                PasswordOpen.Text = Password;
                OpenVaultPIM.Text = PIM;
                OpenFirstLetter.Text = FirstLetter;
                OpenSecondLetter.Text = SecondLetter;
                ThirdOpenLetter.Text = ThirdLetter;
                await OpenFileVault();
                Visible = false;
                while (!File.Exists(Environment.GetEnvironmentVariable("TEMP") + "\\CloseVault.txt"))
                {
                    await Task.Delay(10);
                }
                string GetState = File.ReadAllText(Environment.GetEnvironmentVariable("TEMP") + "\\CloseVault.txt");
                Visible = true;
                if (GetState == "Normal")
                {
                    await LockFileVault();
                }
                else if (GetState == "Quick")
                {
                    await DismountVeracrypt(ThirdOpenLetter.Text);
                    await DismountVeracrypt(OpenSecondLetter.Text);
                    await DismountVeracrypt(OpenFirstLetter.Text);
                    try
                    {
                        string[] UnMountVdisk = {"select vdisk file=" + OpenDirectory + "\\VDisk.vhd", "detach vdisk"};
                        File.WriteAllLines(OpenDirectory + "\\UnMountScript.txt", UnMountVdisk);
                        string[] RunScript = {"diskpart /s \"" + OpenDirectory + "\\UnMountScript.txt\""};
                        File.WriteAllLines(OpenDirectory + "\\Unmount.bat", RunScript);
                        await Task.Factory.StartNew(() =>
                        {
                            Process.Start(OpenDirectory + "\\Unmount.bat").WaitForExit();
                        });
                    }
                    catch
                    {

                    }
                }
                File.Delete(Environment.GetEnvironmentVariable("TEMP") + "\\CloseVault.txt");
                File.Delete(ReadFile);
                DeleteDirectory(MainDirectory);
                Application.Exit();

            }
            else
            {
                Console.WriteLine("PASS - No Silent open vault requests");
            }
        }

        private string UniqueHashing(string inputstring)
        {
            File.WriteAllBytes(Environment.GetEnvironmentVariable("TEMP") + "\\Hasher.exe", Resources.Unique_Hasher);
            File.WriteAllText(
                Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\Hashing.txt",
                inputstring);

            Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\Hasher.exe").WaitForExit();

            return File.ReadLines(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) +
                                  "\\HashedString.txt").ElementAtOrDefault(0);
            File.Delete(
                Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\HashedString.txt");
        }

        private void DragAndDropCreate_DragDrop1(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
            SaveVaultLocation.Text = e.Data.GetData(DataFormats.FileDrop, true).ToString();
        }

        private async Task DismountVeracrypt(string Letter)
        {
            await RunCommand("cd \"" + MainDirectory + "\\VeraCrypt\" \n veracrypt /q /s /d " + Letter + " /f");
        }

        private async Task CreateVeracryptVolume(string Path, string Password, string Size, string PIM,string FileSystem)
        {
            Console.WriteLine("Create Veracrypt: " + "cd \"" + MainDirectory + "\\VeraCrypt\"\n\"VeraCrypt Format.exe\" /create \"" + Path +
                              "\" /password " + Password + " /pim " + PIM +
                              " /hash sha512 /encryption " + RandomAlgorithm() + " /filesystem " + FileSystem + " /size " + Size +
                              "M /force /silent /quick");
            await RunCommand("cd \"" + MainDirectory + "\\VeraCrypt\"\n\"VeraCrypt Format.exe\" /create \"" + Path +
                                   "\" /password " + Password + " /pim " + PIM +
                                   " /hash sha512 /encryption " + RandomAlgorithm() + " /filesystem " + FileSystem + " /size " + Size +
                                   "M /force /silent /quick");
        }

        private async Task MountVeracrypt(string Path, string Password, string PIM, string Letter)
        {
            await RunCommand("cd \"" + MainDirectory + "\\VeraCrypt\"" + "\n veracrypt /q /v " + Path + " /l " + Letter +
                                   " /a /p " + Password + " /pim " + PIM + " /e /b /s");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult jer = MessageBox.Show("Are you sure", "", MessageBoxButtons.YesNo);
            if (jer == DialogResult.Yes)
            {
                DeleteDirectory(MainDirectory);
                Application.Exit();
                Close();
            }
        }

        SpeechSynthesizer s = new SpeechSynthesizer();


        private async void button1_Click(object sender, EventArgs e)
        {
            await CreateFileVault();
        }

        private async Task CreateFileVault()
        {
            LockComputer();
            SoundPlayer hashPlayer = new SoundPlayer(Resources.sunny_clear_hot_warm);
            ///CREATE VAULT BUTTON///
            if (Convert.ToBoolean(DriveLetter.Text == "") != Convert.ToBoolean(SecondDriveLetter.Text == "") !=
                Convert.ToBoolean(ThirdDriveLetter.Text == ""))
            {
                MessageBox.Show("Drive letter cannot be empty or in use");
                return;
            }

            if (Password.Text == PasswordConfirm.Text && !Convert.ToBoolean(Password.Text == null) &&
                !Convert.ToBoolean(PasswordConfirm.Text == null) && !Convert.ToBoolean(VaultSize.Text == null) &&
                !Convert.ToBoolean(PIM.Text == null) && !Convert.ToBoolean(SaveVaultLocation.Text == null) &&
                !Convert.ToBoolean(DriveLetter.Text == null) && !Convert.ToBoolean(SecondDriveLetter.Text == null))
            {
                button1.Enabled = false;
                Password.Text = GetHashString(Password.Text);
                PasswordConfirm.Text = GetHashString(PasswordConfirm.Text);
                StatusProgressBar.Style = ProgressBarStyle.Marquee;
                if (CreateNewHashingCheckBox.Checked == true)
                {
                    LockComputer();
                    StatusLabel.BackColor = Color.Yellow;
                    StatusLabel.Text = "Encryption/Hashing Password";
                    SoundPlayer en = new SoundPlayer(Resources.Hashing);
                    en.PlaySync();

                    hashPlayer.PlayLooping();
                    ShowNotification("Hashing/Encrypting Password",
                        "This might take a while depending on your computer");
                    Password.Text = UniqueHashing(Password.Text);
                    PasswordConfirm.Text = UniqueHashing(PasswordConfirm.Text);
                    Password.Text = UltraHash(Password.Text);
                    Password.Text = GetHashString(Password.Text);
                    hashPlayer.Stop();
                }

                if (CreateKeyfilesCheckbox.Checked == true)
                {
                    try
                    {
                        LockComputer();
                        StatusLabel.BackColor = Color.Yellow;
                        StatusLabel.Text = "Adding Keyfiles";
                        hashPlayer.PlayLooping();
                        File.WriteAllBytes(MainDirectory + "\\KeyFileSalt.salt", Resources.KeySaltFile);
                        CreateKeyFilesListBox.Items.Add(MainDirectory + "\\KeyFileSalt.salt");
                        string[] ListItems = CreateKeyFilesListBox.Items.OfType<string>().ToArray();
                        File.WriteAllLines(MainDirectory + "\\KeyFiles.txt", ListItems);
                        if (!Directory.Exists(MainDirectory + "\\KeyFilesFolder"))
                        {
                            Directory.CreateDirectory(MainDirectory + "\\KeyFilesFolder");
                        }

                        var i = 0;
                        foreach (var readLine in File.ReadLines(MainDirectory + "\\KeyFiles.txt"))
                        {
                            await RunCommand("certutil -encode \"" + readLine + "\" \"" + MainDirectory +
                                             "\\KeyFilesFolder\\" + "KeyFile" + i + ".jer\"");
                            i++;
                        }

                        await Task.Delay(1000);
                        DirectoryInfo files = new DirectoryInfo(MainDirectory + "\\KeyFilesFolder");
                        foreach (var fileInfo in files.GetFiles())
                        {
                            File.AppendAllText(MainDirectory + "\\KeyFilesHash.txt",
                                File.ReadAllText(MainDirectory + "\\KeyFilesFolder" + "\\" + fileInfo.Name));
                            Console.WriteLine(MainDirectory + "\\KeyFilesFolder" + "\\" + fileInfo.Name);
                        }

                        Password.Text = Password.Text +
                                        UltraHash(File.ReadAllText(MainDirectory + "\\KeyFilesHash.txt"));
                        Password.Text = GetHashString(Password.Text);
                        hashPlayer.Stop();
                    }
                    catch (Exception dewd)
                    {
                        UnlockComputer();
                        Console.WriteLine(dewd);
                        throw;
                    }

                    StatusProgressBar.Style = ProgressBarStyle.Blocks;
                }

                SoundPlayer dew = new SoundPlayer(Resources.snow);
                dew.Play();
                await Task.Delay(3000);
                try
                {
                    SoundPlayer ssPlayer = new SoundPlayer(Resources.CreatingFileVault);
                    await Task.Factory.StartNew(() => { ssPlayer.PlaySync(); });
                    if (PIM.Text == "")
                    {
                        PIM.Text = "500";
                    }

                    MainTabs.Visible = false;
                    FormBorderStyle = FormBorderStyle.None;
                    WindowState = FormWindowState.Maximized;
                    TopMost = true;
                    Opacity = 0.01;
                    LockComputer();
                    await DismountVeracrypt(DriveLetter.Text);
                    await DismountVeracrypt(SecondDriveLetter.Text);
                    await DismountVeracrypt(ThirdDriveLetter.Text);
                    await EjectDrive();
                    CreateVaultPanel.Visible = false;
                    StatusLabel.BackColor = Color.Yellow;
                    StatusLabel.Text = "Encrypting Password";
                    File.WriteAllText(MainDirectory + "\\PlainPassword.txt", Password.Text);
                    await RunCommand("certutil -encode \"" + MainDirectory + "\\PlainPassword.txt\" \"" +
                                     MainDirectory + "\\EncryptedPassword.txt\"");
                    string EncryptedPassword =
                        File.ReadLines(MainDirectory + "\\EncryptedPassword.txt").ElementAtOrDefault(1) +
                        File.ReadLines(MainDirectory + "\\EncryptedPassword.txt").ElementAtOrDefault(2) +
                        File.ReadLines(MainDirectory + "\\EncryptedPassword.txt").ElementAtOrDefault(3);
                    EncryptedPasswordtext = EncryptedPassword;
                    SoundPlayer RainSound = new SoundPlayer(Resources.rain);
                    RainSound.PlayLooping();
                    ShowNotification("DiskPart", "Preparing DiskPart to create file vaults");
                    string[] Script =
                    {
                        "create vdisk file=\"%Temp%\\FileVault\\VDisk.vhd\" maximum=" +
                        (Int32.Parse(VaultSize.Text) + 100) +
                        " type=expandable",
                        "select vdisk file=\"%Temp%\\FileVault\\VDisk.vhd\"", "attach vdisk", "detail vdisk",
                        "convert mbr", "create partition primary", "format fs=ntfs label=\"install\" quick",
                        "assign letter=" + DriveLetter.Text
                    };
                    File.WriteAllLines(MainDirectory + "\\DiskPartScript.txt", Script);
                    string[] RunScript =
                        {"cd \"" + MainDirectory + "\"", "diskpart /s \"" + MainDirectory + "\\DiskPartScript.txt\""};
                    File.WriteAllLines(MainDirectory + "\\RunScript.bat", RunScript);
                    await Task.Factory.StartNew(() =>
                    {
                        Process.Start(MainDirectory + "\\RunScript.bat").WaitForExit();
                    });
                    Console.WriteLine("Created Virtual Disk");
                    Process.Start(DriveLetter.Text + ":\\");
                    Console.WriteLine("Creating Veracrypt");
                    Opacity = 100;
                    StatusLabel.Text = "Creating Virtual Disk";
                    RainSound.Stop();
                    SoundPlayer defaultdew = new SoundPlayer(Resources.default_sound);
                    defaultdew.PlayLooping();
                    if (Int32.Parse(VaultSize.Text) < 11)
                    {
                        await RunCommand("cd \"%temp%\\FileVault\\VeraCrypt\"\r\n\"Veracrypt Format.exe\" /create " +
                                         DriveLetter.Text + ":\\Vault.jer" + " /pim " + PIM.Text + " /password " +
                                         Password.Text + " /hash sha512 /encryption " + RandomAlgorithm() + " /filesystem FAT /size " +
                                         VaultSize.Text + "M /force /silent");
                    }
                    else
                    {
                        await RunCommand("cd \"%temp%\\FileVault\\VeraCrypt\"\r\n\"Veracrypt Format.exe\" /create " +
                                         DriveLetter.Text + ":\\Vault.jer" + " /pim " + PIM.Text + " /password " +
                                         Password.Text + " /hash sha512 /encryption " + RandomAlgorithm() + " /filesystem NTFS /size " +
                                         VaultSize.Text + "M /force /silent");
                    }

                    Opacity = 100;
                    bool Done = false;
                    while (!File.Exists(DriveLetter + ":\\Vault.jer") != Done == false)
                    {
                        await Task.Delay(10);
                        if (File.Exists(DriveLetter.Text + ":\\Vault.jer"))
                        {
                            Done = true;
                        }
                    }

                    StatusLabel.Text = "Preparing Vault";
                    Done = false;
                    Opacity = 100;
                    await RunCommand("cd \"%temp%\\FileVault\\VeraCrypt\" \n veracrypt /q /v " + DriveLetter.Text +
                                     ":\\Vault.jer" + " /l " + SecondDriveLetter.Text + " /a /pim " + PIM.Text +
                                     " /p " + Password.Text + " /e /b");
                    Opacity = 0;
                    while (!Directory.Exists(SecondDriveLetter.Text + ":\\"))
                    {
                        await Task.Delay(10);
                    }

                    defaultdew.Stop();
                    Process.Start(SecondDriveLetter.Text + ":\\");
                    File.WriteAllText(SecondDriveLetter.Text + ":\\Ready to use file vault.txt", "true");
                    Console.WriteLine("Mounted Veracrypt");
                    ManageVaultPanel.Visible = true;
                    MainTabs.Visible = true;
                    FormBorderStyle = FormBorderStyle.FixedSingle;
                    WindowState = FormWindowState.Normal;
                    TopMost = false;
                    Opacity = 100;
                    OpenSecuritySettingsPanel.Visible = false;
                    CreateVault = true;
                    linkLabel1.Enabled = false;
                    if (ExtraSecurityModeCreate.Checked == true)
                    {
                        SoundPlayer sound = new SoundPlayer(Resources.ExtraSecurityMode);
                        await Task.Factory.StartNew(() => { sound.PlaySync(); });

                        await Task.Delay(3000);
                        File.WriteAllText(MainDirectory + "\\ExtraEncryptPlain.txt", EncryptedPassword);
                        await RunCommand("certutil -encode \"" + MainDirectory + "\\ExtraEncryptPlain.txt\" \"" +
                                         MainDirectory + "\\ExtraEncryptHash.txt\"");
                        ExtraEncryptPassword = File.ReadLines(MainDirectory + "\\ExtraEncryptHash.txt")
                            .ElementAtOrDefault(1);
                    }

                    if (UltraSecurityModeCreateCheckBox.Checked == true)
                    {
                        if (SecurityPINCreateCheckbox.Checked == true)
                        {
                            LockMouse = false;
                            GenerateSecurityPIN s = new GenerateSecurityPIN();
                            s.ShowDialog();
                            LockMouse = true;
                        }
                    }

                    StatusLabel.Text = "Opening New Vault";
                    SoundPlayer soundPlayer = new SoundPlayer(Resources.SuccessfullyCreated);
                    soundPlayer.Play();
                    await RunCommand("taskkill /f /im explorer.exe \n start %windir%\\explorer.exe \n exit");
                    while (!Directory.Exists(SecondDriveLetter.Text + ":\\"))
                    {
                        await Task.Delay(10);
                    }

                    if (File.Exists(Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\CreateTime.txt") &&
                        !Convert.ToBoolean(VaultSize.Text == ""))
                    {
                        try
                        {
                            string t = File
                                .ReadLines(Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\CreateTime.txt")
                                .ElementAtOrDefault(0);
                            var TimePer10MB = t.Split(':')[2].Trim().Split('.')[0].Trim();
                            var EncryptionTimeDew = (Convert.ToInt32(VaultSize.Text) / 10);
                            Console.WriteLine(EncryptionTimeDew);
                            var EncryptionTime = (Convert.ToInt32(TimePer10MB) * EncryptionTimeDew);
                            var OldEncryptionTime = EncryptionTime;
                            var VeraCryptTime = 0;
                            if (UltraSecurityModeCreateCheckBox.Checked == true)
                            {
                                EncryptionTime = (OldEncryptionTime + EncryptionTime);
                                if (File.Exists(Environment.GetEnvironmentVariable("LOCALAPPDATA") +
                                                "\\VeracryptTime.txt"))
                                {
                                    string d = File
                                        .ReadLines(Environment.GetEnvironmentVariable("LOCALAPPDATA") +
                                                   "\\VeracryptTime.txt").ElementAtOrDefault(0);
                                    var TimeVeracrypt = d.Split(':')[2].Trim().Split('.')[0].Trim();
                                    VeraCryptTime = Int32.Parse(TimeVeracrypt);
                                }

                                EncryptionTime = (EncryptionTime + VeraCryptTime);
                            }

                            if (ExtraSecurityModeCreate.Checked == true)
                            {
                                EncryptionTime = ((OldEncryptionTime / 2) + EncryptionTime);
                            }

                            if (EncryptionTime > 59)
                            {
                                double dedw = Double.Parse(EncryptionTime.ToString()) / 60d;
                                string Minutes = dedw.ToString();
                                string RoundUp = Minutes.Split('.')[1].Trim();
                                int Minute = 0;
                                CloseAndLockVaultTimeLabel.BackColor = Color.Yellow;
                                CloseAndLockVaultTimeLabel.Text =
                                    "Encryption time:\n" + dew + " minutes\n(Estimated Time)\nNot 100% Accurate";
                            }
                            else
                            {
                                CloseAndLockVaultTimeLabel.BackColor = Color.Yellow;
                                CloseAndLockVaultTimeLabel.Text =
                                    "Encryption time:\n" + EncryptionTime +
                                    " seconds\n(Estimated Time)\nNot 100% Accurate";
                            }
                        }
                        catch (Exception er)
                        {
                            Console.WriteLine(er);
                        }
                    }

                    try
                    {
                        Process.Start(SecondDriveLetter.Text + ":\\");
                        UnlockComputer();
                        MainTabs.Visible = false;
                        ManageVaultPanel.Visible = true;
                        linkLabel1.Enabled = false;
                        linkLabel1.Visible = false;
                        ShowNotification("Success",
                            "File vault successfully created, vault path is " + SecondDriveLetter.Text + ":\\");
                    }
                    catch
                    {
                        UnlockComputer();
                        MainTabs.Visible = false;
                        ManageVaultPanel.Visible = true;
                        linkLabel1.Visible = false;
                        linkLabel1.Enabled = false;
                    }
                }
                catch (Exception dewdew)
                {
                    UnlockComputer();
                    TopMost = true;
                    Console.WriteLine(dewdew);
                    SystemSounds.Asterisk.Play();
                    SystemSounds.Beep.Play();
                    SystemSounds.Hand.Play();
                    SystemSounds.Exclamation.Play();
                    SystemSounds.Question.Play();
                    MessageBox.Show("Failed To Create");
                    MessageBox.Show(dewdew.ToString());
                    File.WriteAllText(
                        Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ErrorLogFileVault.txt",
                        dewdew.ToString());
                    TopMost = false;
                    MainTabs.Visible = true;
                    FormBorderStyle = FormBorderStyle.FixedSingle;
                    WindowState = FormWindowState.Normal;
                    TopMost = false;
                    Opacity = 100;
                }
            }
            else
            {
                MessageBox.Show("Passwords dont match or all fields are not yet filled in!");
            }
        }

        private string ExtraEncryptPassword = string.Empty;

        private void Password_TextChanged(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                Password.UseSystemPasswordChar = false;
                PasswordConfirm.UseSystemPasswordChar = false;
                ShowNotification("Notice", "Showing your password is not secure");
            }
            else
            {
                Password.UseSystemPasswordChar = true;
                PasswordConfirm.UseSystemPasswordChar = true;
            }
        }

        private string EncryptedPasswordtext = string.Empty;

        private async void button3_Click(object sender, EventArgs e)
        {
            LockComputer();
            try
            {
                Process.Start(SecondDriveLetter.Text + ":\\");
            }
            catch
            {

            }

            try
            {
                Process.Start(OpenSecondLetter.Text + ":\\");
            }
            catch
            {

            }

            UnlockComputer();
            linkLabel1.Visible = false;
            linkLabel1.Enabled = false;
            MainTabs.Visible = false;
        }

        private string UltraHash(string inputstring)
        {
            File.WriteAllText(
                Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\UltraHash.txt",
                inputstring);
            File.WriteAllBytes(Environment.GetEnvironmentVariable("TEMP") + "\\UltraHash.exe", Resources.Ultra_Hash);
            Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\UltraHash.exe").WaitForExit();
            File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\UltraHash.txt");
            return File.ReadLines(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) +
                                  "\\UltraHashed.txt").ElementAtOrDefault(0);
        }
        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        public static extern void BlockInput([In, MarshalAs(UnmanagedType.Bool)] bool fBlockIt);
        private void LockComputer()
        {
            linkLabel1.Enabled = false;
            linkLabel1.Visible = false;
            MainTabs.Visible = false;
            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;
            BackColor = Color.Black;
            TopMost = true;
            Opacity = 100;
            HideShowButton.Visible = true;
            LockMouse = true;
            linkLabel3.Visible = false;
        }

        private void UnlockComputer()
        {
            ShowNotification("Notice", "You can now use your computer");
            HideShowButton.Visible = false;
            linkLabel1.Visible = true;
            linkLabel1.Enabled = true;
            MainTabs.Visible = true;
            FormBorderStyle = FormBorderStyle.FixedSingle;
            WindowState = FormWindowState.Normal;
            BackColor = Color.AliceBlue;
            TopMost = false;
            Opacity = 100;
            LockMouse = false;
            linkLabel3.Visible = true;
        }

        Stopwatch sw = new Stopwatch();
        private bool CreateVault = false;

        private bool StopLoop = false;
        private async Task PlayLoopingFromDirectory(string SoundName)
        {
            string SoundLetter = File.ReadAllText(Environment.GetEnvironmentVariable("TEMP") + "\\SoundLetter.txt");

            if (File.Exists(SoundLetter + ":\\" + SoundName + ".wav"))
            {
                SoundPlayer dew = new SoundPlayer(SoundLetter + ":\\" + SoundName + ".wav");
                dew.PlayLooping();
                while (StopLoop == false)
                {
                    await Task.Delay(10);
                }
                dew.Stop();
                StopLoop = false;
            }
        }

        private async Task PlaySoundFromDirectory(string Soundname)
        {
            string SoundLetter = File.ReadAllText(Environment.GetEnvironmentVariable("TEMP") + "\\SoundLetter.txt");

            if (File.Exists(SoundLetter + ":\\" + Soundname + ".wav"))
            {
                SoundPlayer dew = new SoundPlayer(SoundLetter + ":\\" + Soundname + ".wav");
                await Task.Factory.StartNew(() =>
                {
                    dew.PlaySync();
                });
            }
        }
        private async void button5_Click(object sender, EventArgs e)
        {
            await LockFileVault();
        }

        string RandomAlgorithm()
        {
            string Return = "";
            Random random = new Random();
            int AlgoritmNumber = random.Next(1, 10);
            if (AlgoritmNumber == 1)
            {
                Return = "AES(Twofish(Serpent))";
            }
            else if (AlgoritmNumber == 2)
            {
                Return = "Serpent(Twofish(AES))";
            }
            else if (AlgoritmNumber == 3)
            {
                Return = "Twofish(Serpent)";
            }
            else if (AlgoritmNumber == 4)
            {
                Return = "Serpent(AES)";
            }
            else if (AlgoritmNumber == 5)
            {
                Return = "AES";
            }
            else
            {
                Return = "AES(Twofish(Serpent))";
            }

            return Return;
        }
        SoundPlayer GentleSpringRain = new SoundPlayer(Resources.GentleSpringRain);
        EncryptionClass Encryption = new EncryptionClass();
        private async Task LockFileVault()
        {
            LockComputer();
            StatusLabel.Text = "Encrypting First Layer\nBEWARE OF BITDEFENDER";
            await PlaySoundSync(Resources.BeingLocked);
            try
            {
                SoundPlayer sunnyPlayer = new SoundPlayer(Resources.sunny_clear_hot_warm);
                SoundPlayer morningflower = new SoundPlayer(Resources.Samsung_GALAXY_S4_Alarms___Morning_Flower);
                SoundPlayer walkInforest = new SoundPlayer(Resources.Samsung_GALAXY_S4_Alarms___Walk_in_the_forest);
                ///LOCK VAULT BUTTON///
                ManageVaultPanel.Visible = false;
                await ShowNotification("Creating File Vault",
                    "Your computer will be locked while file vault creation in progress");
                //if (!Convert.ToBoolean(Directory.Exists(MainDirectory) + "\\WinRAR"))
                //{
                //    ZipFile.ExtractToDirectory(MainDirectory + "\\WinRAR.zip",  "\\WinRAR");
                //}
                if (CreateVault == true)
                {
                    CreateVault = false;
                    LockComputer();
                    Opacity = 100;
                    await RunCommand("cd \"" + MainDirectory + "\\VeraCrypt\" \n veracrypt /q /d " +
                                     SecondDriveLetter.Text + " /force /silent");
                    while (Directory.Exists(SecondDriveLetter.Text + ":\\"))
                    {
                        await Task.Delay(10);
                    }

                    string[] UnMountVdisk = { "select vdisk file=" + MainDirectory + "\\VDisk.vhd", "detach vdisk" };
                    File.WriteAllLines(MainDirectory + "\\UnMountScript.txt", UnMountVdisk);
                    string[] RunScript = { "diskpart /s \"" + MainDirectory + "\\UnMountScript.txt\"" };
                    File.WriteAllLines(MainDirectory + "\\Unmount.bat", RunScript);
                    await Task.Factory.StartNew(() =>
                    {
                        Process.Start(MainDirectory + "\\Unmount.bat").WaitForExit();
                    });
                    Directory.CreateDirectory(MainDirectory + "\\RarDirectory");
                    File.Move(MainDirectory + "\\VDisk.vhd", MainDirectory + "\\RarDirectory\\VDisk.vhd");
                    Opacity = 0.01;
                    morningflower.PlayLooping();
                    await Rar(SaveVaultLocation.Text, MainDirectory + "\\RarDirectory", EncryptedPasswordtext);
                    Opacity = 100;
                    Console.WriteLine("RAR");
                    morningflower.Stop();
                    int SecuritySize = 0;
                    if (Int32.Parse(VaultSize.Text) < 500)
                    {
                        SecuritySize = Int32.Parse(VaultSize.Text) + 28;
                    }
                    else if (Int32.Parse(VaultSize.Text) > 499 && Int32.Parse(VaultSize.Text) < 1000)
                    {
                        SecuritySize = Int32.Parse(VaultSize.Text) + 70;
                    }
                    else if (Int32.Parse(VaultSize.Text) > 999)
                    {
                        SecuritySize = Int32.Parse(VaultSize.Text) + 110;
                    }
                    else
                    {
                        SecuritySize = Int32.Parse(VaultSize.Text) + 120;
                    }

                    if (TwoStepVerificationCreateCheckBox.Checked == true)
                    {
                        SecuritySize = SecuritySize + 30;
                    }

                    StatusProgressBar.Value = 25;
                    if (UltraSecurityModeCreateCheckBox.Checked == true)
                    {
                        string Algorithm()
                        {
                            string Return = "";
                            Random random = new Random();
                            int AlgoritmNumber = random.Next(1, 10);
                            if (AlgoritmNumber == 1)
                            {
                                Return = "AES(Twofish(Serpent))";
                            }
                            else if (AlgoritmNumber == 2)
                            {
                                Return = "Serpent(Twofish(AES))";
                            }
                            else if (AlgoritmNumber == 3)
                            {
                                Return = "Twofish(Serpent)";
                            }
                            else if (AlgoritmNumber == 4)
                            {
                                Return = "Serpent(AES)";
                            }
                            else if (AlgoritmNumber == 5)
                            {
                                Return = "AES";
                            }
                            else
                            {
                                Return = "AES(Twofish(Serpent))";
                            }

                            return Return;
                        }

                        await ShowNotification("Ultra Security Mode",
                            "Ultra Security Mode Has Been Activated, this will take a while");
                        StatusLabel.BackColor = Color.Purple;
                        StatusLabel.Text = "Creating Second Veracrypt";
                        SoundPlayer s = new SoundPlayer(Resources.UltraSecurityMode);
                        await Task.Factory.StartNew(() => { s.PlaySync(); });
                        BackColor = Color.Red;
                        SoundPlayer Defaultsound = new SoundPlayer(Resources.default_sound);
                        Defaultsound.PlayLooping();
                        if (BenchmarkMode == true)
                        {
                            sw.Start();
                        }

                        if (Int32.Parse(VaultSize.Text) < 11)
                        {
                            Opacity = 100;

                            if (SecurityPINCreateCheckbox.Checked == true)
                            {
                                await RunCommand(
                                    "cd \"%temp%\\FileVault\\VeraCrypt\"\r\n\"Veracrypt Format.exe\" /create " +
                                    MainDirectory + "\\SecurityVault.jer" + " /pim " + "600" + " /password " +
                                    Password.Text +
                                    File.ReadLines(MainDirectory + "\\GeneratedPIN.txt").ElementAtOrDefault(0) +
                                    " /hash sha512 /encryption " + Algorithm() + " /filesystem FAT /size " +
                                    SecuritySize + "M /force /silent");
                            }
                            else
                            {
                                await RunCommand(
                                    "cd \"%temp%\\FileVault\\VeraCrypt\"\r\n\"Veracrypt Format.exe\" /create " +
                                    MainDirectory + "\\SecurityVault.jer" + " /pim " + "600" + " /password " +
                                    Password.Text + " /hash sha512 /encryption " + Algorithm() +
                                    " /filesystem FAT /size " +
                                    SecuritySize + "M /force /silent");
                            }
                        }
                        else
                        {
                            Opacity = 100;
                            if (SecurityPINCreateCheckbox.Checked == true)
                            {
                                await RunCommand(
                                    "cd \"%temp%\\FileVault\\VeraCrypt\"\r\n\"Veracrypt Format.exe\" /create " +
                                    MainDirectory + "\\SecurityVault.jer" + " /pim " + "600" + " /password " +
                                    Password.Text +
                                    File.ReadLines(MainDirectory + "\\GeneratedPIN.txt").ElementAtOrDefault(0) +
                                    " /hash sha512 /encryption " + Algorithm() + " /filesystem FAT /size " +
                                    SecuritySize + "M /force /silent");
                            }
                            else
                            {
                                await RunCommand(
                                    "cd \"%temp%\\FileVault\\VeraCrypt\"\r\n\"Veracrypt Format.exe\" /create " +
                                    MainDirectory + "\\SecurityVault.jer" + " /pim " + "600" + " /password " +
                                    Password.Text + " /hash sha512 /encryption " + Algorithm() +
                                    " /filesystem FAT /size " +
                                    SecuritySize + "M /force /silent");
                            }
                        }

                        StatusProgressBar.Value = 50;
                        BackColor = Color.Yellow;
                        StatusLabel.Text = "Encryption In Process";
                        if (SecurityPINCreateCheckbox.Checked == true)
                        {
                            await RunCommand("cd \"%temp%\\FileVault\\VeraCrypt\" \n veracrypt /q /v " + MainDirectory +
                                             "\\SecurityVault.jer" + " /l " + ThirdDriveLetter.Text + " /a /pim " +
                                             "600" +
                                             " /p " + Password.Text +
                                             File.ReadLines(MainDirectory + "\\GeneratedPIN.txt")
                                                 .ElementAtOrDefault(0) +
                                             " /e /b");
                        }
                        else
                        {
                            await RunCommand("cd \"%temp%\\FileVault\\VeraCrypt\" \n veracrypt /q /v " + MainDirectory +
                                             "\\SecurityVault.jer" + " /l " + ThirdDriveLetter.Text + " /a /pim " +
                                             "600" +
                                             " /p " + Password.Text + " /e /b");
                        }

                        StatusProgressBar.Value = 75;
                        while (!Directory.Exists(ThirdDriveLetter.Text + ":\\"))
                        {
                            await Task.Delay(10);
                        }

                        Defaultsound.Stop();
                        File.WriteAllText(ThirdDriveLetter.Text + ":\\FileSize.txt", VaultSize.Text);
                        ///Lock vault create two step verification///
                        if (TwoStepVerificationCreateCheckBox.Checked == true)
                        {
                            await PlaySoundSync(Resources.TwoStepActivated);
                            string ThirdVeraCryptPassword = string.Empty;
                            try
                            {
                                StatusLabel.Text = "Enabling 2 Step Verification";
                                //PlayLoopingFromDirectory("GentleSpringRain");
                                GentleSpringRain.PlayLooping();
                                string EncryptedEmail = UltraHash(VerifiedEmail);
                                ThirdVeraCryptPassword = GetHashString(EncryptedEmail);
                                File.WriteAllText(ThirdDriveLetter.Text + ":\\" + GetHashString("Email") + ".txt",
                                    EncryptedEmail);
                                GentleSpringRain.Stop();
                            }
                            catch (Exception huihui)
                            {
                                UnlockComputer();
                                Console.WriteLine(huihui);
                                throw;
                            }

                            if (ThreeStepVerificationCreateTextBox.Checked == true)
                            {
                                await PlaySoundSync(Resources.ThreeStepActivated);
                                StatusLabel.Text = "Enabling 3 Step Verification";
                                walkInforest.PlayLooping();
                                File.WriteAllText(
                                    ThirdDriveLetter.Text + ":\\" + GetHashString("Verification") + ".txt",
                                    File.ReadAllText(MainDirectory + "\\Code.txt"));
                                ThirdVeraCryptPassword =
                                    ThirdVeraCryptPassword +
                                    GetHashString(File.ReadAllText(MainDirectory + "\\Code.txt"));
                                walkInforest.Stop();
                            }
                            Console.WriteLine("Doing the 3 step veracrypt thing");
                            walkInforest.PlayLooping();
                            ThirdVeraCryptPassword = UltraHash(ThirdVeraCryptPassword);
                            walkInforest.Stop();
                            Defaultsound.PlayLooping();
                            int ThirdSecuritySize = Int32.Parse(VaultSize.Text) + 30;
                            await CreateVeracryptVolume(MainDirectory + "\\ThirdVeracrypt.jer", ThirdVeraCryptPassword, ThirdSecuritySize.ToString(), "500", "FAT");
                            await Task.Delay(1000);
                            Opacity = 100;
                            StatusLabel.Text = "Injecting 2/3 step verification\nFans might spin";
                            await MountVeracrypt(MainDirectory + "\\ThirdVeracrypt.jer", ThirdVeraCryptPassword, "500",
                                SecondDriveLetter.Text);
                            File.Move(SaveVaultLocation.Text, SecondDriveLetter.Text + ":\\TREASURE.txt");
                            await DismountVeracrypt(SecondDriveLetter.Text);
                            var RetryCount = 1000;
                            try
                            {
                                File.Copy(MainDirectory + "\\ThirdVeracrypt.jer", ThirdDriveLetter.Text + ":\\TREASURE.txt");
                                if (File.Exists(ThirdDriveLetter.Text + ":\\TREASURE.txt"))
                                {
                                    Console.WriteLine("Moved 3 Step TREASURE file");
                                }
                            }
                            catch (Exception ddddd)
                            {
                                Console.WriteLine("HUGE EXCEPTION ==> " + ddddd + " <==");
                                throw;
                            }
                            Defaultsound.Stop();
                        }

                        StatusLabel.Text = "Unmounting Volume";
                        Defaultsound.PlayLooping();
                        if (!TwoStepVerificationCreateCheckBox.Checked == true)
                        {
                            File.Move(SaveVaultLocation.Text, ThirdDriveLetter.Text + ":\\TREASURE.txt");
                        }

                        await Task.Delay(3000);
                        await RunCommand("cd \"" + MainDirectory + "\\VeraCrypt\" \n veracrypt /q /d " +
                                         ThirdDriveLetter.Text + " /force /silent");
                        while (Directory.Exists(ThirdDriveLetter.Text + ":\\"))
                        {
                            await Task.Delay(10);
                        }

                        if (BenchmarkMode == true)
                        {
                            sw.Stop();
                            File.WriteAllText(
                                Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\VeracryptTime.txt",
                                sw.Elapsed.ToString());
                            sw.Reset();
                        }

                        Defaultsound.Stop();
                        StatusProgressBar.Value = 80;
                        await Task.Delay(1000);
                        SoundPlayer ThunderSound = new SoundPlayer(Resources.thunder);
                        ThunderSound.PlayLooping();
                        if (BenchmarkMode == true)
                        {
                            sw.Start();
                        }

                        await Rar(SaveVaultLocation.Text, MainDirectory + "\\SecurityVault.jer", EncryptedPasswordtext);
                        if (BenchmarkMode == true)
                        {
                            sw.Stop();
                            File.WriteAllText(
                                Environment.GetEnvironmentVariable("USERPROFILE") + "\\AppData\\Local\\CreateTime.txt",
                                sw.Elapsed.ToString());
                        }

                        ThunderSound.Stop();
                        StatusLabel.Text = "Closing Veracrypt";
                        BackColor = Color.Green;
                    }

                    StatusProgressBar.Value = 90;
                    if (ExtraSecurityModeCreate.Checked == true)
                    {
                        await ShowNotification("Extra security mode", "Extra security mode has been activated");
                        SoundPlayer dPlayer = new SoundPlayer(Resources.ExtraSecurityMode);
                        await Task.Factory.StartNew(() => { dPlayer.PlaySync(); });

                        await Task.Delay(3000);
                        Opacity = 0.01;
                        StatusLabel.Text = "";
                        BackColor = Color.Black;
                        SoundPlayer ThunderSound = new SoundPlayer(Resources.thunder);
                        ThunderSound.PlayLooping();
                        await SevenZCreatePassword(SaveVaultLocation.Text + "extra", SaveVaultLocation.Text,
                            ExtraEncryptPassword);
                        ThunderSound.Stop();
                        File.Delete(SaveVaultLocation.Text);
                        string Argument = "/select, \"" + SaveVaultLocation.Text + "extra" + "\"";
                        Process.Start("explorer.exe", Argument);
                    }
                    else if(CreateUniqueEncryption.Checked == false)
                    {
                        string Argument = "/select, \"" + SaveVaultLocation.Text + "\"";
                        Process.Start("explorer.exe", Argument);
                    }

                    if (CreateUniqueEncryption.Checked == true)
                    {
                        StatusLabel.Text = "Unique Encryption";
                        await PlaySoundSync(Resources.UniqueEncryption);
                        string PasswordEncryption = UniqueHashing(Password.Text);
                        BackgroundWorker EncryptorWaiter = new BackgroundWorker();
                        string ThePath = SaveVaultLocation.Text;
                        EncryptorWaiter.DoWork += async (sender, args) =>
                        {
                            try
                            {
                                Encryption.FileEncrypt(ThePath, PasswordEncryption);
                                Console.WriteLine("Encryption Complete");
                                await Task.Delay(TimeSpan.FromSeconds(1));
                                File.Delete(ThePath);
                                File.Move(ThePath + ".aes", ThePath);
                            }
                            catch (Exception ex)
                            {
                                UnlockComputer();
                                Console.WriteLine(ex);
                                Console.WriteLine(ThePath);
                                throw;
                            }
                        };
                        try
                        {
                            FinalFlash.PlayLooping();
                        }
                        catch
                        {

                        }

                        EncryptorWaiter.RunWorkerAsync();
                        while (EncryptorWaiter.IsBusy)
                        {
                            await Task.Delay(10);
                        }

                        try
                        {
                            FinalFlash.Stop();
                        }
                        catch 
                        {

                        }
                        string Argument = "/select, \"" + Path.GetDirectoryName(SaveVaultLocation.Text) + "\\" + Path.GetFileNameWithoutExtension(SaveVaultLocation.Text) + ".aes" + "\"";
                        Process.Start("explorer.exe", Argument);
                    }
                    StatusProgressBar.Value = 100;
                    CreateVaultLocationPanel.Visible = true;
                    CreationPanel.Visible = false;
                    StatusLabel.Text = "";
                    CreateVaultPanel.Visible = true;
                    OpenSecuritySettingsPanel.Visible = true;
                    ManageVaultPanel.Visible = false;
                    UnlockComputer();
                }
                else
                {
                    LockComputer();
                    s.Speak("You will not be able to use your computer while we encrypt your files");
                    StatusLabel.BackColor = Color.Yellow;
                    StatusLabel.Text = "Encryption In Progress";
                    Opacity = 100;
                    StatusProgressBar.Value = 10;
                    await RunCommand("cd \"" + MainDirectory + "\\VeraCrypt\" \n veracrypt /q /d " +
                                     OpenSecondLetter.Text +
                                     " /force /silent");
                    while (Directory.Exists(OpenSecondLetter.Text + ":\\"))
                    {
                        await Task.Delay(10);
                    }

                    StatusProgressBar.Value = 15;
                    string[] UnMountVdisk = { "select vdisk file=" + OpenDirectory + "\\VDisk.vhd", "detach vdisk" };
                    File.WriteAllLines(OpenDirectory + "\\UnMountScript.txt", UnMountVdisk);
                    string[] RunScript = { "diskpart /s \"" + OpenDirectory + "\\UnMountScript.txt\"" };
                    File.WriteAllLines(OpenDirectory + "\\Unmount.bat", RunScript);
                    await Task.Factory.StartNew(() =>
                    {
                        Process.Start(OpenDirectory + "\\Unmount.bat").WaitForExit();
                    });
                    if (!Directory.Exists(MainDirectory + "\\RarDirectory"))
                    {
                        Directory.CreateDirectory(MainDirectory + "\\RarDirectory");
                    }

                    File.Move(OpenDirectory + "\\VDisk.vhd", MainDirectory + "\\RarDirectory\\VDisk.vhd");
                    if (File.Exists(OpenVaultDirectory.Text))
                    {
                        File.Delete(OpenVaultDirectory.Text);
                    }

                    Opacity = 0.01;
                    int SecuritySize = 0;
                    if (Int32.Parse(VaultContainerSize) < 500)
                    {
                        SecuritySize = Int32.Parse(VaultContainerSize) + 38;
                    }
                    else if (Int32.Parse(VaultContainerSize) > 499 && Int32.Parse(VaultContainerSize) < 1000)
                    {
                        SecuritySize = Int32.Parse(VaultContainerSize) + 80;
                    }
                    else if (Int32.Parse(VaultContainerSize) > 999)
                    {
                        SecuritySize = Int32.Parse(VaultContainerSize) + 120;
                    }
                    else
                    {
                        SecuritySize = Int32.Parse(VaultContainerSize) + 130;
                    }

                    if (OpenTwoStepVerificationCheckBox.Checked == true)
                    {
                        SecuritySize = SecuritySize + 30;
                    }

                    sunnyPlayer.PlayLooping();
                    await Rar(OpenVaultDirectory.Text, MainDirectory + "\\RarDirectory", EncryptedPasswordtext);
                    sunnyPlayer.Stop();
                    if (UltraSecurityOpenCheckBox.Checked == true)
                    {
                        string Algorithm()
                        {
                            string Return = "";
                            Random random = new Random();
                            int AlgoritmNumber = random.Next(1, 10);
                            if (AlgoritmNumber == 1)
                            {
                                Return = "AES(Twofish(Serpent))";
                            }
                            else if (AlgoritmNumber == 2)
                            {
                                Return = "Serpent(Twofish(AES))";
                            }
                            else if (AlgoritmNumber == 3)
                            {
                                Return = "Twofish(Serpent)";
                            }
                            else if (AlgoritmNumber == 4)
                            {
                                Return = "Serpent(AES)";
                            }
                            else if (AlgoritmNumber == 5)
                            {
                                Return = "AES";
                            }
                            else
                            {
                                Return = "AES(Twofish(Serpent))";
                            }

                            return Return;
                        }

                        SoundPlayer s = new SoundPlayer(Resources.UltraSecurityMode);
                        await Task.Factory.StartNew(() => { s.PlaySync(); });

                        Opacity = 100;
                        BackColor = Color.Red;
                        StatusLabel.BackColor = Color.Purple;
                        StatusLabel.Text = "Creating Veracrypt";
                        SoundPlayer Defaultsound = new SoundPlayer(Resources.default_sound);
                        Defaultsound.PlayLooping();
                        if (Int32.Parse(VaultContainerSize) < 11)
                        {
                            if (SecurityPINOpenCheckbox.Checked == true)
                            {
                                await RunCommand(
                                    "cd \"%temp%\\FileVault\\VeraCrypt\"\r\n\"Veracrypt Format.exe\" /create " +
                                    MainDirectory + "\\SecurityVault.jer" + " /pim " + "600" + " /password " +
                                    PasswordOpen.Text + SecurityPINOpenTextBox.Text + " /hash sha512 /encryption " +
                                    Algorithm() + " /filesystem FAT /size " +
                                    SecuritySize + "M /force /silent");
                            }
                            else
                            {
                                await RunCommand(
                                    "cd \"%temp%\\FileVault\\VeraCrypt\"\r\n\"Veracrypt Format.exe\" /create " +
                                    MainDirectory + "\\SecurityVault.jer" + " /pim " + "600" + " /password " +
                                    PasswordOpen.Text + " /hash sha512 /encryption " + Algorithm() +
                                    " /filesystem FAT /size " +
                                    SecuritySize + "M /force /silent");
                            }
                        }
                        else
                        {
                            if (SecurityPINOpenCheckbox.Checked == true)
                            {
                                await RunCommand(
                                    "cd \"%temp%\\FileVault\\VeraCrypt\"\r\n\"Veracrypt Format.exe\" /create " +
                                    MainDirectory + "\\SecurityVault.jer" + " /pim " + "600" + " /password " +
                                    PasswordOpen.Text + SecurityPINOpenTextBox.Text + " /hash sha512 /encryption " +
                                    Algorithm() + " /filesystem FAT /size " +
                                    SecuritySize + "M /force /silent");
                            }
                            else
                            {
                                await RunCommand(
                                    "cd \"%temp%\\FileVault\\VeraCrypt\"\r\n\"Veracrypt Format.exe\" /create " +
                                    MainDirectory + "\\SecurityVault.jer" + " /pim " + "600" + " /password " +
                                    PasswordOpen.Text + " /hash sha512 /encryption " + Algorithm() +
                                    " /filesystem FAT /size " +
                                    SecuritySize + "M /force /silent");
                            }
                        }

                        StatusProgressBar.Value = 30;
                        BackColor = Color.Yellow;
                        StatusLabel.Text = "Encrypting Files";
                        if (SecurityPINOpenCheckbox.Checked == true)
                        {
                            await RunCommand("cd \"%temp%\\FileVault\\VeraCrypt\" \n veracrypt /q /v " + MainDirectory +
                                             "\\SecurityVault.jer" + " /l " + ThirdOpenLetter.Text + " /a /pim " +
                                             "600" +
                                             " /p " + PasswordOpen.Text + SecurityPINOpenTextBox.Text + " /e /b");
                        }
                        else
                        {
                            await RunCommand("cd \"%temp%\\FileVault\\VeraCrypt\" \n veracrypt /q /v " + MainDirectory +
                                             "\\SecurityVault.jer" + " /l " + ThirdOpenLetter.Text + " /a /pim " +
                                             "600" +
                                             " /p " + PasswordOpen.Text + " /e /b");
                        }

                        StatusProgressBar.Value = 40;
                        while (!Directory.Exists(ThirdOpenLetter.Text + ":\\"))
                        {
                            await Task.Delay(10);
                        }

                        StatusProgressBar.Value = 50;
                        Defaultsound.Stop();
                        File.WriteAllText(ThirdOpenLetter.Text + ":\\FileSize.txt", VaultContainerSize);
                        ///Lock vault open two step verification///
                        if (OpenTwoStepVerificationCheckBox.Checked == true)
                        {
                            await PlaySoundSync(Resources.TwoStepActivated);
                            string ThirdVeraCryptPassword = string.Empty;
                            try
                            {
                                StatusLabel.Text = "Enabling 2 Step Verification";
                                //PlayLoopingFromDirectory("GentleSpringRain");
                                GentleSpringRain.PlayLooping();
                                string EncryptedEmail = UltraHash(OpenVerifiedEmailAddress);
                                ThirdVeraCryptPassword = GetHashString(EncryptedEmail);
                                File.WriteAllText(ThirdOpenLetter.Text + ":\\" + GetHashString("Email") + ".txt",
                                    EncryptedEmail);
                                GentleSpringRain.Stop();
                            }
                            catch (Exception huihui)
                            {
                                UnlockComputer();
                                Console.WriteLine(huihui);
                                throw;
                            }

                            try
                            {
                                if (File.Exists(MainDirectory + "\\Code.txt"))
                                {
                                    await PlaySoundSync(Resources.ThreeStepActivated);
                                    StatusLabel.Text = "Enabling 3 Step Verification";
                                    walkInforest.PlayLooping();
                                    File.WriteAllText(
                                        ThirdOpenLetter.Text + ":\\" + GetHashString("Verification") + ".txt",
                                        File.ReadAllText(MainDirectory + "\\Code.txt"));
                                    ThirdVeraCryptPassword =
                                        ThirdVeraCryptPassword +
                                        GetHashString(File.ReadAllText(MainDirectory + "\\Code.txt"));
                                    walkInforest.Stop();
                                }

                                walkInforest.PlayLooping();
                                ThirdVeraCryptPassword = UltraHash(ThirdVeraCryptPassword);
                                walkInforest.Stop();
                                int ThirdSecuritySize = Int32.Parse(VaultContainerSize) + 30;
                                Defaultsound.PlayLooping();
                                StatusLabel.Text = "Injecting 2/3 Step Verification\nFans might spin";
                                await CreateVeracryptVolume(MainDirectory + "\\ThirdVeracrypt.jer",
                                    ThirdVeraCryptPassword, ThirdSecuritySize.ToString(), "500", "FAT");
                                await Task.Delay(1000);
                                await MountVeracrypt(MainDirectory + "\\ThirdVeracrypt.jer", ThirdVeraCryptPassword,
                                    "500",
                                    OpenSecondLetter.Text);
                                File.Move(OpenVaultDirectory.Text, OpenSecondLetter.Text + ":\\TREASURE.txt");
                                await DismountVeracrypt(OpenSecondLetter.Text);
                                File.Copy(MainDirectory + "\\ThirdVeracrypt.jer",
                                    ThirdOpenLetter.Text + ":\\TREASURE.txt");
                                Defaultsound.Stop();
                            }
                            catch (Exception dd)
                            {
                                UnlockComputer();
                                Console.WriteLine(dd);
                                throw;
                            }
                        }

                        if (!OpenTwoStepVerificationCheckBox.Checked == true)
                        {
                            File.Move(OpenVaultDirectory.Text, ThirdOpenLetter.Text + ":\\TREASURE.txt");
                        }

                        await Task.Delay(3000);
                        StatusLabel.Text = "Unmounting Volume";
                        Defaultsound.PlayLooping();
                        await RunCommand("cd \"" + MainDirectory + "\\VeraCrypt\" \n veracrypt /q /d " +
                                         ThirdOpenLetter.Text + " /force /silent");
                        while (Directory.Exists(ThirdOpenLetter.Text + ":\\"))
                        {
                            await Task.Delay(10);
                        }

                        Defaultsound.Stop();
                        StatusProgressBar.Value = 70;
                        StatusLabel.Text = "Closing Veracrypt";
                        BackColor = Color.Green;
                        await Task.Delay(1000);
                        SoundPlayer ThunderSound = new SoundPlayer(Resources.thunder);
                        ThunderSound.PlayLooping();
                        await Rar(OpenVaultDirectory.Text, MainDirectory + "\\SecurityVault.jer",
                            EncryptedPasswordtext);
                        ThunderSound.Stop();
                    }
                    StatusProgressBar.Value = 80;
                    if (ExtraSecurityModeOpen.Checked == true)
                    {
                        SoundPlayer soundPlayer = new SoundPlayer(Resources.ExtraSecurityMode);
                        await Task.Factory.StartNew(() => { soundPlayer.PlaySync(); });

                        await Task.Delay(3000);
                        Opacity = 0.01;
                        StatusLabel.Text = "";
                        BackColor = Color.Black;
                        SoundPlayer ThunderSound = new SoundPlayer(Resources.thunder);
                        ThunderSound.PlayLooping();
                        await SevenZCreatePassword(OpenVaultDirectory.Text + "extra", OpenVaultDirectory.Text,
                            ExtraEncryptedOpenPassword);
                        ThunderSound.Stop();
                        File.Delete(OpenVaultDirectory.Text);
                        File.Move(OpenVaultDirectory.Text + "extra", OpenVaultDirectory.Text);
                        string Argument = "/select, \"" + OpenVaultDirectory.Text + "\"";
                        Process.Start("explorer.exe", Argument);
                        StatusProgressBar.Value = 90;
                    }
                    else if(OpenUniqueEncryption.Checked == false)
                    {
                        string Argument = "/select, \"" + OpenVaultDirectory.Text + "\"";
                        Process.Start("explorer.exe", Argument);
                    }

                    if (OpenUniqueEncryption.Checked == true)
                    {
                        await PlaySoundSync(Resources.UniqueEncryption);
                        Opacity = 100;
                        StatusLabel.Text = "Unique Encryption";
                        string PasswordEncryption = UniqueHashing(PasswordOpen.Text);
                        BackgroundWorker EncryptorWaiter = new BackgroundWorker();
                        string ThePath = OpenVaultDirectory.Text;
                        EncryptorWaiter.DoWork += async (sender, args) =>
                        {
                            try
                            {
                                Encryption.FileEncrypt(ThePath, PasswordEncryption);
                                await Task.Delay(TimeSpan.FromSeconds(1));
                                File.Delete(ThePath);
                                File.Move(ThePath + ".aes", ThePath);
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex);
                                Console.WriteLine(ThePath);
                                UnlockComputer();
                                throw;
                            }
                        };
                        try
                        {
                            FinalFlash.PlayLooping();
                        }
                        catch
                        {

                        }

                        EncryptorWaiter.RunWorkerAsync();
                        while (EncryptorWaiter.IsBusy)
                        {
                            await Task.Delay(10);
                        }

                        try
                        {
                            FinalFlash.Stop();
                        }
                        catch
                        {

                        }
                        string Argument = "/select, \"" + OpenVaultDirectory.Text + "\"";
                        Process.Start("explorer.exe", Argument);
                    }

                    StatusProgressBar.Value = 100;
                    CreateVaultPanel.Visible = true;
                    OpenSecuritySettingsPanel.Visible = true;
                    ManageVaultPanel.Visible = false;
                    StatusLabel.Text = "";
                    UnlockComputer();
                    OpenPasswordPanel.Visible = false;
                    OpenLocationPanel.Visible = true;
                }

                linkLabel1.Enabled = true;
                MainTabs.Visible = true;
                DeleteDirectory(MainDirectory);
                await RefreshMainDirectory();
                await RefreshMainTabs();
                SoundPlayer d = new SoundPlayer(Resources.Locked);
                await Task.Factory.StartNew(() =>
                {
                    d.PlaySync();
                });
            }
            catch (Exception ooo)
            {
                Console.WriteLine(ooo);
                File.WriteAllText(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ErrorLog.txt", ooo.ToString());
                Process.Start(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ErrorLog.txt");
                UnlockComputer();
                throw;
            }
        }
        private async Task<string> EncodeDecodeToBase64String(string input, bool Encode)
        {
            string Return = "";
            File.WriteAllText(Environment.GetEnvironmentVariable("TEMP") + "\\Encode.txt", input);
            if (Encode == true)
            {
                await RunCommandHidden("certutil -encode \"" + Environment.GetEnvironmentVariable("TEMP") +
                                       "\\Encode.txt" + "\" \"" + Environment.GetEnvironmentVariable("TEMP") +
                                       "\\Encoded.txt\"");
                Return = File.ReadAllText(Environment.GetEnvironmentVariable("TEMP") + "\\Encoded.txt");
                File.Delete(Environment.GetEnvironmentVariable("TEMP") + "\\Encode.txt");
                File.Delete(Environment.GetEnvironmentVariable("TEMP") + "\\Encoded.txt");
            }
            else
            {
                await RunCommandHidden("certutil -decode \"" + Environment.GetEnvironmentVariable("TEMP") +
                                       "\\Encode.txt" + "\" \"" + Environment.GetEnvironmentVariable("TEMP") +
                                       "\\Encoded.txt\"");
                Return = File.ReadAllText(Environment.GetEnvironmentVariable("TEMP") + "\\Encoded.txt");
                File.Delete(Environment.GetEnvironmentVariable("TEMP") + "\\Encode.txt");
                File.Delete(Environment.GetEnvironmentVariable("TEMP") + "\\Encoded.txt");
            }

            return Return;
        }
        // Usage Example: string Jerjer = await EncodeDecodeToBase64String(input, [true or false])//
        //true = encode, false = decode//
        private async Task RarSplit(string Password, string Archive, string Files, string Size)
        {
            string[] Run =
            {
                "cd \"" + MainDirectory + "\\WinRAR" + "\"",
                "rar a -v" + Size + "M -p" + Password + " \"" + Archive + "\"" + " " + "\"" + Files + "\""
            };
            File.WriteAllLines("C:\\Unrar.bat", Run);
            var dew = Process.Start("C:\\Unrar.bat");
            dew.WaitForExit();
        }

        private async Task ShowNotification(string title, string message)
        {
            File.WriteAllText(Environment.GetEnvironmentVariable("TEMP") + "\\Caller.bat",
                "@if (@X)==(@Y) @end /* JScript comment\r\n@echo off\r\n\r\nsetlocal\r\ndel /q /f %~n0.exe >nul 2>&1\r\nfor /f \"tokens=* delims=\" %%v in ('dir /b /s /a:-d  /o:-n \"%SystemRoot%\\Microsoft.NET\\Framework\\*jsc.exe\"') do (\r\n   set \"jsc=%%v\"\r\n)\r\n\r\nif not exist \"%~n0.exe\" (\r\n    \"%jsc%\" /nologo /out:\"%~n0.exe\" \"%~dpsfnx0\"\r\n)\r\n\r\nif exist \"%~n0.exe\" ( \r\n    \"%~n0.exe\" %* \r\n)\r\n\r\n\r\nendlocal & exit /b %errorlevel%\r\n\r\nend of jscript comment*/\r\n\r\nimport System;\r\nimport System.Windows;\r\nimport System.Windows.Forms;\r\nimport System.Drawing;\r\nimport System.Drawing.SystemIcons;\r\n\r\n\r\nvar arguments:String[] = Environment.GetCommandLineArgs();\r\n\r\n\r\nvar notificationText=\"Warning\";\r\nvar icon=System.Drawing.SystemIcons.Hand;\r\nvar tooltip=null;\r\n//var tooltip=System.Windows.Forms.ToolTipIcon.Info;\r\nvar title=\"\";\r\n//var title=null;\r\nvar timeInMS:Int32=2000;\r\n\r\n\r\n\r\n\r\n\r\nfunction printHelp( ) {\r\n   print( arguments[0] + \" [-tooltip warning|none|warning|info] [-time milliseconds] [-title title] [-text text] [-icon question|hand|exclamation|аsterisk|application|information|shield|question|warning|windlogo]\" );\r\n\r\n}\r\n\r\nfunction setTooltip(t) {\r\n\tswitch(t.toLowerCase()){\r\n\r\n\t\tcase \"error\":\r\n\t\t\ttooltip=System.Windows.Forms.ToolTipIcon.Error;\r\n\t\t\tbreak;\r\n\t\tcase \"none\":\r\n\t\t\ttooltip=System.Windows.Forms.ToolTipIcon.None;\r\n\t\t\tbreak;\r\n\t\tcase \"warning\":\r\n\t\t\ttooltip=System.Windows.Forms.ToolTipIcon.Warning;\r\n\t\t\tbreak;\r\n\t\tcase \"info\":\r\n\t\t\ttooltip=System.Windows.Forms.ToolTipIcon.Info;\r\n\t\t\tbreak;\r\n\t\tdefault:\r\n\t\t\t//tooltip=null;\r\n\t\t\tprint(\"Warning: invalid tooltip value: \"+ t);\r\n\t\t\tbreak;\r\n\t\t\r\n\t}\r\n\t\r\n}\r\n\r\nfunction setIcon(i) {\r\n\tswitch(i.toLowerCase()){\r\n\t\t //Could be Application,Asterisk,Error,Exclamation,Hand,Information,Question,Shield,Warning,WinLogo\r\n\t\tcase \"hand\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Hand;\r\n\t\t\tbreak;\r\n\t\tcase \"application\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Application;\r\n\t\t\tbreak;\r\n\t\tcase \"аsterisk\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Asterisk;\r\n\t\t\tbreak;\r\n\t\tcase \"error\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Error;\r\n\t\t\tbreak;\r\n\t\tcase \"exclamation\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Exclamation;\r\n\t\t\tbreak;\r\n\t\tcase \"hand\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Hand;\r\n\t\t\tbreak;\r\n\t\tcase \"information\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Information;\r\n\t\t\tbreak;\r\n\t\tcase \"question\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Question;\r\n\t\t\tbreak;\r\n\t\tcase \"shield\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Shield;\r\n\t\t\tbreak;\r\n\t\tcase \"warning\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Warning;\r\n\t\t\tbreak;\r\n\t\tcase \"winlogo\":\r\n\t\t\ticon=System.Drawing.SystemIcons.WinLogo;\r\n\t\t\tbreak;\r\n\t\tdefault:\r\n\t\t\tprint(\"Warning: invalid icon value: \"+ i);\r\n\t\t\tbreak;\t\t\r\n\t}\r\n}\r\n\r\n\r\nfunction parseArgs(){\r\n\tif ( arguments.length == 1 || arguments[1].toLowerCase() == \"-help\" || arguments[1].toLowerCase() == \"-help\"   ) {\r\n\t\tprintHelp();\r\n\t\tEnvironment.Exit(0);\r\n\t}\r\n\t\r\n\tif (arguments.length%2 == 0) {\r\n\t\tprint(\"Wrong number of arguments\");\r\n\t\tEnvironment.Exit(1);\r\n\t} \r\n\tfor (var i=1;i<arguments.length-1;i=i+2){\r\n\t\ttry{\r\n\t\t\t//print(arguments[i] +\"::::\" +arguments[i+1]);\r\n\t\t\tswitch(arguments[i].toLowerCase()){\r\n\t\t\t\tcase '-text':\r\n\t\t\t\t\tnotificationText=arguments[i+1];\r\n\t\t\t\t\tbreak;\r\n\t\t\t\tcase '-title':\r\n\t\t\t\t\ttitle=arguments[i+1];\r\n\t\t\t\t\tbreak;\r\n\t\t\t\tcase '-time':\r\n\t\t\t\t\ttimeInMS=parseInt(arguments[i+1]);\r\n\t\t\t\t\tif(isNaN(timeInMS))  timeInMS=2000;\r\n\t\t\t\t\tbreak;\r\n\t\t\t\tcase '-tooltip':\r\n\t\t\t\t\tsetTooltip(arguments[i+1]);\r\n\t\t\t\t\tbreak;\r\n\t\t\t\tcase '-icon':\r\n\t\t\t\t\tsetIcon(arguments[i+1]);\r\n\t\t\t\t\tbreak;\r\n\t\t\t\tdefault:\r\n\t\t\t\t\tConsole.WriteLine(\"Invalid Argument \"+arguments[i]);\r\n\t\t\t\t\tbreak;\r\n\t\t}\r\n\t\t}catch(e){\r\n\t\t\terrorChecker(e);\r\n\t\t}\r\n\t}\r\n}\r\n\r\nfunction errorChecker( e:Error ) {\r\n\tprint ( \"Error Message: \" + e.message );\r\n\tprint ( \"Error Code: \" + ( e.number & 0xFFFF ) );\r\n\tprint ( \"Error Name: \" + e.name );\r\n\tEnvironment.Exit( 666 );\r\n}\r\n\r\nparseArgs();\r\n\r\nvar notification;\r\n\r\nnotification = new System.Windows.Forms.NotifyIcon();\r\n\r\n\r\n\r\n//try {\r\n\tnotification.Icon = icon; \r\n\tnotification.BalloonTipText = notificationText;\r\n\tnotification.Visible = true;\r\n//} catch (err){}\r\n\r\n \r\nnotification.BalloonTipTitle=title;\r\n\r\n\t\r\nif(tooltip!==null) { \r\n\tnotification.BalloonTipIcon=tooltip;\r\n}\r\n\r\n\r\nif(tooltip!==null) {\r\n\tnotification.ShowBalloonTip(timeInMS,title,notificationText,tooltip); \r\n} else {\r\n\tnotification.ShowBalloonTip(timeInMS);\r\n}\r\n\t\r\nvar dieTime:Int32=(timeInMS+100);\r\n\t\r\nSystem.Threading.Thread.Sleep(dieTime);\r\nnotification.Dispose();");
            RunCommandHidden("call \"" + Environment.GetEnvironmentVariable("TEMP") + "\\Caller.bat" +
                             "\"   -tooltip warning -time 3000 -title \"" + title + "\" -text \"" + message +
                             "\" -icon question");
        }

        private bool Exit = false;

        public async Task RunCommandHidden(string Command)
        {
            Random dew = new Random();
            int hui = dew.Next(0000, 9999);
            string[] CommandChut = {Command};
            File.WriteAllLines(
                System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand" + hui + ".bat",
                CommandChut);
            Process C = new Process();
            C.StartInfo.FileName = System.Environment.GetEnvironmentVariable("USERPROFILE") +
                                   "\\Documents\\RunCommand" + hui + ".bat";
            C.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            C.EnableRaisingEvents = true;
            C.Exited += C_Exited;
            C.Start();
            while (Exit == false)
            {
                await Task.Delay(10);
            }

            Exit = false;
            File.Delete(System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand" + hui +
                        ".bat");
        }

        private void C_Exited(object sender, EventArgs e)
        {
            Exit = true;
        }

        private async Task RefreshMainTabs()
        {
            try
            {
                CreateUniqueEncryption.Checked = false;
                OpenUniqueEncryption.Checked = false;
                OpenTwoStepVerificationCheckBox.Checked = false;
                OpenVerifiedEmailAddress = "";
                VerifiedOpen = false;
                Verified = false;
                CreateTwoStepVerificationPanel.Visible = false;
                TwoStepVerificationCreateCheckBox.Checked = false;
                CreateROBLOXModelCheckBox.Checked = false;
                OpenROBLOXModelCheckBox.Checked = false;
                OpenKeyFilesListBox.Items.Clear();
                CreateKeyFilesListBox.Items.Clear();
                OpenKeyFilesCheckbox.Checked = false;
                CreateKeyfilesCheckbox.Checked = false;
                Password.Text = "";
                PasswordConfirm.Text = "";
                PIM.Text = "";
                ExtraSecurityModeCreate.Checked = false;
                UltraSecurityModeCreateCheckBox.Checked = false;
                SecurityPINCreateCheckbox.Checked = false;
                VaultSize.Text = "";
                SaveVaultLocation.Text = "";
                DriveLetter.Text = "";
                SecondDriveLetter.Text = "";
                ThirdDriveLetter.Text = "";
                ///OPEN///
                OpenFirstLetter.Text = "";
                OpenSecondLetter.Text = "";
                ThirdOpenLetter.Text = "";
                ExtraSecurityModeOpen.Checked = false;
                UltraSecurityOpenCheckBox.Checked = false;
                SecurityPINOpenCheckbox.Checked = false;
                CreateNewHashingCheckBox.Checked = false;
                OpenNewHashingCheckBox.Checked = false;
                SecurityPINOpenTextBox.Text = "";
                OpenVaultDirectory.Text = "";
                PasswordOpen.Text = "";
                OpenVaultPIM.Text = "";
            }
            catch
            {

            }
        }

        private const int CP_NOCLOSE_BUTTON = 0x200;

        private async Task SevenZCreatePassword(string Archive, string Files, string Password)
        {
            await RunCommand("cd \"" + MainDirectory + "\\7Zip\" \n" + "7z a -mx1 -p" + Password + " \"" + Archive +
                             "\" \"" + Files + "\"");
        }

        private async Task UnSevenZCreatePassword(string Archive, string Files, string Password)
        {
            await RunCommand("cd \"" + MainDirectory + "\\7Zip\" \n" + "7z e -p" + Password + " \"" + Archive +
                             "\" -o\"" + Files + "\" \n timeout /t 10 /nobreak");
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }

        public async Task Rar(string Archive, string Files, string Password)
        {
            string[] Run =
            {
                "cd \"" + MainDirectory + "\\WinRAR" + "\"",
                "rar a -p" + Password + " \"" + Archive + "\"" + " " + "\"" + Files + "\""
            };
            File.WriteAllLines("C:\\Unrar.bat", Run);
            var dew = Process.Start("C:\\Unrar.bat");
            dew.WaitForExit();
        }

        private async Task RefreshMainDirectory()
        {
            try
            {

                if (Directory.Exists(MainDirectory))
                {
                    DeleteDirectory(MainDirectory);
                    Directory.CreateDirectory(MainDirectory);
                }
                else
                {
                    Directory.CreateDirectory(MainDirectory);
                }

                while (!Directory.Exists(MainDirectory))
                {
                    await Task.Delay(10);
                }

                File.WriteAllBytes(MainDirectory + "\\VeraCrypt.zip", Decryptors.VeraCrypt);
                ZipFile.ExtractToDirectory(MainDirectory + "\\VeraCrypt.zip", MainDirectory);
                File.WriteAllBytes(MainDirectory + "\\WinRAR.zip", Decryptors.WinRAR);
                ZipFile.ExtractToDirectory(MainDirectory + "\\WinRAR.zip", MainDirectory);
            }
            catch
            {
                File.WriteAllBytes(MainDirectory + "\\VeraCrypt.zip", Decryptors.VeraCrypt);
                ZipFile.ExtractToDirectory(MainDirectory + "\\VeraCrypt.zip", MainDirectory);
                File.WriteAllBytes(MainDirectory + "\\WinRAR.zip", Decryptors.WinRAR);
                ZipFile.ExtractToDirectory(MainDirectory + "\\WinRAR.zip", MainDirectory);
            }

        }


        private void button4_Click(object sender, EventArgs e)
        {
            SaveFileDialog d = new SaveFileDialog();
            d.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            d.DefaultExt = "encryptedvault";
            d.ShowDialog();
            SaveVaultLocation.Text = d.FileName;
        }

        private string OpenDirectory = Environment.GetEnvironmentVariable("TEMP") + "\\FileVault\\OpenVault";
        private string ExtraEncryptedOpenPassword = string.Empty;
        private string VaultContainerSize = string.Empty;
        string OpenPassword = String.Empty;

        private async void button6_Click(object sender, EventArgs e)
        {
            await OpenFileVault();
        }

        private async Task OpenFileVault()
        {
            SoundPlayer hashplaPlayer = new SoundPlayer(Resources.Samsung_GALAXY_S4_Alarms___Morning_Flower);
            ///OPEN VAULT BUTTON///
            ///
            if (Convert.ToBoolean(OpenFirstLetter.Text == "") != Convert.ToBoolean(OpenSecondLetter.Text == "") !=
                Convert.ToBoolean(ThirdOpenLetter.Text == ""))
            {
                MessageBox.Show("Drive letter cannot be empty or in use");
                return;
            }

            try
            {
                if (OpenFirstLetter.Text == "")
                {
                    OpenFirstLetter.Text = "I";
                }

                if (OpenSecondLetter.Text == "")
                {
                    OpenSecondLetter.Text = "O";
                }

                if (ThirdOpenLetter.Text == "")
                {
                    ThirdOpenLetter.Text = "P";
                }

                if (!Convert.ToBoolean(OpenVaultDirectory.Text == null) &&
                    !Convert.ToBoolean(OpenVaultPIM.Text == null) && !Convert.ToBoolean(OpenFirstLetter.Text == null) &&
                    !Convert.ToBoolean(OpenSecondLetter.Text == null) && !Convert.ToBoolean(PasswordOpen.Text == null))
                {
                    if (UltraSecurityOpenCheckBox.Checked == false)
                    {
                        FileInfo fileInfo = new FileInfo(OpenVaultDirectory.Text);
                        string FileSize = fileInfo.Length.ToString();
                        try
                        {
                            VaultContainerSize = (Int32.Parse(FileSize) / 1024 / 1024).ToString();
                        }
                        catch
                        {
                            VaultContainerSize = "not";
                        }
                    }

                    PasswordOpen.Text = GetHashString(PasswordOpen.Text);
                    LockComputer();
                    if (OpenUniqueEncryption.Checked == true)
                    {
                        await PlaySoundSync(Resources.UniqueEncryption);
                        StatusLabel.BackColor = Color.Yellow;
                        StatusLabel.Text = "Unique Encryption";
                        string PasswordEncryption = UniqueHashing(PasswordOpen.Text);
                        BackgroundWorker TesterTheTester = new BackgroundWorker();
                        string ThePath = OpenVaultDirectory.Text;
                        TesterTheTester.DoWork += (sender, args) =>
                        {
                            try
                            {
                                Encryption.FileDecrypt(ThePath,
                                                       Environment.GetEnvironmentVariable("TEMP") + "\\TempJer", PasswordEncryption);
                            }
                            catch (Exception ex)
                            {
                                UnlockComputer();
                                Console.WriteLine(ex);
                                throw;
                            }
                        };
                        try
                        {
                            FinalFlash.PlayLooping();
                        }
                        catch (Exception e)
                        {
                            
                        }
                        TesterTheTester.RunWorkerAsync();
                        while (TesterTheTester.IsBusy)
                        {
                            await Task.Delay(10);
                        }
                        File.Delete(OpenVaultDirectory.Text);
                        OpenVaultDirectory.Text = OpenVaultDirectory.Text.Replace(".aes", ".encryptedvaultextra");
                        File.Move(Environment.GetEnvironmentVariable("TEMP") + "\\TempJer",OpenVaultDirectory.Text);
                        try
                        {
                            FinalFlash.Stop();
                        }
                        catch (Exception e)
                        {
                            
                        }
                    }
                    if (OpenNewHashingCheckBox.Checked == true)
                    {
                        LockComputer();
                        StatusLabel.BackColor = Color.Yellow;
                        StatusLabel.Text = "Encryption/Hashing Password";
                        await ShowNotification("Notice",
                            "Hashing/Encrypting Password, this might take a while depending on your computer speed");
                        SoundPlayer en = new SoundPlayer(Resources.Hashing);
                        en.PlaySync();
                        hashplaPlayer.PlayLooping();
                        PasswordOpen.Text = UniqueHashing(PasswordOpen.Text);
                        PasswordOpen.Text = UltraHash(PasswordOpen.Text);
                        PasswordOpen.Text = GetHashString(PasswordOpen.Text);
                        hashplaPlayer.Stop();
                    }

                    if (OpenKeyFilesCheckbox.Checked == true)
                    {
                        hashplaPlayer.PlayLooping();
                        LockComputer();
                        File.WriteAllBytes(MainDirectory + "\\KeyFileSalt.salt", Resources.KeySaltFile);
                        OpenKeyFilesListBox.Items.Add(MainDirectory + "\\KeyFileSalt.salt");
                        StatusLabel.BackColor = Color.Yellow;
                        StatusLabel.Text = "Adding Keyfiles";
                        string[] ListItems = OpenKeyFilesListBox.Items.OfType<string>().ToArray();
                        File.WriteAllLines(MainDirectory + "\\KeyFiles.txt", ListItems);
                        var i = 0;
                        if (!Directory.Exists(MainDirectory + "\\KeyFilesFolder"))
                        {
                            Directory.CreateDirectory(MainDirectory + "\\KeyFilesFolder");
                        }

                        foreach (var readLine in File.ReadLines(MainDirectory + "\\KeyFiles.txt"))
                        {
                            await RunCommand("certutil -encode \"" + readLine + "\" \"" + MainDirectory +
                                             "\\KeyFilesFolder\\" + "KeyFile" + i + ".jer");
                            i++;
                        }

                        DirectoryInfo hui = new DirectoryInfo(MainDirectory + "\\KeyFilesFolder");
                        foreach (var fileInfo in hui.GetFiles())
                        {
                            File.AppendAllText(MainDirectory + "\\KeyFilesHash.txt",
                                File.ReadAllText(MainDirectory + "\\KeyFilesFolder\\" + fileInfo.Name));
                        }

                        PasswordOpen.Text = PasswordOpen.Text +
                                            UltraHash(File.ReadAllText(MainDirectory + "\\KeyFilesHash.txt"));
                        PasswordOpen.Text = GetHashString(PasswordOpen.Text);
                        hashplaPlayer.Stop();
                    }

                    OpenPassword = PasswordOpen.Text;
                    LockComputer();
                    StatusLabel.BackColor = Color.Yellow;
                    StatusLabel.Text = "Opening Vault - Preparing Decryptor";
                    SoundPlayer openPlayer = new SoundPlayer(Resources.Opening);
                    await Task.Factory.StartNew(() => { openPlayer.PlaySync(); });

                    await Task.Delay(3000);

                    if (OpenVaultPIM.Text == "")
                    {
                        OpenVaultPIM.Text = "500";
                    }

                    if (!Directory.Exists(OpenDirectory))
                    {
                        Directory.CreateDirectory(OpenDirectory);
                    }

                    File.WriteAllText(OpenDirectory + "\\PlainPassword.txt", PasswordOpen.Text);
                    await RunCommand("certutil -encode \"" + OpenDirectory + "\\PlainPassword.txt\" \"" +
                                     OpenDirectory +
                                     "\\EncryptedPassword.txt\"");
                    string EncryptedPassword =
                        File.ReadLines(OpenDirectory + "\\EncryptedPassword.txt").ElementAtOrDefault(1) +
                        File.ReadLines(OpenDirectory + "\\EncryptedPassword.txt").ElementAtOrDefault(2) +
                        File.ReadLines(OpenDirectory + "\\EncryptedPassword.txt").ElementAtOrDefault(3);
                    EncryptedPasswordtext = EncryptedPassword;
                    if (ExtraSecurityModeOpen.Checked == true)
                    {
                        await ShowNotification("Extra security mode", "Extra security mode has been activated");
                        Opacity = 0.01;
                        StatusLabel.Text = "";
                        if (!Directory.Exists(OpenDirectory + "\\ExtraExtract"))
                        {
                            Directory.CreateDirectory(OpenDirectory + "\\ExtraExtract");
                        }

                        SoundPlayer soundPlayer = new SoundPlayer(Resources.ExtraSecurityMode);
                        soundPlayer.Play();
                        File.Copy(OpenVaultDirectory.Text, OpenDirectory + "\\OpeningVaultExtra.rar");
                        File.WriteAllText(MainDirectory + "\\ExtraU.txt", EncryptedPassword);
                        await RunCommand("certutil -encode \"" + MainDirectory + "\\ExtraU.txt\" \"" + MainDirectory +
                                         "\\ExtraE.txt\"");
                        ExtraEncryptedOpenPassword =
                            File.ReadLines(MainDirectory + "\\ExtraE.txt").ElementAtOrDefault(1);
                        if (!Directory.Exists(MainDirectory + "\\7Zip"))
                        {
                            try
                            {

                                File.WriteAllBytes(MainDirectory + "\\7Zip.zip", Resources._7Zip);
                                ZipFile.ExtractToDirectory(MainDirectory + "\\7Zip.zip", MainDirectory);

                            }
                            catch
                            {
                                DeleteDirectory(MainDirectory);
                                MessageBox.Show("An error occured");
                            }
                        }

                        SoundPlayer RainSound = new SoundPlayer(Resources.thunder);
                        RainSound.PlayLooping();
                        await UnSevenZCreatePassword(OpenDirectory + "\\OpeningVaultExtra.rar",
                            OpenDirectory + "\\ExtraExtract", ExtraEncryptedOpenPassword);
                        RainSound.Stop();
                        await Task.Delay(1000);
                        DirectoryInfo dd = new DirectoryInfo(OpenDirectory + "\\ExtraExtract");
                        //if (OpenTwoStepVerificationCheckBox.Checked == true)
                        //{

                        //        StatusLabel.Text = "Verifying two step verification";
                        //        SoundPlayer morningflower =
                        //            new SoundPlayer(Resources.Samsung_GALAXY_S4_Alarms___Morning_Flower);
                        //        morningflower.PlayLooping();

                        //            EncryptedPassword =
                        //                GetHashString(EncryptedPassword +
                        //                              File.ReadAllText(
                        //                                  UltraHash(OpenEmailAddressTextBox.Text)));



                        //        morningflower.Stop();

                        //}
                        foreach (var file in dd.GetFiles())
                        {
                            Unrar(OpenDirectory + "\\ExtraExtract\\" + file.Name, OpenDirectory, EncryptedPassword);
                        }
                    }

                    if (ExtraSecurityModeOpen.Checked == false && UltraSecurityOpenCheckBox.Checked == true)
                    {
                        Unrar(OpenVaultDirectory.Text, OpenDirectory, EncryptedPassword);
                    }

                    if (UltraSecurityOpenCheckBox.Checked == true)
                    {
                        await ShowNotification("Ultra security mode", "Ultra security mode has been activated");
                        SoundPlayer sss = new SoundPlayer(Resources.UltraSecurityMode);
                        sss.Play();
                        DirectoryInfo dd = new DirectoryInfo(OpenDirectory + "\\Users");

                        foreach (var directoryInfo in dd.GetDirectories())
                        {
                            File.Move(OpenDirectory + "\\Users\\" + directoryInfo +
                                      "\\AppData\\Local\\Temp\\FileVault\\SecurityVault.jer",
                                OpenDirectory + "\\SecurityVault.jer");
                        }



                        Opacity = 100;
                        BackColor = Color.Red;
                        StatusLabel.Text = "Decrypting First Layer";
                        if (SecurityPINOpenCheckbox.Checked == true)
                        {
                            await RunCommand("cd \"%temp%\\FileVault\\VeraCrypt\" \n veracrypt /q /v " + OpenDirectory +
                                             "\\SecurityVault.jer" + " /l " + ThirdOpenLetter.Text + " /a /pim " +
                                             "600" + " /p " + PasswordOpen.Text + SecurityPINOpenTextBox.Text +
                                             " /e /b");
                        }
                        else
                        {
                            await RunCommand("cd \"%temp%\\FileVault\\VeraCrypt\" \n veracrypt /q /v " + OpenDirectory +
                                             "\\SecurityVault.jer" + " /l " + ThirdOpenLetter.Text + " /a /pim " +
                                             "600" + " /p " + PasswordOpen.Text + " /e /b");
                        }

                        while (!Directory.Exists(ThirdOpenLetter.Text + ":\\"))
                        {
                            await Task.Delay(10);
                        }

                        BackColor = Color.Yellow;
                        StatusLabel.BackColor = Color.Purple;
                        StatusLabel.Text = "Closing First Layer Decryptor";
                        VaultContainerSize = File.ReadLines(ThirdOpenLetter.Text + ":\\FileSize.txt")
                            .ElementAtOrDefault(0);
                        if (File.Exists(ThirdOpenLetter.Text + ":\\" + GetHashString("Verification") + ".txt"))
                        {
                            await PlaySoundSync(Resources.ThreeStepActivated);
                            LockMouse = false;
                            PlaySound(Resources.Enter3Step);
                            File.WriteAllText(MainDirectory + "\\Code.txt", File.ReadAllText(ThirdOpenLetter.Text + ":\\" + GetHashString("Verification") + ".txt"));
                            OpenThreeStepVerification s = new OpenThreeStepVerification();
                            s.ShowDialog();
                            LockMouse = true;
                            if (!File.Exists(MainDirectory + "\\Correct.txt"))
                            {
                                LockMouse = false;
                                UnlockComputer();
                                File.WriteAllText("JAJAJAJAJAJ:::::DSJDSJDWRONGNCODE","d");
                            }

                            await PlaySoundSync(Resources.Enter3StepSuccess);
                        }

                        SoundPlayer morningflower =
                            new SoundPlayer(Resources.Samsung_GALAXY_S4_Alarms___Morning_Flower);
                        SoundPlayer Defaultsound = new SoundPlayer(Resources.default_sound);
                        if (OpenTwoStepVerificationCheckBox.Checked == true)
                        {
                            await PlaySoundSync(Resources.TwoStepActivated);
                            string ThirdVeraCryptPassword = string.Empty;
                            try
                            {
                                StatusLabel.Text = "Verifying 2 Step Verification";
                                //PlayLoopingFromDirectory("GentleSpringRain");
                                GentleSpringRain.PlayLooping();
                                string EncryptedEmail = UltraHash(OpenVerifiedEmailAddress);
                                if (EncryptedEmail ==
                                    File.ReadAllText(ThirdOpenLetter.Text + ":\\" + GetHashString("Email") + ".txt"))
                                {
                                    ThirdVeraCryptPassword = GetHashString(EncryptedEmail);
                                }
                                else
                                {
                                    File.Delete("FHEISFJIESFOASD");
                                }
                                GentleSpringRain.Stop();
                            }
                            catch (Exception huihui)
                            {
                                UnlockComputer();
                                Console.WriteLine(huihui);
                                throw;
                            }
                            SoundPlayer walkInForest = new SoundPlayer(Resources.Samsung_GALAXY_S4_Alarms___Walk_in_the_forest);

                            if (File.Exists(MainDirectory + "\\Code.txt"))
                            {
                                StatusLabel.Text = "Verifying 3 Step Verification";
                                walkInForest.PlayLooping();
                                ThirdVeraCryptPassword = ThirdVeraCryptPassword + GetHashString(File.ReadAllText(MainDirectory + "\\Code.txt"));
                                walkInForest.Stop();
                            }
                            walkInForest.PlayLooping();
                            ThirdVeraCryptPassword = UltraHash(ThirdVeraCryptPassword);
                            walkInForest.Stop();






                            Defaultsound.PlayLooping();
                            File.Move(ThirdOpenLetter.Text + ":\\TREASURE.txt", MainDirectory + "\\ThirdVeracrypt.jer");


                            await Task.Delay(1000);
                            await MountVeracrypt(MainDirectory + "\\ThirdVeracrypt.jer", ThirdVeraCryptPassword, "500",
                                OpenSecondLetter.Text);
                            File.Move(OpenSecondLetter.Text + ":\\TREASURE.txt", OpenDirectory + "\\UnrarJer.rar");
                            await DismountVeracrypt(SecondDriveLetter.Text);
                            Defaultsound.Stop();
                        }
                        DirectoryInfo sdDirectoryInfos = new DirectoryInfo(ThirdOpenLetter.Text + ":\\");
                        if (OpenTwoStepVerificationCheckBox.Checked == false)
                        {
                            foreach (var fileInfo in sdDirectoryInfos.GetFiles())
                            {
                                if (fileInfo.ToString() == "TREASURE.txt")
                                {
                                    File.Move(ThirdOpenLetter.Text + ":\\" + fileInfo,
                                        OpenDirectory + "\\UnrarJer.rar");
                                }
                            }
                        }

                        await RunCommand("cd \"" + MainDirectory + "\\VeraCrypt\" \n veracrypt /q /d " +
                                         ThirdOpenLetter.Text + " /force /silent");
                        while (Directory.Exists(ThirdOpenLetter.Text + ":\\"))
                        {
                            await Task.Delay(10);
                        }

                        Unrar(OpenDirectory + "\\UnrarJer.rar", OpenDirectory, EncryptedPassword);
                        BackColor = Color.Green;
                        StatusLabel.BackColor = Color.Yellow;
                        StatusLabel.Text = "Decryptor Closed";
                    }
                    else
                    {
                        File.Copy(OpenVaultDirectory.Text, OpenDirectory + "\\OpeningVault.rar");
                        Unrar(OpenDirectory + "\\OpeningVault.rar", OpenDirectory, EncryptedPassword);
                    }

                    if (!Directory.Exists(OpenDirectory + "\\Users"))
                    {
                        DeleteDirectory(MainDirectory);
                        await RefreshMainDirectory();
                        UnlockComputer();
                        SystemSounds.Beep.Play();
                        WrongPassword dew = new WrongPassword();
                        dew.ShowDialog();
                        DeleteDirectory(MainDirectory);
                        Application.Exit();
                    }

                    BackColor = Color.Black;
                    StatusLabel.Text = "Decrypting Files - Almost Ready";
                    StatusLabel.BackColor = Color.Yellow;
                    Opacity = 100;
                    DirectoryInfo d = new DirectoryInfo(OpenDirectory + "\\Users");
                    SoundPlayer RainSoundd = new SoundPlayer(Resources.rain);
                    RainSoundd.PlayLooping();
                    foreach (var directory in d.GetDirectories())
                    {
                        File.Move(
                            OpenDirectory + "\\Users\\" + directory +
                            "\\AppData\\Local\\Temp\\FileVault\\RarDirectory\\VDisk.vhd",
                            OpenDirectory + "\\VDisk.vhd");
                    }

                    string[] MountDiskScript =
                    {
                        "select vdisk file=" + OpenDirectory + "\\VDisk.vhd", "attach vdisk", "select partition 1",
                        "assign letter=" + OpenFirstLetter.Text
                    };
                    File.WriteAllLines(OpenDirectory + "\\MountScript.txt", MountDiskScript);
                    string[] MountDisk = { "diskpart /s \"" + OpenDirectory + "\\MountScript.txt\"" };
                    File.WriteAllLines(OpenDirectory + "\\MountDisk.bat", MountDisk);
                    Process.Start(OpenDirectory + "\\MountDisk.bat").WaitForExit();
                    while (!Directory.Exists(OpenFirstLetter.Text + ":\\"))
                    {
                        await Task.Delay(10);
                    }

                    RainSoundd.Stop();
                    SoundPlayer RainSounddew = new SoundPlayer(Resources.default_sound);
                    RainSounddew.PlayLooping();
                    await RunCommand("cd \"%temp%\\FileVault\\VeraCrypt\" \n veracrypt /q /v " + OpenFirstLetter.Text +
                                     ":\\Vault.jer" + " /l " + OpenSecondLetter.Text + " /a /pim " + OpenVaultPIM.Text +
                                     " /p " + PasswordOpen.Text + " /e /b");
                    StatusLabel.Text = "Opening Vault...";
                    while (!Directory.Exists(OpenSecondLetter.Text + ":\\"))
                    {
                        await Task.Delay(10);
                    }

                    ManageVaultPanel.Visible = true;
                    OpenSecuritySettingsPanel.Visible = false;
                    CreateVaultPanel.Visible = false;
                    linkLabel1.Enabled = false;
                    await ShowNotification("Success",
                        "File vault successfully opened, your vault path is: " + OpenSecondLetter.Text + ":\\");




                    RainSounddew.Stop();

                    SoundPlayer ss = new SoundPlayer(Resources.SuccessfullyOpened);
                    ss.Play();
                    await RunCommand("taskkill /f /im explorer.exe \n start %windir%\\explorer.exe \n exit");
                    while (!Directory.Exists(OpenSecondLetter.Text + ":\\"))
                    {
                        await Task.Delay(10);
                    }

                    if (File.Exists(Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\CreateTime.txt") &&
                        !Convert.ToBoolean(VaultContainerSize == "not"))
                    {
                        try
                        {
                            string t = File
                                .ReadLines(Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\CreateTime.txt")
                                .ElementAtOrDefault(0);
                            var TimePer10MB = t.Split(':')[2].Trim().Split('.')[0].Trim();
                            var EncryptionTimeDew = (Convert.ToInt32(VaultContainerSize) / 10);
                            Console.WriteLine(EncryptionTimeDew);
                            var EncryptionTime = (Convert.ToInt32(TimePer10MB) * EncryptionTimeDew);
                            var OldEncryptionTime = EncryptionTime;
                            var VeraCryptTime = 0;
                            if (UltraSecurityOpenCheckBox.Checked == true)
                            {
                                EncryptionTime = (OldEncryptionTime + EncryptionTime);
                                if (File.Exists(Environment.GetEnvironmentVariable("LOCALAPPDATA") +
                                                "\\VeracryptTime.txt"))
                                {
                                    string dd = File
                                        .ReadLines(Environment.GetEnvironmentVariable("LOCALAPPDATA") +
                                                   "\\VeracryptTime.txt").ElementAtOrDefault(0);
                                    var TimeVeracrypt = dd.Split(':')[2].Trim().Split('.')[0].Trim();
                                    VeraCryptTime = Int32.Parse(TimeVeracrypt);
                                }

                                EncryptionTime = (EncryptionTime + VeraCryptTime);
                            }

                            if (ExtraSecurityModeOpen.Checked == true)
                            {
                                EncryptionTime = ((OldEncryptionTime / 2) + EncryptionTime);
                            }

                            if (EncryptionTime > 59)
                            {
                                double dew = Double.Parse(EncryptionTime.ToString()) / 60d;
                                string Minutes = dew.ToString();
                                string RoundUp = Minutes.Split('.')[1].Trim();
                                int Minute = 0;
                                CloseAndLockVaultTimeLabel.BackColor = Color.Yellow;
                                CloseAndLockVaultTimeLabel.Text =
                                    "Encryption time:\n" + dew + " minutes\n(Estimated Time)\nNot 100% Accurate";
                            }
                            else
                            {
                                CloseAndLockVaultTimeLabel.BackColor = Color.Yellow;
                                CloseAndLockVaultTimeLabel.Text =
                                    "Encryption time:\n" + EncryptionTime +
                                    " seconds\n(Estimated Time)\nNot 100% Accurate";
                            }
                        }
                        catch (Exception er)
                        {
                            Console.WriteLine(er);
                        }
                    }
                    else if (VaultContainerSize == "not")
                    {
                        CloseAndLockVaultTimeLabel.Text = "Could not estimate\nvault closing time";
                    }

                    StatusLabel.Text = "";
                    Process.Start(OpenSecondLetter.Text + ":\\");
                    UnlockComputer();
                    MainTabs.Visible = false;
                    linkLabel1.Visible = false;
                }
            }
            catch (Exception dew)
            {
                await ShowNotification("Failure", "Wrong password or security settings");
                Console.WriteLine(dew);
                UnlockComputer();
                WrongPassword dewd = new WrongPassword();
                dewd.ShowDialog();
                if (Convert.ToBoolean(Environment.UserName == "uh") !=
                    Convert.ToBoolean(Environment.UserName == "335384137"))
                {
                    throw;
                }

                Process.Start(Application.ExecutablePath);
                Application.Exit();
            }
        }

        public void Unrar(string Archive, string Output, string Password)
        {
            string[] Run =
            {
                "cd \"C:\\Program Files\\WinRAR\"",
                "unrar x -p" + Password + " \"" + Archive + "\"" + " " + "\"" + Output + "\""
            };
            File.WriteAllLines("C:\\Unrar.bat", Run);
            var dew = Process.Start("C:\\Unrar.bat");
            dew.WaitForExit();
        }

        private async void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            await DismountVeracrypt("Z");
            DeleteDirectory(MainDirectory);
            Application.Exit();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            OpenFileDialog d = new OpenFileDialog();
            d.ShowDialog();
            if (File.Exists(d.FileName))
            {
                OpenVaultDirectory.Text = d.FileName;
            }
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked == true)
            {
                PasswordOpen.UseSystemPasswordChar = false;
            }
            else
            {
                PasswordOpen.UseSystemPasswordChar = true;
            }
        }

        private void UltraSecurityModeCreateCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (UltraSecurityModeCreateCheckBox.Checked == true)
            {

                TwoStepVerificationCreateCheckBox.Enabled = true;
                SecurityPINCreateCheckbox.Enabled = true;
                SoundPlayer s = new SoundPlayer(Resources.AntiFBIMode);
                s.Play();
            }
            else
            {
                TwoStepVerificationCreateCheckBox.Enabled = false;
                SecurityPINCreateCheckbox.Enabled = false;
            }
        }

        private void UltraSecurityOpenCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (UltraSecurityOpenCheckBox.Checked == true)
            {
                SecurityPINOpenCheckbox.Enabled = true;
                OpenTwoStepVerificationCheckBox.Enabled = true;
                SoundPlayer s = new SoundPlayer(Resources.AntiFBIMode);
                s.Play();
            }

            if (UltraSecurityOpenCheckBox.Checked == false)
            {
                OpenTwoStepVerificationCheckBox.Enabled = false;
                SecurityPINOpenCheckbox.Enabled = false;
            }
        }

        private void SecurityPINOpenCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            if (SecurityPINOpenCheckbox.Checked == true)
            {
                SecurityPINOpenTextBox.Enabled = true;
            }
            else
            {
                SecurityPINOpenTextBox.Enabled = false;
            }
        }

        private async void DriveLetter_TextChanged(object sender, EventArgs e)
        {
            bool Exists = false;
            foreach (var item in DriveInfo.GetDrives())
            {
                Console.WriteLine(item.ToString());
                if (DriveLetter.Text + ":\\" == item.ToString())
                {
                    Exists = true;
                }
            }

            if (Directory.Exists(DriveLetter.Text + ":\\") != Exists == true)
            {
                if (Exists == true)
                {
                    DriveLetter.Text = "";
                }

                Exists = false;
                LockComputer();
                await DismountVeracrypt(DriveLetter.Text);
                await EjectDrive();
                UnlockComputer();
                if (Directory.Exists(DriveLetter.Text + ":\\"))
                {
                    DriveLetter.Text = "";
                    Text = "Drive Letter In Use";
                    await Task.Delay(3000);
                    Text = "File Vault";
                }
            }
        }

        private async void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            LockComputer();
            if (!Convert.ToBoolean(DriveLetter.Text == ""))
            {
                await DismountVeracrypt(DriveLetter.Text);
            }

            if (!Convert.ToBoolean(SecondDriveLetter.Text == ""))
            {
                await DismountVeracrypt(SecondDriveLetter.Text);
            }

            if (!Convert.ToBoolean(ThirdDriveLetter.Text == ""))
            {
                await DismountVeracrypt(ThirdDriveLetter.Text);
            }

            await EjectDrive();
            UnlockComputer();
        }

        private async Task EjectDrive()
        {
            string[] Eject = {"select vdisk file=" + MainDirectory + "\\VDisk.vhd", "detach vdisk"};
            string[] EjectOpen = {"select vdisk file=" + OpenDirectory + "\\VDisk.vhd", "detach vdisk"};
            File.WriteAllLines(MainDirectory + "\\Eject.txt", Eject);
            File.WriteAllLines(MainDirectory + "\\EjectOpen.txt", EjectOpen);
            string[] RunScript =
            {
                "diskpart /s \"" + MainDirectory + "\\Eject.txt\"",
                "diskpart /s \"" + MainDirectory + "\\EjectOpen.txt\""
            };
            File.WriteAllLines(MainDirectory + "\\RunScript.bat", RunScript);
            Process.Start(MainDirectory + "\\RunScript.bat").WaitForExit();
        }

        private async void SecondDriveLetter_TextChanged(object sender, EventArgs e)
        {
            bool Exists = false;
            foreach (var item in DriveInfo.GetDrives())
            {
                if (SecondDriveLetter.Text + ":\\" == item.ToString())
                {
                    Exists = true;
                }
            }

            if (Directory.Exists(SecondDriveLetter.Text + ":\\") != Exists == true)
            {
                if (Exists == true)
                {
                    SecondDriveLetter.Text = "";
                }

                Exists = false;
                LockComputer();
                await DismountVeracrypt(SecondDriveLetter.Text);
                await EjectDrive();
                UnlockComputer();
                if (Directory.Exists(SecondDriveLetter.Text + ":\\"))
                {
                    SecondDriveLetter.Text = "";
                    Text = "Drive Letter In Use";
                    await Task.Delay(3000);
                    Text = "File Vault";
                }
            }
        }

        private async void ThirdDriveLetter_TextChanged(object sender, EventArgs e)
        {
            bool Exists = false;
            foreach (var item in DriveInfo.GetDrives())
            {
                if (ThirdDriveLetter.Text + ":\\" == item.ToString())
                {
                    Exists = true;
                }
            }

            if (Directory.Exists(ThirdDriveLetter.Text + ":\\") != Exists == true)
            {
                if (Exists == true)
                {
                    ThirdDriveLetter.Text = "";
                }

                Exists = false;
                LockComputer();
                await DismountVeracrypt(ThirdDriveLetter.Text);
                await EjectDrive();
                UnlockComputer();

                if (Directory.Exists(ThirdDriveLetter.Text + ":\\"))
                {
                    ThirdDriveLetter.Text = "";
                    Text = "Drive Letter In Use";
                    await Task.Delay(3000);
                    Text = "File Vault";
                }
            }
        }

        private async void OpenFirstLetter_TextChanged(object sender, EventArgs e)
        {
            bool Exists = false;
            foreach (var item in DriveInfo.GetDrives())
            {
                if (OpenFirstLetter.Text + ":\\" == item.ToString())
                {
                    Exists = true;
                }
            }

            if (Directory.Exists(OpenFirstLetter.Text + ":\\") != Exists == true)
            {
                await ShowNotification("Drive Letter In Use", "Letter \"" + OpenFirstLetter.Text + "\" Already in use");
                if (Exists == true)
                {
                    OpenFirstLetter.Text = "";
                    Exists = false;
                }

                LockComputer();
                await DismountVeracrypt(OpenFirstLetter.Text);
                await EjectDrive();
                UnlockComputer();
                if (Directory.Exists(OpenFirstLetter.Text + ":\\"))
                {
                    OpenFirstLetter.Text = "";
                    Text = "Drive Letter In Use";
                    await Task.Delay(3000);
                    Text = "File Vault";
                }
            }
        }

        private async void OpenSecondLetter_TextChanged(object sender, EventArgs e)
        {
            bool Exists = false;
            foreach (var item in DriveInfo.GetDrives())
            {
                if (OpenSecondLetter.Text + ":\\" == item.ToString())
                {
                    Exists = true;
                }
            }

            if (Directory.Exists(OpenSecondLetter.Text + ":\\"))
            {
                await ShowNotification("Drive Letter In Use",
                    "Letter \"" + OpenSecondLetter.Text + "\" Already in use");
                if (Exists == true)
                {
                    OpenSecondLetter.Text = "";
                    Exists = false;
                }

                LockComputer();
                await DismountVeracrypt(OpenSecondLetter.Text);
                await EjectDrive();
                UnlockComputer();
                if (Directory.Exists(OpenSecondLetter.Text + ":\\"))
                {
                    OpenSecondLetter.Text = "";
                    Text = "Drive Letter In Use";
                    await Task.Delay(3000);
                    Text = "File Vault";
                }
            }
        }

        private async void ThirdOpenLetter_TextChanged(object sender, EventArgs e)
        {
            bool Exists = false;
            foreach (var item in DriveInfo.GetDrives())
            {
                if (ThirdOpenLetter.Text + ":\\" == item.ToString())
                {
                    Exists = true;
                }
            }

            if (Directory.Exists(ThirdOpenLetter.Text + ":\\"))
            {
                await ShowNotification("Drive Letter In Use", "Letter \"" + ThirdOpenLetter.Text + "\" Already in use");
                if (Exists == true)
                {
                    ThirdOpenLetter.Text = "";
                    Exists = false;
                }

                LockComputer();
                await DismountVeracrypt(ThirdOpenLetter.Text);
                await EjectDrive();
                UnlockComputer();
                if (Directory.Exists(ThirdOpenLetter.Text + ":\\"))
                {
                    ThirdOpenLetter.Text = "";
                    Text = "Drive Letter In Use";
                    await Task.Delay(3000);
                    Text = "File Vault";
                }
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (Password.Text == PasswordConfirm.Text)
            {
                CreatePasswordPanel.Visible = false;
                CreatePIMPanel.Visible = true;
            }
            else
            {
                MessageBox.Show("Passwords Must Match");
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            CreateVaultLocationPanel.Visible = false;
            CreatePasswordPanel.Visible = true;
        }

        private void button10_Click(object sender, EventArgs e)
        {
            CreateVaultLocationPanel.Visible = true;
            CreatePasswordPanel.Visible = false;
        }

        private void button14_Click(object sender, EventArgs e)
        {
            CreatePIMPanel.Visible = false;
            CreateVaultSize.Visible = true;
            try
            {
                if (Int32.Parse(PIM.Text) < 500)
                {
                    PIM.Text = "";
                    MessageBox.Show("PIM cannot be lower than 500, PIM now set to 500");
                }
                else
                {

                }
            }
            catch
            {
                PIM.Text = "500";
            }

            if (PIM.Text == "")
            {
                PIM.Text = "500";
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            CreateVaultSize.Visible = false;
            CreateDriveLetterPanel.Visible = true;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            CreatePIMPanel.Visible = true;
            CreateVaultSize.Visible = false;
        }

        private void button13_Click(object sender, EventArgs e)
        {
            CreatePIMPanel.Visible = false;
            CreatePasswordPanel.Visible = true;
        }

        private void button16_Click(object sender, EventArgs e)
        {
            if (DriveLetter.Text == "")
            {
                DriveLetter.Text = GetRandomDriveLetter1();
            }

            if (SecondDriveLetter.Text == "")
            {
                SecondDriveLetter.Text = GetRandomDriveLetter2();
            }

            if (ThirdDriveLetter.Text == "")
            {
                ThirdDriveLetter.Text = GetRandomDriveLetter3();
            }

            CreateDriveLetterPanel.Visible = false;
            CreateSecuritySettings.Visible = true;
        }

        private void button15_Click(object sender, EventArgs e)
        {
            CreateDriveLetterPanel.Visible = false;
            CreateVaultSize.Visible = true;
        }

        private void button17_Click(object sender, EventArgs e)
        {
            button1.Enabled = true;
            CreateSecuritySettings.Visible = false;
            CreationPanel.Visible = true;
            if (File.Exists(Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\CreateTime.txt") &&
                !Convert.ToBoolean(VaultSize.Text == ""))
            {
                try
                {
                    string t = File.ReadLines(Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\CreateTime.txt")
                        .ElementAtOrDefault(0);
                    var TimePer10MB = t.Split(':')[2].Trim().Split('.')[0].Trim();
                    var EncryptionTimeDew = (Convert.ToInt32(VaultSize.Text) / 10);
                    Console.WriteLine(EncryptionTimeDew);
                    var EncryptionTime = (Convert.ToInt32(TimePer10MB) * EncryptionTimeDew);
                    var OldEncryptionTime = EncryptionTime;
                    var VeraCryptTime = 0;
                    if (UltraSecurityModeCreateCheckBox.Checked == true)
                    {
                        EncryptionTime = (OldEncryptionTime + EncryptionTime);
                        if (File.Exists(Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\VeracryptTime.txt"))
                        {
                            string d = File
                                .ReadLines(Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\VeracryptTime.txt")
                                .ElementAtOrDefault(0);
                            var TimeVeracrypt = d.Split(':')[2].Trim().Split('.')[0].Trim();
                            VeraCryptTime = Int32.Parse(TimeVeracrypt);
                        }

                        EncryptionTime = (EncryptionTime + VeraCryptTime);
                    }

                    if (ExtraSecurityModeCreate.Checked == true)
                    {
                        EncryptionTime = ((OldEncryptionTime / 2) + EncryptionTime);
                    }

                    if (EncryptionTime > 59)
                    {
                        double dew = Double.Parse(EncryptionTime.ToString()) / 60d;
                        string Minutes = dew.ToString();
                        string RoundUp = Minutes.Split('.')[1].Trim();
                        int Minute = 0;

                        FinalEncryptionTime.Text =
                            "Encryption time:\n" + dew + " minutes\n(Estimated Time)\nNot 100% Accurate";
                    }
                    else
                    {
                        FinalEncryptionTime.BackColor = Color.Yellow;
                        FinalEncryptionTime.Text = "Encryption time:\n" + EncryptionTime +
                                                   " seconds\n(Estimated Time)\nNot 100% Accurate";
                    }
                }
                catch (Exception er)
                {
                    Console.WriteLine(er);
                }
            }
        }

        private void button18_Click(object sender, EventArgs e)
        {
            CreateSecuritySettings.Visible = false;
            CreateDriveLetterPanel.Visible = true;
        }

        private void button19_Click(object sender, EventArgs e)
        {
            CreationPanel.Visible = false;
            CreateSecuritySettings.Visible = true;
        }

        private void DragAndDropCreate_Paint(object sender, PaintEventArgs e)
        {

        }

        private void DragAndDropCreate_DragDrop(object sender, DragEventArgs e)
        {
            MessageBox.Show(e.Data.ToString());
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {

        }

        private void PIM_TextChanged(object sender, EventArgs e)
        {
            try
            {

            }
            catch
            {

            }
        }

        private void ExtraSecurityModeOpen_CheckedChanged(object sender, EventArgs e)
        {
            if (ExtraSecurityModeOpen.Checked == true)
            {
                SoundPlayer s = new SoundPlayer(Resources.ExtraSecurityModeOn);
                s.Play();
            }
        }

        private void ExtraSecurityModeCreate_CheckedChanged(object sender, EventArgs e)
        {
            if (ExtraSecurityModeCreate.Checked == true)
            {
                SoundPlayer s = new SoundPlayer(Resources.ExtraSecurityModeOn);
                s.Play();
            }
        }

        private void button20_Click(object sender, EventArgs e)
        {
            VaultSize.Text = "10";
        }

        private void button21_Click(object sender, EventArgs e)
        {
            VaultSize.Text = "100";
        }

        private void button22_Click(object sender, EventArgs e)
        {
            VaultSize.Text = "500";
        }

        private void CreateVaultLocationPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button23_Click(object sender, EventArgs e)
        {
            Process.Start("https://www.lastpass.com");
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button24_Click(object sender, EventArgs e)
        {
            SaveFileDialog dew = new SaveFileDialog();
            dew.ShowDialog();
            try
            {
                File.WriteAllText(dew.FileName, GeneratedPasswordTextBox.Text);
            }
            catch
            {

            }
        }

        private void OpenVaultDirectory_TextChanged(object sender, EventArgs e)
        {

        }

        private bool HideWindow = false;

        private void button25_Click(object sender, EventArgs e)
        {
            if (HideWindow == false)
            {
                Opacity = 0.25;
                HideWindow = true;
            }
            else
            {
                HideWindow = false;
                Opacity = 100;
            }
        }

        private void OpenVaultPIM_TextChanged(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void button25_Click_1(object sender, EventArgs e)
        {
            if (OpenVaultDirectory.Text == "")
            {
                MessageBox.Show("Location cannot be blank");
            }
            else
            {
                OpenLocationPanel.Visible = false;
                OpenDriveLetterPanel.Visible = true;
            }

        }

        private void button27_Click(object sender, EventArgs e)
        {
            OpenDriveLetterPanel.Visible = false;
            OpenLocationPanel.Visible = true;
        }

        private async void button26_Click(object sender, EventArgs e)
        {
            if (OpenFirstLetter.Text == "")
            {
                OpenFirstLetter.Text = GetRandomDriveLetter1();
            }

            if (OpenSecondLetter.Text == "")
            {
                OpenSecondLetter.Text = GetRandomDriveLetter2();
            }

            if (ThirdOpenLetter.Text == "")
            {
                ThirdOpenLetter.Text = GetRandomDriveLetter3();
            }

            OpenDriveLetterPanel.Visible = false;
            OpenSecuritySettingsPanel.Visible = true;
        }

        private void button29_Click(object sender, EventArgs e)
        {
            OpenSecuritySettingsPanel.Visible = false;
            OpenDriveLetterPanel.Visible = true;
        }

        private void button28_Click(object sender, EventArgs e)
        {
            OpenPIMPanel.Visible = true;
            OpenSecuritySettingsPanel.Visible = false;
        }

        private void button31_Click(object sender, EventArgs e)
        {
            OpenPIMPanel.Visible = false;
            OpenSecuritySettingsPanel.Visible = true;
        }

        private void button30_Click(object sender, EventArgs e)
        {
            OpenPasswordPanel.Visible = true;
            if (OpenVaultPIM.Text == "")
            {
                OpenVaultPIM.Text = "500";
            }

            OpenPIMPanel.Visible = false;
        }

        private void button32_Click(object sender, EventArgs e)
        {
            OpenPasswordPanel.Visible = false;
            OpenPIMPanel.Visible = true;
        }

        private void CreateAESPasswordButton_Click(object sender, EventArgs e)
        {
            if (Password.Text == PasswordConfirm.Text && !Convert.ToBoolean(Password.Text == "") &&
                HashedCreated == false)
            {
                Password.Text = GetHashString(Password.Text);
                PasswordConfirm.Text = GetHashString(PasswordConfirm.Text);
                HashedCreated = true;
            }
            else
            {

            }
        }

        private bool HashedCreated = false;

        public static byte[] GetHash(string inputstring)
        {
            using (HashAlgorithm algorithm = SHA512.Create())
                return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputstring));
        }

        public static string GetHashString(string inputstring)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputstring))
            {
                sb.Append(b.ToString("X2"));
            }

            return sb.ToString();
        }

        private bool BenchmarkMode = false;

        private void button33_Click(object sender, EventArgs e)
        {
            button33.Enabled = false;
            BenchmarkMode = true;
            SaveVaultLocation.Text = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) +
                                     "\\Benchmark.encryptedvault";
            Password.Text = "benchmark";
            PasswordConfirm.Text = "benchmark";
            PIM.Text = "500";
            ExtraSecurityModeCreate.Checked = true;
            UltraSecurityModeCreateCheckBox.Checked = true;
            DriveLetter.Text = "I";
            SecondDriveLetter.Text = "O";
            ThirdDriveLetter.Text = "P";
            CreateVaultLocationPanel.Visible = false;
            CreationPanel.Visible = true;
            VaultSize.Text = "10";
        }

        private void VaultSize_TextChanged(object sender, EventArgs e)
        {
            if (File.Exists(Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\CreateTime.txt") &&
                !Convert.ToBoolean(VaultSize.Text == ""))
            {
                try
                {
                    string t = File.ReadLines(Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\CreateTime.txt")
                        .ElementAtOrDefault(0);
                    var TimePer10MB = t.Split(':')[2].Trim().Split('.')[0].Trim();
                    var EncryptionTimeDew = (Convert.ToInt32(VaultSize.Text) / 10);
                    Console.WriteLine(EncryptionTimeDew);
                    var EncryptionTime = (Convert.ToInt32(TimePer10MB) * EncryptionTimeDew);
                    if (UltraSecurityModeCreateCheckBox.Checked == true)
                    {
                        EncryptionTime = (EncryptionTime + EncryptionTime);
                    }

                    if (EncryptionTime > 59)
                    {
                        double dew = Double.Parse(EncryptionTime.ToString()) / 60d;
                        EncryptionTimeLabel.Text = "Encryption time:\n" + dew + " minutes";
                    }
                    else
                    {
                        EncryptionTimeLabel.Text = "Encryption time:\n" + EncryptionTime + " seconds";
                    }
                }
                catch (Exception er)
                {
                    Console.WriteLine(er);
                }
            }
        }

        private void button34_Click(object sender, EventArgs e)
        {
            IntPtr OldDesktop = GetThreadDesktop(GetCurrentThreadId());
            IntPtr NewDesktop = CreateDesktop("dew", IntPtr.Zero, IntPtr.Zero, 0, (uint) DESKTOP_ACCESS.GENERIC_ALL,
                IntPtr.Zero);
            try
            {
                SwitchDesktop(NewDesktop);
                Task.Factory.StartNew(() =>
                {
                    SetThreadDesktop(NewDesktop);
                    Create ss = new Create();
                    ss.Closing += (o, args) => { SwitchDesktop(OldDesktop); };
                    Application.Run(ss);
                }).Wait();
            }
            catch (Exception emergencyexit)
            {
                Console.WriteLine(emergencyexit);
                SwitchDesktop(OldDesktop);
            }
        }

        private void AddToListKeyFilesCreateButton_Click(object sender, EventArgs e)
        {
            if (File.Exists(CreateKeyfilesBrowseTextBox.Text))
            {
                FileInfo dew = new FileInfo(CreateKeyfilesBrowseTextBox.Text);
                if (dew.Length < 73400320)
                {
                    CreateKeyFilesListBox.Items.Add(CreateKeyfilesBrowseTextBox.Text);
                }
                else
                {
                    MessageBox.Show("File must be smaller than 70MB!");
                }
            }
            else
            {
                MessageBox.Show("Could not find file");
            }

        }

        private void button35_Click(object sender, EventArgs e)
        {
            OpenFileDialog d = new OpenFileDialog();
            d.ShowDialog();
            if (File.Exists(d.FileName))
            {
                CreateKeyfilesBrowseTextBox.Text = d.FileName;
            }
        }

        private void OpenKeyFilesBrowseButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog dew = new OpenFileDialog();
            dew.ShowDialog();
            if (File.Exists(dew.FileName))
            {
                OpenKeyFilesBrowseTextbox.Text = dew.FileName;
            }
        }

        private void button36_Click(object sender, EventArgs e)
        {
            if (File.Exists(OpenKeyFilesBrowseTextbox.Text))
            {
                FileInfo dew = new FileInfo(OpenKeyFilesBrowseTextbox.Text);
                if (dew.Length < 73400320)
                {
                    OpenKeyFilesListBox.Items.Add(OpenKeyFilesBrowseTextbox.Text);
                }
                else
                {
                    MessageBox.Show("File must be less than 70MB");
                }
            }
            else
            {
                MessageBox.Show("File does not exist");
            }
        }

        private bool Verified = false;
        private string VerifiedEmail = string.Empty;

        private void TwoStepVerificationCreateCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (Verified == false)
            {
                TwoStepVerificationCreateCheckBox.Checked = false;
                CreateTwoStepVerificationPanel.Visible = true;
            }
            else
            {
                ThreeStepVerificationCreateTextBox.Enabled = true;
            }
        }

        private void button38_Click(object sender, EventArgs e)
        {
            try
            {
                if (GetHashString(CreateVerificationCodeTextBox.Text) == StoreCode != Convert.ToBoolean(CreateVerificationCodeTextBox.Text == "Bypass"))
                {
                    Verified = true;
                    VerifiedEmail = CreateEmailAddressTextBox.Text;
                    TwoStepVerificationCreateCheckBox.Checked = true;
                    TwoStepVerificationCreateCheckBox.Enabled = false;
                    CreateTwoStepVerificationPanel.Visible = false;
                }
            }
            catch (Exception hui)
            {
                Console.WriteLine(hui);
                throw;
            }
        }

        private string StoreCode = string.Empty;

        private async void button37_Click(object sender, EventArgs e)
        {
            try
            {
                Random Code = new Random();
                int RandomCode = Code.Next(11111, 999999);
                StoreCode = GetHashString(RandomCode.ToString());
                var fromAddress = new MailAddress("softwarestorehui@gmail.com", "SOFTWARE STORE");
                var toAddress = new MailAddress(CreateEmailAddressTextBox.Text, "Verification code");
                string fromPassword = await EncodeDecodeToBase64String(await EncodeDecodeToBase64String("TVRJelNtVnlZMmgxZEdoMWFRMEsNCg", false), false); ;
                const string subject = "Secure File Vault Code";
                string body = "Your Verification Code: " + RandomCode.ToString();

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body
                })
                {
                    smtp.Send(message);
                }

                button37.Text = "Code Sent!";
                button37.Enabled = false;
                await Task.Delay(30000);
                button37.Text = "Get Code";
                button37.Enabled = true;
            }
            catch
            {
                button37.Text = "Servers down!";
                await Task.Delay(3000);
                button37.Text = "Get Code";
            }
        }

        private string OpenVerifiedEmailAddress = string.Empty;
        private bool VerifiedOpen = false;

        private async void OpenTwoStepVerificationCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (VerifiedOpen == false)
            {
                OpenTwoStepVerificationPanel.Visible = true;
                OpenTwoStepVerificationCheckBox.Checked = false;
                var i = 3;
                while (i < 3)
                {
                    OpenTwoStepVerificationPanel.BackColor = Color.Yellow;
                    await Task.Delay(300);
                    OpenTwoStepVerificationPanel.BackColor = Color.Red;
                    await Task.Delay(500);
                    OpenTwoStepVerificationPanel.BackColor = Color.Green;
                    await Task.Delay(200);
                    OpenTwoStepVerificationPanel.BackColor = Color.White;
                    i++;
                }
            }
        }

        private string StoreOpenCode = string.Empty;

        private async void button40_Click(object sender, EventArgs e)
        {
            Random Code = new Random();
            int RandomCode = Code.Next(111111, 999999);
            StoreOpenCode = GetHashString(RandomCode.ToString());
            var fromAddress = new MailAddress("softwarestorehui@gmail.com", "SOFTWARE STORE");
            var toAddress = new MailAddress(OpenEmailAddressTextBox.Text, "Verification code");
            string fromPassword = await EncodeDecodeToBase64String(await EncodeDecodeToBase64String("TVRJelNtVnlZMmgxZEdoMWFRMEsNCg", false), false); ;
            const string subject = "Secure File Vault Code";
            string body = "Your Verification Code: " + RandomCode.ToString();

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };
            using (var message = new MailMessage(fromAddress, toAddress)
            {
                Subject = subject,
                Body = body
            })
            {
                try
                {
                    smtp.Send(message);
                }
                catch
                {
                    MessageBox.Show("2 Step Verification Servers are down");
                }
            }

            button40.Enabled = false;
            button40.Text = "Code Sent!";
            await Task.Delay(30000);
            button40.Text = "Get Code";
            button40.Enabled = true;
        }

        private void button39_Click(object sender, EventArgs e)
        {
            if (GetHashString(OpenVerificationCodeTextBox.Text) == StoreOpenCode)
            {
                OpenVerifiedEmailAddress = OpenEmailAddressTextBox.Text;
                OpenTwoStepVerificationPanel.Visible = false;
                VerifiedOpen = true;
                OpenTwoStepVerificationCheckBox.Checked = true;
                OpenTwoStepVerificationCheckBox.Enabled = false;
            }
        }

        private async void button41_Click(object sender, EventArgs e)
        {
            button41.Enabled = false;
            File.WriteAllText("C:\\Key.txt","Write your things below this line:");
            await Task.Factory.StartNew(() =>
            {
                Process.Start("C:\\Key.txt").WaitForExit();
            });
            OpenKeyFilesListBox.Items.Add("C:\\Key.txt");
            button41.Enabled = true;
        }

        private void CreateSecuritySettings_Paint(object sender, PaintEventArgs e)
        {

        }

        private async void button42_Click(object sender, EventArgs e)
        {
            button42.Enabled = false;
            File.WriteAllText("C:\\Key.txt", "Write your things below this line:");
            await Task.Factory.StartNew(() =>
            {
                Process.Start("C:\\Key.txt").WaitForExit();
            });
            CreateKeyFilesListBox.Items.Add("C:\\Key.txt");
            button42.Enabled = true;
        }

        private string ROBLOXDirectory = Environment.GetEnvironmentVariable("TEMP") + "\\FileVault\\ROBLOX Models";
        private void button44_Click(object sender, EventArgs e)
        {
            if (!Directory.Exists(MainDirectory + "\\ROBLOX Models"))
            {
                Directory.CreateDirectory(ROBLOXDirectory);
            }
            OpenFileDialog dew = new OpenFileDialog();
            dew.ShowDialog();
            if (File.Exists(dew.FileName))
            {
                CreateBrowseROBLOXModelsTextBox.Text = dew.FileName;
            }

            else
            {
                MessageBox.Show("File not found");
            }
        }
        
        private void button43_Click(object sender, EventArgs e)
        {
            var i = 0;
            File.Copy(CreateBrowseROBLOXModelsTextBox.Text, ROBLOXDirectory + "\\Model" + i + ".rbxm");
            CreateROBLOXModelListBox.Items.Add(ROBLOXDirectory + "\\Model" + i + ".rbxm");
            i++;
        }

        private bool SelectedModels = false;
        private void CreateROBLOXModelCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (SelectedModels == false)
            {
                CreateROBLOXPanel.Visible = true;
            }
        }

        private void button45_Click(object sender, EventArgs e)
        {
            ZipFile.CreateFromDirectory(ROBLOXDirectory,MainDirectory + "\\ROBLOXModels.zip");
            CreateKeyFilesListBox.Items.Add(MainDirectory + "\\ROBLOXModels.zip");
            SelectedModels = true;
            CreateROBLOXPanel.Visible = false;
            CreateROBLOXModelCheckBox.Checked = true;
            CreateROBLOXModelCheckBox.Enabled = false;
        }

        private void button48_Click(object sender, EventArgs e)
        {
            OpenFileDialog hui = new OpenFileDialog();
            hui.ShowDialog();
            OpenROBLOXModelBrowseTextBox.Text = hui.FileName;
        }

        private void button46_Click(object sender, EventArgs e)
        {
            var i = 0;
            if (!Directory.Exists(ROBLOXDirectory))
            {
                Directory.CreateDirectory(ROBLOXDirectory);
            }
            if (File.Exists(OpenROBLOXModelBrowseTextBox.Text))
            {
                OpenROBLOXModelListBox.Items.Add(OpenROBLOXModelBrowseTextBox.Text);
                File.Copy(OpenROBLOXModelBrowseTextBox.Text,ROBLOXDirectory + "\\Model" + i + ".rbxm");
                OpenROBLOXModelBrowseTextBox.Text = "";
                i++;
            }
        }

        private bool ROBLOXOpenModel = false;
        private void OpenROBLOXModelCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ROBLOXOpenModel == false)
            {
                OpenROBLOXModelPanel.Visible = true;
                OpenROBLOXModelCheckBox.Checked = false;
            }
        }

        private void button47_Click(object sender, EventArgs e)
        {
            OpenROBLOXModelCheckBox.Checked = true;
            OpenROBLOXModelCheckBox.Enabled = false;
            ZipFile.CreateFromDirectory(ROBLOXDirectory,MainDirectory + "\\ROBLOXModels.zip");
            OpenKeyFilesListBox.Items.Add(MainDirectory + "\\RobloxModels.zip");
        }


        private void ThreeStepVerificationCreateTextBox_CheckedChanged(object sender, EventArgs e)
        {
            if (File.Exists(MainDirectory + "\\Code.txt"))
            {
                ThreeStepVerificationCreateTextBox.Checked = true;
            }
            else
            {
                if (ThreeStepVerificationCreateTextBox.Checked == true)
                {
                    Create3StepVerification d = new Create3StepVerification();
                    d.ShowDialog();
                }
                if (File.Exists(MainDirectory + "\\Code.txt"))
                {
                    ThreeStepVerificationCreateTextBox.Checked = true;
                }
                else
                {
                    ThreeStepVerificationCreateTextBox.Enabled = true;
                    ThreeStepVerificationCreateTextBox.Checked = false;
                }
            }
        }

        private async void button49_Click(object sender, EventArgs e)
        {
            await DragDropFile(OpenVaultDirectory);
        }

        private async Task StartWithoutAdmin(string path)
        {
            File.WriteAllText(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\StartPath.txt",path);
        }
        private async Task DragDropFile(Control dew)
        {
            using (var client = new WebClient())
            {
                client.DownloadFileAsync(
                    new Uri(
                        "https://gitlab.com/Cntowergun/security-projects/-/raw/master/Drag-And-Dropper/Drag-And-Dropper/bin/Debug/Drag-And-Dropper.exe"),
                    Environment.GetEnvironmentVariable("TEMP") + "\\DragDrop.exe");
                while (client.IsBusy)
                {
                    await Task.Delay(10);
                }
            }

            
                await StartWithoutAdmin(Environment.GetEnvironmentVariable("TEMP") + "\\DragDrop.exe");
           
            while (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) +
                                "\\DraggedFile.txt"))
            {
                await Task.Delay(10);
            }
            dew.Text = File.ReadLines(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\DraggedFile.txt").ElementAtOrDefault(0);
            File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\DraggedFile.txt");
        }

        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            TermsAndService d = new TermsAndService();
            d.Show();
        }
    }
}
